#include "misc/cold.h"

/**
* @brief This is my first person action demo
*
*        09.11.2017:
*			- terrain generation
*			- locations and paths debug output
*			- free camera mode
*		
*		 15.02.2018:
*           - bsp partitioning
*           - frustum objects discarding
*           - bigger terrain, custom depth test
*           - simple collision handling in lua script
*        
*        22.04.2018:
*           - added fake field and distant shapes
*           - added fake moon and stars
*
*        29.07.2018:
*           - added ambient, diffuse, specular light
*
*/

int main() {
	return createApplication()->run();
}

#include "MinTechEngine.h"

#include "entities/Terrain.h"
#include "entities/Object.h"
#include "interfaces/CollisionTestDelegate.h"
#include "entities/Background.h"
#include "entities/Sky.h"

#include <ogl/glm/gtc/matrix_transform.hpp>

#include <iostream>
#include <iomanip>
#include <cassert>
#include <algorithm>
#include <list>

const double mte::MinTechEngine::BSP_NODE_SIZE = 12800000.0;
const double mte::MinTechEngine::EPSILON = 0.00009;

void mte::MinTechEngine::init(int width, int height, double fieldOfView, double frustumNear, double frustumFar) {
	assert(("Viewport width is invalid", width > 320));
	assert(("Viewport height is invalid", height > 240));
	assert(("Field of view is invalid", fieldOfView > 20.0f));
	
	nodes.clear();

	camera.setWidth(width);
	camera.setHeight(height);
	camera.setFieldOfView(fieldOfView);
	camera.setZNear(frustumNear);
	camera.setZFar(frustumFar);

	shouldDisplayBoundingBoxes = false;

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CW);
	glEnable(GL_DEPTH_TEST);

	camera.setPerspective(glm::perspective((float)camera.getFieldOfView(), (float)(1.0 * width / height), float(camera.getZNear()), float(camera.getZFar())));
	camera.setLookat(glm::vec3(0.0, 0.0, 1.0), glm::vec3(0.0, 1.0, 0.0));

	numberOfNodes = (int)pow(2.0, ceil(log2(1.0 * (frustumFar / BSP_NODE_SIZE))));
	nodes.resize(numberOfNodes * numberOfNodes);

	nodeInfoLookup.resize(numberOfNodes * numberOfNodes * 8);
	for (auto node = 0; node < nodes.size(); ++node) {
		auto row = node / numberOfNodes;
		auto column = node % numberOfNodes;
		int offset = node << 3;
		nodeInfoLookup[offset + 0] = (column - numberOfNodes / 2) * BSP_NODE_SIZE - EPSILON;
		nodeInfoLookup[offset + 1] = (column - numberOfNodes / 2 + 1) * BSP_NODE_SIZE + EPSILON;
		nodeInfoLookup[offset + 2] = (row - numberOfNodes / 2) * BSP_NODE_SIZE - EPSILON;
		nodeInfoLookup[offset + 3] = (row - numberOfNodes / 2 + 1) * BSP_NODE_SIZE + EPSILON;
		nodeInfoLookup[offset + 4] = (nodeInfoLookup[offset + 1] + nodeInfoLookup[offset + 0]) / 2;
		nodeInfoLookup[offset + 5] = (nodeInfoLookup[offset + 3] + nodeInfoLookup[offset + 2]) / 2;
		nodeInfoLookup[offset + 6] = (nodeInfoLookup[offset + 1] - nodeInfoLookup[offset + 0]);
		nodeInfoLookup[offset + 7] = (nodeInfoLookup[offset + 3] - nodeInfoLookup[offset + 2]);
	}

	lodThresholds[0] = BSP_NODE_SIZE * BSP_NODE_SIZE + EPSILON;
	lodThresholds[1] = 2 * lodThresholds[0] + EPSILON;
}

size_t mte::MinTechEngine::createTerrain(const std::vector<GLfloat> &heights, const glm::ivec2 &dimensions, glm::dvec3 scale) {
	auto temp = numberOfHandles++;
	worldObjects[temp] = std::shared_ptr<Object>(new Terrain(temp, heights, dimensions, scale, this));
	return temp;
}

size_t mte::MinTechEngine::createBackground(const std::vector<GLfloat> &heights, const glm::ivec2 &dimensions) {
	auto temp = numberOfHandles++;
	auto heightRatio = static_cast<float>(dimensions.y) / camera.getHeight();
	auto widthRatio = camera.getWidth() / static_cast<float>(dimensions.x);
	worldObjects[temp] = std::shared_ptr<Object>(new Background(temp, heights, dimensions, heightRatio, widthRatio, camera.getFieldOfView() / 360.0 * 2 * 3.14159263));
	background.push_back(temp);
	return temp;
}

size_t mte::MinTechEngine::createSky(const std::vector<glm::vec3> &starfield, const std::vector<GLfloat> &luminaryLandscape, const glm::ivec2 &luminaryDimensions, glm::dvec3 luminaryPosition) {
	auto temp = numberOfHandles++;
	worldObjects[temp] = std::shared_ptr<Object>(new Sky(temp, starfield, luminaryLandscape, luminaryDimensions, glm::vec3(luminaryPosition)));
	background.push_back(temp);
	cachedLuminaryDirection = glm::normalize(luminaryPosition);
	return temp;
}

void mte::MinTechEngine::drawObjects() {
	auto zNear = camera.getZNear();
	auto zFar = camera.getZFar();
	auto zMiddle = 0.01 * camera.getZFar();
	auto fov = (float)camera.getFieldOfView();
	auto ratio = (float)(1.0 * camera.getWidth() / camera.getHeight());

	const auto& look = camera.getLookat();
	const auto& position = camera.getPosition();

	std::list<std::pair<int, int>> farObjects;
	std::list<std::pair<int, int>> nearObjects;

	// draw world nodes
	for (auto node = 0; node < nodes.size(); ++node) {
		// lower and upper bounds
		double lowX, highX, lowY, highY, lowZ, highZ;
		lowX = lowY = lowZ = std::numeric_limits<decltype(lowX)>::max();
		highX = highY = highZ = std::numeric_limits<decltype(highX)>::min();
		for (auto& object : nodes[node]) {
			auto objectBounds = worldObjects[object]->getBSPInfo()->getVerticalBounds(node);
			if (lowY > objectBounds.r)
				lowY = objectBounds.r;
			if (highY < objectBounds.g)
				highY = objectBounds.g;
		}
		
		int offset = (node << 3);
		lowX = nodeInfoLookup[offset + 0];
		highX = nodeInfoLookup[offset + 1];
		lowZ = nodeInfoLookup[offset + 2];
		highZ = nodeInfoLookup[offset + 3];

		glm::dmat4 transformation(1.0);
		glm::dvec3 center(nodeInfoLookup[offset + 4], (highY + lowY) / 2, nodeInfoLookup[offset + 5]);
		transformation = glm::translate(transformation, center - position);
		transformation = glm::scale(transformation, glm::dvec3(nodeInfoLookup[offset + 6], highY - lowY, nodeInfoLookup[offset + 7]));
		box.transform(glm::dmat4(look) * transformation);
		
		auto temp = center - position;
		auto distance = temp.x * temp.x + temp.y * temp.y;

		auto lod = -1;
		if (nodes[node].size()) {
			if (distance < lodThresholds[0])
				lod = Object::LOD_HIGH;
			else if (camera.hasInSight(box)) {
				if (distance < lodThresholds[1])
					lod = Object::LOD_MEDIUM;
				else
					lod = Object::LOD_LOW;
			}
		}

		if (lod == Object::LOD_HIGH) {
			farObjects.push_back(std::make_pair(node, lod));
			nearObjects.push_back(std::make_pair(node, lod));
		} else {
			farObjects.push_back(std::make_pair(node, lod));
		}
	}

	// draw background
	camera.setZNear(0.1);
	camera.setZFar(2.0);
	camera.setPerspective(glm::perspective(fov, ratio, float(0.1), float(2.0)));
	glDisable(GL_DEPTH_TEST);
	for (auto& object : background)
		worldObjects[object]->draw(camera.getPerspective(), look);
	glEnable(GL_DEPTH_TEST);

	// draw far objects
	camera.setZNear(zMiddle);
	camera.setZFar(zFar);
	camera.setPerspective(glm::perspective(fov, ratio, float(zMiddle), float(zFar)));
	for (auto& nodeAndLod : farObjects) {
		if (nodeAndLod.second != -1)
			for (auto& object : nodes[nodeAndLod.first])
				worldObjects[object]->draw(camera.getPerspective(), look, cachedLuminaryDirection, position, nodeAndLod.first, nodeAndLod.second);
	}

	// draw near objects
	glClear(GL_DEPTH_BUFFER_BIT);
	camera.setZNear(zNear);
	camera.setZFar(zMiddle);
	camera.setPerspective(glm::perspective(fov, ratio, float(zNear), float(zMiddle)));
	for (auto& nodeAndLod : nearObjects) {
		if (nodeAndLod.second != -1)
			for (auto& object : nodes[nodeAndLod.first])
				worldObjects[object]->draw(camera.getPerspective(), look, cachedLuminaryDirection, position, nodeAndLod.first, nodeAndLod.second);
	}

	// restore camera
	camera.setZNear(zNear);
	camera.setZFar(zFar);
	camera.setPerspective(glm::perspective(fov, ratio, float(zNear), float(zFar)));
}

void mte::MinTechEngine::clear() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void mte::MinTechEngine::flush() {
	glutSwapBuffers();
}

void mte::MinTechEngine::resize(int width, int height) {
	assert(("Viewport width is invalid", width > 320));
	assert(("Viewport height is invalid", height > 240));

	camera.setWidth(width);
	camera.setHeight(height);

	glViewport(0, 0, width, height);
	camera.setPerspective(glm::perspective((float)camera.getFieldOfView(), (float)(1.0 * width / height), float(camera.getZNear()), float(camera.getZFar())));
}

void mte::MinTechEngine::setCamera(const glm::dvec3 &position, const glm::dvec3 &target, const glm::dvec3 &up) {
	glm::vec3 bar((float)target.x, (float)target.y, (float)target.z);
	glm::vec3 baz((float)up.x, (float)up.y, (float)up.z);
	camera.setLookat(bar, baz);
	camera.setPosition(glm::dvec3(position.x, position.y, position.z));
}

void mte::MinTechEngine::toggleBoundingBoxesDrawing() {
	shouldDisplayBoundingBoxes = !shouldDisplayBoundingBoxes;
}

void mte::MinTechEngine::performCollisionTest(CollisionTestDelegate& delegate) {
	int sizeInNodes = numberOfNodes;
	std::vector<glm::dvec4> vertices = {
		glm::dvec4(-0.5, +0.5, +0.5, +1.0),
		glm::dvec4(-0.5, +0.5, -0.5, +1.0),
		glm::dvec4(+0.5, +0.5, -0.5, +1.0),
		glm::dvec4(+0.5, +0.5, +0.5, +1.0),
		glm::dvec4(-0.5, -0.5, +0.5, +1.0),
		glm::dvec4(-0.5, -0.5, -0.5, +1.0),
		glm::dvec4(+0.5, -0.5, -0.5, +1.0),
		glm::dvec4(+0.5, -0.5, +0.5, +1.0)
	};
	decltype(auto) locateBox = [&sizeInNodes, &vertices](const glm::dmat4& boxTransformation) {
		for (auto& vertex : vertices)
			vertex = boxTransformation * vertex;

		std::set<int> result;

		auto min = -sizeInNodes / 2 * BSP_NODE_SIZE - EPSILON;
		auto max = -min;
		auto HALF_WORLD_SIZE = sizeInNodes / 2 * BSP_NODE_SIZE;

		for (auto& vertex : vertices) {
			double offsets[4][2] = {
				{ -EPSILON, -EPSILON }, { -EPSILON, +EPSILON }, { +EPSILON, -EPSILON }, { +EPSILON, +EPSILON }
			};
			{
				glm::dvec3 point(vertex.x - EPSILON, vertex.y, vertex.z - EPSILON);
				if (point.z > min && point.z < max && point.x > min && point.x < max) {
					auto column = static_cast<int>((point.x + HALF_WORLD_SIZE) / BSP_NODE_SIZE);
					auto row = static_cast<int>((point.z + HALF_WORLD_SIZE) / BSP_NODE_SIZE);
					if (column > -1 && row > -1 && column < sizeInNodes && row < sizeInNodes)
						result.insert(row * sizeInNodes + column);
				}
			}
			{
				glm::dvec3 point(vertex.x - EPSILON, vertex.y, vertex.z + EPSILON);
				if (point.z > min && point.z < max && point.x > min && point.x < max) {
					auto column = static_cast<int>((point.x + HALF_WORLD_SIZE) / BSP_NODE_SIZE);
					auto row = static_cast<int>((point.z + HALF_WORLD_SIZE) / BSP_NODE_SIZE);
					if (column > -1 && row > -1 && column < sizeInNodes && row < sizeInNodes)
						result.insert(row * sizeInNodes + column);
				}
			}
			{
				glm::dvec3 point(vertex.x + EPSILON, vertex.y, vertex.z - EPSILON);
				if (point.z > min && point.z < max && point.x > min && point.x < max) {
					auto column = static_cast<int>((point.x + HALF_WORLD_SIZE) / BSP_NODE_SIZE);
					auto row = static_cast<int>((point.z + HALF_WORLD_SIZE) / BSP_NODE_SIZE);
					if (column > -1 && row > -1 && column < sizeInNodes && row < sizeInNodes)
						result.insert(row * sizeInNodes + column);
				}
			}
			{
				glm::dvec3 point(vertex.x + EPSILON, vertex.y, vertex.z + EPSILON);
				if (point.z > min && point.z < max && point.x > min && point.x < max) {
					auto column = static_cast<int>((point.x + HALF_WORLD_SIZE) / BSP_NODE_SIZE);
					auto row = static_cast<int>((point.z + HALF_WORLD_SIZE) / BSP_NODE_SIZE);
					if (column > -1 && row > -1 && column < sizeInNodes && row < sizeInNodes)
						result.insert(row * sizeInNodes + column);
				}
			}
		}
		return result;
	};

	auto test = [](const glm::dvec3& a, const glm::dvec3& b, const glm::dvec3& c, const glm::dvec3& k, const glm::dvec3& l, const glm::dvec3& m) {
		// calculate the plane equation for (a, b, c)
		auto ab = b - a;
		auto ac = c - a;
		auto n = glm::cross(ab, ac);
		auto d =  -glm::dot(n, a);
		decltype(auto) check = [&a, &b, &c, &n, &d](const glm::dvec3& p, const glm::dvec3& q) {
			// calculate the parametric line equation for (k, l)
			auto v = q - p;
			// compute the intersection
			auto t = -(d + glm::dot(n, p)) / glm::dot(n, v);
			if (t < -EPSILON || t > 1.0 + EPSILON)
				return false;
			auto x = p + v * t;
			//check if intersection is inside the triangle
			auto cross0 = glm::cross((x - a), (b - a));
			auto cross1 = glm::cross((x - b), (c - b));
			auto cross2 = glm::cross((x - c), (a - c));
			auto test0 = glm::dot(cross0, cross1);
			auto test1 = glm::dot(cross1, cross2);
			auto test2 = glm::dot(cross2, cross0);
			if ((test0 < 0 && test1 < 0 && test2 < 0) || (test0 > 0 && test1 > 0 && test2 > 0))
				return true;
			return false;
		};
		return (check(k, l) || check(l, m) || check(m, k));
	};

	for (auto i = 0U; i < delegate.numberOfActors(); ++i) {
		const auto& transformation = delegate.boundingBoxTransformationForActor(i);
		auto nodesIndices = locateBox(transformation);
		for (auto index : nodesIndices) {
			// index is an integer value of node index
			// what wegonna do is check every triangle of bounding box
			// for collision with every triangle of every object
			// but in that particular node only
			for (auto& handle : nodes[index]) {
				auto object = worldObjects[handle];
				auto bspInfo = object->getBSPInfo();
				auto numberOfTriangles = bspInfo->numberOfTriangles(index);
				for (unsigned int i = 0; i < numberOfTriangles; ++i) {
					// retrieve triangle points
					glm::dvec3 a, b, c;
					if (bspInfo->triangleAtIndex(index, i, a, b, c)) {
						static const double MAX_DISTANCE = BSP_NODE_SIZE / 4;
						glm::dvec3 center = (a + b + c) / 3.0;
						glm::dvec3 boxCenter = glm::dvec3(transformation * glm::dvec4(0.0, 0.0, 0.0, 1.0));
						if (glm::length(center - boxCenter) > MAX_DISTANCE)
							continue;
						// check for collision with triangles from actor's bb
						static std::vector<GLint> indices = {
							0, 1, 2, 0, 2, 3,
							4, 0, 3, 4, 3, 7,
							7, 3, 2, 7, 2, 6,
							6, 2, 1, 6, 1, 5,
							5, 1, 0, 5, 0, 4,
							5, 4, 7, 5, 7, 6
						};
						for (auto j = 0U; j < indices.size(); j += 3) {
							glm::dvec3 k(vertices[indices[j + 0]]);
							glm::dvec3 l(vertices[indices[j + 1]]);
							glm::dvec3 m(vertices[indices[j + 2]]);
							if (test(a, b, c, k, l, m))
								goto collision;
						}
					}
				}
			}
		}
		continue;
	collision:
		delegate.onCollision(i);
	}
}

void mte::MinTechEngine::executePartition(Object &object, std::pair<std::vector<glm::dvec3>, std::vector<glm::dvec3>>& vertices, std::vector<GLuint>& indices, std::vector<std::vector<GLuint>> &indicesParts, std::list<int> &indicesPartsIndices) noexcept {
	auto addPoint = [&vertices](const glm::dvec3 &coords)->void {
		vertices.first.push_back(coords);
		vertices.second.emplace_back(1.0, 1.0, 1.0);
	};
	auto addTriangle = [&indices](const glm::ivec3 &primitive)->void {
		indices.push_back(primitive[0]);
		indices.push_back(primitive[1]);
		indices.push_back(primitive[2]);
	};
	auto count = indices.size();
	for (auto plane = 0; plane < numberOfNodes - 1; ++plane) {
		auto intersector = (plane - (numberOfNodes >> 1) + 1) * (BSP_NODE_SIZE);
		// X0Y planes
		for (auto index = 0U; index < count; index += 3) {
			auto& v0 = vertices.first[indices[index + 0]];
			auto& v1 = vertices.first[indices[index + 1]];
			auto& v2 = vertices.first[indices[index + 2]];
			if (abs(v2.z - intersector) <= (BSP_NODE_SIZE) + EPSILON) {
				auto dz_a = v0.z - intersector;
				auto dz_b = v1.z - intersector;
				auto dz_c = v2.z - intersector;
				if ((abs(dz_a) < EPSILON && dz_b < 0.0 && dz_c > 0.0) || (abs(dz_a) < EPSILON && dz_b > 0.0 && dz_c < 0.0)) {
					auto ratio = -dz_b / (dz_c - dz_b);
					auto new_x = v1.x * (1.0 - ratio) + v2.x * ratio;
					auto new_y = v1.y * (1.0 - ratio) + v2.y * ratio;
					auto new_z = intersector;
					addPoint(glm::detail::tvec3<decltype(new_x)>(new_x, new_y, new_z));
					addTriangle(glm::ivec3(indices[index + 0], indices[index + 1], vertices.first.size() - 1));
					addTriangle(glm::ivec3(indices[index + 0], vertices.first.size() - 1, indices[index + 2]));
					indices[index + 0] = indices[index + 1] = indices[index + 2];
				}
				else if ((abs(dz_b) < EPSILON && dz_c < 0.0 && dz_a > 0.0) || (abs(dz_b) < EPSILON && dz_c > 0.0 && dz_a < 0.0)) {
					auto ratio = -dz_c / (dz_a - dz_c);
					auto new_x = v2.x * (1.0 - ratio) + v0.x * ratio;
					auto new_y = v2.y * (1.0 - ratio) + v0.y * ratio;
					auto new_z = intersector;
					addPoint(glm::detail::tvec3<decltype(new_x)>(new_x, new_y, new_z));
					addTriangle(glm::ivec3(indices[index + 0], indices[index + 1], vertices.first.size() - 1));
					addTriangle(glm::ivec3(vertices.first.size() - 1, indices[index + 1], indices[index + 2]));
					indices[index + 0] = indices[index + 1] = indices[index + 2];
				}
				else if ((abs(dz_c) < EPSILON && dz_a < 0.0 && dz_b > 0.0) || (abs(dz_c) < EPSILON && dz_a > 0.0 && dz_b < 0.0)) {
					auto ratio = -dz_a / (dz_b - dz_a);
					auto new_x = v0.x * (1.0 - ratio) + v1.x * ratio;
					auto new_y = v0.y * (1.0 - ratio) + v1.y * ratio;
					auto new_z = intersector;
					addPoint(glm::detail::tvec3<decltype(new_x)>(new_x, new_y, new_z));
					addTriangle(glm::ivec3(indices[index + 0], vertices.first.size() - 1, indices[index + 2]));
					addTriangle(glm::ivec3(vertices.first.size() - 1, indices[index + 1], indices[index + 2]));
					indices[index + 0] = indices[index + 1] = indices[index + 2];
				}
				else if ((dz_a < 0.0 && dz_b < 0.0 && dz_c > 0.0) || (dz_a > 0.0 && dz_b > 0.0 && dz_c < 0.0)) {
					auto ratio_0 = dz_c / (dz_c - dz_a);
					auto new_x_0 = v2.x * (1.0 - ratio_0) + v0.x * ratio_0;
					auto new_y_0 = v2.y * (1.0 - ratio_0) + v0.y * ratio_0;
					auto new_z_0 = intersector;
					auto ratio_1 = dz_c / (dz_c - dz_b);
					auto new_x_1 = v2.x * (1.0 - ratio_1) + v1.x * ratio_1;
					auto new_y_1 = v2.y * (1.0 - ratio_1) + v1.y * ratio_1;
					auto new_z_1 = intersector;
					addPoint(glm::detail::tvec3<decltype(new_x_0)>(new_x_0, new_y_0, new_z_0));
					addPoint(glm::detail::tvec3<decltype(new_x_1)>(new_x_1, new_y_1, new_z_1));
					addTriangle(glm::ivec3(indices[index + 0], indices[index + 1], vertices.first.size() - 2));
					addTriangle(glm::ivec3(indices[index + 1], vertices.first.size() - 1, vertices.first.size() - 2));
					addTriangle(glm::ivec3(vertices.first.size() - 2, vertices.first.size() - 1, indices[index + 2]));
					indices[index + 0] = indices[index + 1] = indices[index + 2];
				}
				else if ((dz_b < 0.0 && dz_c < 0.0 && dz_a > 0.0) || (dz_b > 0.0 && dz_c > 0.0 && dz_a < 0.0)) {
					auto ratio_0 = dz_a / (dz_a - dz_b);
					auto new_x_0 = v0.x * (1.0 - ratio_0) + v1.x * ratio_0;
					auto new_y_0 = v0.y * (1.0 - ratio_0) + v1.y * ratio_0;
					auto new_z_0 = intersector;
					auto ratio_1 = dz_a / (dz_a - dz_c);
					auto new_x_1 = v0.x * (1.0 - ratio_1) + v2.x * ratio_1;
					auto new_y_1 = v0.y * (1.0 - ratio_1) + v2.y * ratio_1;
					auto new_z_1 = intersector;
					addPoint(glm::detail::tvec3<decltype(new_x_0)>(new_x_0, new_y_0, new_z_0));
					addPoint(glm::detail::tvec3<decltype(new_x_1)>(new_x_1, new_y_1, new_z_1));
					addTriangle(glm::ivec3(indices[index + 0], vertices.first.size() - 2, vertices.first.size() - 1));
					addTriangle(glm::ivec3(vertices.first.size() - 1, vertices.first.size() - 2, indices[index + 1]));
					addTriangle(glm::ivec3(vertices.first.size() - 1, indices[index + 1], indices[index + 2]));
					indices[index + 0] = indices[index + 1] = indices[index + 2];
				}
				else if ((dz_c < 0.0 && dz_a < 0.0 && dz_b > 0.0) || (dz_c > 0.0 && dz_a > 0.0 && dz_b < 0.0)) {
					auto ratio_0 = dz_b / (dz_b - dz_c);
					auto new_x_0 = v1.x * (1.0 - ratio_0) + v2.x * ratio_0;
					auto new_y_0 = v1.y * (1.0 - ratio_0) + v2.y * ratio_0;
					auto new_z_0 = intersector;
					auto ratio_1 = dz_b / (dz_b - dz_a);
					auto new_x_1 = v1.x * (1.0 - ratio_1) + v0.x * ratio_1;
					auto new_y_1 = v1.y * (1.0 - ratio_1) + v0.y * ratio_1;
					auto new_z_1 = intersector;
					addPoint(glm::detail::tvec3<decltype(new_x_0)>(new_x_0, new_y_0, new_z_0));
					addPoint(glm::detail::tvec3<decltype(new_x_1)>(new_x_1, new_y_1, new_z_1));
					addTriangle(glm::ivec3(indices[index + 1], vertices.first.size() - 2, vertices.first.size() - 1));
					addTriangle(glm::ivec3(vertices.first.size() - 1, vertices.first.size() - 2, indices[index + 2]));
					addTriangle(glm::ivec3(vertices.first.size() - 1, indices[index + 2], indices[index + 0]));
					indices[index + 0] = indices[index + 1] = indices[index + 2];
				}
			}
		}
	}
	count = indices.size();
	for (auto plane = 0; plane < numberOfNodes - 1; ++plane) {
		auto intersector = (plane - (numberOfNodes >> 1) + 1) * (BSP_NODE_SIZE);
		// Y0Z planes
		for (auto index = 0U; index < count; index += 3) {
			auto& v0 = vertices.first[indices[index + 0]];
			auto& v1 = vertices.first[indices[index + 1]];
			auto& v2 = vertices.first[indices[index + 2]];
			if (abs(v2.x - intersector) <= (BSP_NODE_SIZE) + EPSILON) {
				auto dz_a = v0.x - intersector;
				auto dz_b = v1.x - intersector;
				auto dz_c = v2.x - intersector;
				if ((abs(dz_a) < EPSILON && dz_b < 0.0 && dz_c > 0.0) || (abs(dz_a) < EPSILON && dz_b > 0.0 && dz_c < 0.0)) {
					auto ratio = -dz_b / (dz_c - dz_b);
					auto new_y = v1.y * (1.0 - ratio) + v2.y * ratio;
					auto new_z = v1.z * (1.0 - ratio) + v2.z * ratio;
					auto new_x = intersector;
					addPoint(glm::detail::tvec3<decltype(new_x)>(new_x, new_y, new_z));
					addTriangle(glm::ivec3(indices[index + 0], indices[index + 1], vertices.first.size() - 1));
					addTriangle(glm::ivec3(indices[index + 0], vertices.first.size() - 1, indices[index + 2]));
					indices[index + 0] = indices[index + 1] = indices[index + 2];
				}
				else if ((abs(dz_b) < EPSILON && dz_c < 0.0 && dz_a > 0.0) || (abs(dz_b) < EPSILON && dz_c > 0.0 && dz_a < 0.0)) {
					auto ratio = -dz_c / (dz_a - dz_c);
					auto new_y = v2.y * (1.0 - ratio) + v0.y * ratio;
					auto new_z = v2.z * (1.0 - ratio) + v0.z * ratio;
					auto new_x = intersector;
					addPoint(glm::detail::tvec3<decltype(new_x)>(new_x, new_y, new_z));
					addTriangle(glm::ivec3(indices[index + 0], indices[index + 1], vertices.first.size() - 1));
					addTriangle(glm::ivec3(vertices.first.size() - 1, indices[index + 1], indices[index + 2]));
					indices[index + 0] = indices[index + 1] = indices[index + 2];
				}
				else if ((abs(dz_c) < EPSILON && dz_a < 0.0 && dz_b > 0.0) || (abs(dz_c) < EPSILON && dz_a > 0.0 && dz_b < 0.0)) {
					auto ratio = -dz_a / (dz_b - dz_a);
					auto new_y = v0.y * (1.0 - ratio) + v1.y * ratio;
					auto new_z = v0.z * (1.0 - ratio) + v1.z * ratio;
					auto new_x = intersector;
					addPoint(glm::detail::tvec3<decltype(new_x)>(new_x, new_y, new_z));
					addTriangle(glm::ivec3(indices[index + 0], vertices.first.size() - 1, indices[index + 2]));
					addTriangle(glm::ivec3(vertices.first.size() - 1, indices[index + 1], indices[index + 2]));
					indices[index + 0] = indices[index + 1] = indices[index + 2];
				}
				else if ((dz_a < 0.0 && dz_b < 0.0 && dz_c > 0.0) || (dz_a > 0.0 && dz_b > 0.0 && dz_c < 0.0)) {
					auto ratio_0 = dz_c / (dz_c - dz_a);
					auto new_y_0 = v2.y * (1.0 - ratio_0) + v0.y * ratio_0;
					auto new_z_0 = v2.z * (1.0 - ratio_0) + v0.z * ratio_0;
					auto new_x_0 = intersector;
					auto ratio_1 = dz_c / (dz_c - dz_b);
					auto new_y_1 = v2.y * (1.0 - ratio_1) + v1.y * ratio_1;
					auto new_z_1 = v2.z * (1.0 - ratio_1) + v1.z * ratio_1;
					auto new_x_1 = intersector;
					addPoint(glm::detail::tvec3<decltype(new_x_0)>(new_x_0, new_y_0, new_z_0));
					addPoint(glm::detail::tvec3<decltype(new_x_1)>(new_x_1, new_y_1, new_z_1));
					addTriangle(glm::ivec3(indices[index + 0], indices[index + 1], vertices.first.size() - 2));
					addTriangle(glm::ivec3(indices[index + 1], vertices.first.size() - 1, vertices.first.size() - 2));
					addTriangle(glm::ivec3(vertices.first.size() - 2, vertices.first.size() - 1, indices[index + 2]));
					indices[index + 0] = indices[index + 1] = indices[index + 2];
				}
				else if ((dz_b < 0.0 && dz_c < 0.0 && dz_a > 0.0) || (dz_b > 0.0 && dz_c > 0.0 && dz_a < 0.0)) {
					auto ratio_0 = dz_a / (dz_a - dz_b);
					auto new_y_0 = v0.y * (1.0 - ratio_0) + v1.y * ratio_0;
					auto new_z_0 = v0.z * (1.0 - ratio_0) + v1.z * ratio_0;
					auto new_x_0 = intersector;
					auto ratio_1 = dz_a / (dz_a - dz_c);
					auto new_y_1 = v0.y * (1.0 - ratio_1) + v2.y * ratio_1;
					auto new_z_1 = v0.z * (1.0 - ratio_1) + v2.z * ratio_1;
					auto new_x_1 = intersector;
					addPoint(glm::detail::tvec3<decltype(new_x_0)>(new_x_0, new_y_0, new_z_0));
					addPoint(glm::detail::tvec3<decltype(new_x_1)>(new_x_1, new_y_1, new_z_1));
					addTriangle(glm::ivec3(indices[index + 0], vertices.first.size() - 2, vertices.first.size() - 1));
					addTriangle(glm::ivec3(vertices.first.size() - 1, vertices.first.size() - 2, indices[index + 1]));
					addTriangle(glm::ivec3(vertices.first.size() - 1, indices[index + 1], indices[index + 2]));
					indices[index + 0] = indices[index + 1] = indices[index + 2];
				}
				else if (dz_c < 0.0 && dz_a < 0.0 && dz_b > 0.0) {
					auto ratio_0 = dz_b / (dz_b - dz_c);
					auto new_y_0 = v1.y * (1.0 - ratio_0) + v2.y * ratio_0;
					auto new_z_0 = v1.z * (1.0 - ratio_0) + v2.z * ratio_0;
					auto new_x_0 = intersector;
					auto ratio_1 = dz_b / (dz_b - dz_a);
					auto new_y_1 = v1.y * (1.0 - ratio_1) + v0.y * ratio_1;
					auto new_z_1 = v1.z * (1.0 - ratio_1) + v0.z * ratio_1;
					auto new_x_1 = intersector;
					addPoint(glm::detail::tvec3<decltype(new_x_0)>(new_x_0, new_y_0, new_z_0));
					addPoint(glm::detail::tvec3<decltype(new_x_1)>(new_x_1, new_y_1, new_z_1));
					addTriangle(glm::ivec3(indices[index + 1], vertices.first.size() - 2, vertices.first.size() - 1));
					addTriangle(glm::ivec3(vertices.first.size() - 1, vertices.first.size() - 2, indices[index + 2]));
					addTriangle(glm::ivec3(vertices.first.size() - 1, indices[index + 2], indices[index + 0]));
					indices[index + 0] = indices[index + 1] = indices[index + 2];
				}
				else if ((dz_c > 0.0 && dz_a > 0.0 && dz_b < 0.0) || (dz_c > 0.0 && dz_a > 0.0 && dz_b < 0.0)) {
					auto ratio_0 = dz_b / (dz_b - dz_c);
					auto new_y_0 = v1.y * (1.0 - ratio_0) + v2.y * ratio_0;
					auto new_z_0 = v1.z * (1.0 - ratio_0) + v2.z * ratio_0;
					auto new_x_0 = intersector;
					auto ratio_1 = dz_b / (dz_b - dz_a);
					auto new_y_1 = v1.y * (1.0 - ratio_1) + v0.y * ratio_1;
					auto new_z_1 = v1.z * (1.0 - ratio_1) + v0.z * ratio_1;
					auto new_x_1 = intersector;
					addPoint(glm::detail::tvec3<decltype(new_x_0)>(new_x_0, new_y_0, new_z_0));
					addPoint(glm::detail::tvec3<decltype(new_x_1)>(new_x_1, new_y_1, new_z_1));
					addTriangle(glm::ivec3(indices[index + 1], vertices.first.size() - 2, vertices.first.size() - 1));
					addTriangle(glm::ivec3(vertices.first.size() - 1, vertices.first.size() - 2, indices[index + 2]));
					addTriangle(glm::ivec3(vertices.first.size() - 1, indices[index + 2], indices[index + 0]));
					indices[index + 0] = indices[index + 1] = indices[index + 2];
				}
			}
		}
	}
	indicesParts.resize(numberOfNodes * numberOfNodes);
	for (auto i = 0U; i < indices.size(); i += 3) {
		// process the triangle here
		const auto& a = vertices.first[indices[i + 0]];
		const auto& b = vertices.first[indices[i + 1]];
		const auto& c = vertices.first[indices[i + 2]];
		glm::dvec3 center((a.x + b.x + c.x) / 3, (a.y + b.y + c.y) / 3, (a.z + b.z + c.z) / 3);
		auto column = static_cast<int>((center.x + numberOfNodes / 2 * BSP_NODE_SIZE) / BSP_NODE_SIZE);
		auto row = static_cast<int>((center.z + numberOfNodes / 2 * BSP_NODE_SIZE) / BSP_NODE_SIZE);
		if (column > -1 && row > -1 && column < numberOfNodes && row < numberOfNodes) {
			auto nodeIndex = row * numberOfNodes + column;
			indicesParts[nodeIndex].push_back(indices[i + 0]);
			indicesParts[nodeIndex].push_back(indices[i + 1]);
			indicesParts[nodeIndex].push_back(indices[i + 2]);
		}
	}
	// store a reference to object in node list only if it actually has some geometry there
	for (auto i = 0U; i < indicesParts.size(); ++i)
		if (!indicesParts[i].empty()) {
			nodes[i].push_back(object.getHandle());
			indicesPartsIndices.push_back(i);
		}
}

void mte::MinTechEngine::shutdown() {
	numberOfHandles = 0;
}

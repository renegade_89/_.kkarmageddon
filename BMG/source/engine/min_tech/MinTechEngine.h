#pragma once

/**
* @brief Min Tech Engine 3000
*
*/

#include <application/common/utils/Singleton.h>

#include <ogl/GL/glew.h>
#include <ogl/GL/glut.h>
#include <ogl/glm/gtc/matrix_transform.hpp>

#include <cstdint>
#include <memory>
#include <vector>
#include <list>
#include <unordered_map>

#include "entities/BoundingBox.h"
#include "entities/Camera.h"
#include "interfaces/BSPDelegate.h"

namespace mte {
	class Object;
	class CollisionTestDelegate;
	class MinTechEngine : public uti::Singleton<MinTechEngine>, public BSPDelegate {
		static const double BSP_NODE_SIZE; /**< World node legth along any x or z axis (TODO: can we change it on the fly?) */
		static const double EPSILON; /**< SOme little threshold value */

	private:
		Camera camera; /**< Camera object*/

	private:
		std::unordered_map<int, std::shared_ptr<Object>> worldObjects; /**< Some structure of world objects */
		std::vector<glm::dmat4> boundingBoxesTransformations; /**< Stores up to date bounding boxes transformations */
		BoundingBox box; /**< Temporary object for bounding boxes debug output */
		bool shouldDisplayBoundingBoxes; /**< Boolean flag, set if bounding boxes debug output is needed */
		
		/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
		std::list<int> background;
		std::vector<std::list<int>> nodes; /**< For each world node we have a list of object handles*/
		/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

		int numberOfNodes; /**< Number of nodes along one axis */
		size_t numberOfHandles; /**< Number of objects created so far */
		std::vector<double> nodeInfoLookup;
		glm::dvec2 lodThresholds;
		glm::vec3 cachedLuminaryDirection;

	public:
		/**
		* @brief Performs all needed initialization
		*
		* @param width Integer value holding viewport width
		* @param height Integer value holding viewport height
		* @param fieldOfView Floating point number holding field of view (in degrees!)
		* @param frustumNear Floating point value of near clipping plane
		* @param frustumFar Floating point value of far clipping plane
		*/
		void init(int width, int height, double fieldOfView = 30.0, double frustumNear = 0.1, double frustumFar = 100000.0 * 256 * 2);

		/**
		* @brief Create terrain object, store it internally and return handle to client side
		*
		* @param heights Height map
		* @param dimensions Size of map
		* @param scale Scaling factors to generate geometry from height map
		* @return Integer value of object handle
		*/
		size_t createTerrain(const std::vector<GLfloat> &heights, const glm::ivec2 &dimensions, glm::dvec3 scale);

		/**
		* @brief Create horizon object, store it internally and return handle to client side
		*
		* @param heights Height map
		* @param dimensions Length and max height!
		* @return Integer value of object handle
		*/
		size_t createBackground(const std::vector<GLfloat> &heights, const glm::ivec2 &dimensions);

		/**
		* @brief Create sky object, store it internally and return handle to client side
		*
		* @param starfield Coords of stars in the sky
		* @param luminaryLandscape Heightmap, used to create distant objects illusion
		* @param luminaryDimensions Size of the moon
		* @param luminaryPosition Moon coords
		* @return Integer value of object handle
		*/
		size_t createSky(const std::vector<glm::vec3> &starfield, const std::vector<GLfloat> &luminaryLandscape, const glm::ivec2 &luminaryDimensions, glm::dvec3 luminaryPosition);

		/**
		* @brief Draws everything
		*
		*/
		void drawObjects();

		/**
		* @brief Clear color and depth buffer
		*
		*/
		void clear();

		/**
		* @brief Invoke back and front buffers flipping
		*
		*/
		void flush();

		/**
		* @brief Update the viewport
		*
		* @param width Viewport width
		* @param height Viewport height
		*/
		void resize(int width, int height);

		/**
		* @brief Update camera
		*
		* @param position Player's position in the world
		* @param target This is where player is looking at the moment
		* @param up Vector pointing up in the sky above 
		*/
		void setCamera(const glm::dvec3 &position, const glm::dvec3 &target, const glm::dvec3 &up);
		
		/**
		* @brief Perform all needed cleanup
		*
		*/
		void shutdown();

		/**
		* @brief Simply toggles bb debug output flag
		*
		*/
		void toggleBoundingBoxesDrawing();

		/**
		* @brief Invoked externally to perform collision test by engine, while the actual handling of each collision is performed by delegate object 
		*
		*/
		void performCollisionTest(CollisionTestDelegate& delegate);

		/**
		* @brief For a given object, and it's geometry, execute partition and store the result in output containers
		*
		* @param object Object which needs partition
		* @param vertices Collection of vertices which belong to terrain
		* @param indices Collection of triangles which are basically primitives forming the terrain
		* @param indicesParts Indices sorted by world nodes
		* @param indicesPartsIndices Marks non-empty chunks of object geometry (TODO: do we really need this?)
		*/
		virtual void executePartition(Object &object, std::pair<std::vector<glm::dvec3>, std::vector<glm::dvec3>>& vertices, std::vector<GLuint>& indices, std::vector<std::vector<GLuint>> &indicesParts, std::list<int> &indicesPartsIndices) noexcept override;

		friend class uti::Singleton<MinTechEngine>;
	};
}

using Engine = mte::MinTechEngine;
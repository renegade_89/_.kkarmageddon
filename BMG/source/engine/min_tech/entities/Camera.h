#pragma once

#include "Plane.h"

#include <ogl/glm/glm.hpp>

#include <array>

/**
* @brief Wraps the camera data and is smart detects if some bounding box is out of view frustum (so we may discard it from drawing)
*
*/

namespace mte {
	class CameraSightCheckDelegate;
	class Camera {
		int width; /**< Projection plane width */
		int height; /**< Projection plane height */
		double fieldOfView; /**< Field of view in degrees */
		double tangent;
		double zNear; /**< Near clipping plane */
		double zFar; /**< Far clipping plane */
		double zNearNegated; /**< Near clipping plane */
		double zFarNegated; /**< Far clipping plane */
		glm::mat4 perspective; /**< Perspective transformation matrix */
		glm::mat4 lookat; /**< Lookat matrix (position not stored in this matrix!) */
		glm::dvec3 target; /**< Vector pointing forward */
		glm::dvec3 up;  /**< Vector pointing upwards */
		glm::dvec3 position;  /**< Position of our camera stored separately here */

	public:
		/**
		* @brief Retrieves the projection width
		*
		* @return Projection width
		*/
		int getWidth() const {
			return width;
		}

		/**
		* @brief Sets the projection width
		*
		* @param width Projection width
		*/
		void setWidth(int width) {
			this->width = width;
		}

		/**
		* @brief Retrieves the projection height
		*
		* @return Projection height
		*/
		int getHeight() const {
			return height;
		}

		/**
		* @brief Sets the projection height
		*
		* @param height Projection height
		*/
		void setHeight(int height) {
			this->height = height;
		}

		/**
		* @brief Retrieves the field of view 
		*
		* @return Double value of field of view in degrees
		*/
		double getFieldOfView() const {
			return fieldOfView;
		}

		/**
		* @brief Sets the field of view
		*
		* @param fieldOfView Double value of field of view in degrees
		*/
		void setFieldOfView(double fieldOfView) {
			tangent = tan(fieldOfView / 180.0 * 3.145926);
			this->fieldOfView = fieldOfView;
		}

		/**
		* @brief Retrieves the near clipping plane
		*
		* @return Near clipping plane z
		*/
		double getZNear() const {
			return zNear;
		}

		/**
		* @brief Sets the near clipping plane
		*
		* @param zNear Near clipping plane z
		*/
		void setZNear(double zNear) {
			this->zNearNegated = -zNear;
			this->zNear = zNear;
		}
		
		/**
		* @brief Retrieves the far clipping plane
		*
		* @return Far clipping plane z
		*/
		double getZFar() const {
			return zFar;
		}
		
		/**
		* @brief Sets the far clipping plane
		*
		* @param zFar Far clipping plane z
		*/
		void setZFar(double zFar) {
			this->zFarNegated = -zFar;
			this->zFar = zFar;
		}
		
		/**
		* @brief Retrieves the perspective transformation
		*
		* @return Perspective matrix transfromation matrix
		*/
		const glm::mat4& getPerspective() const {
			return perspective;
		}
		
		/**
		* @brief Sets the perspective transformation
		*
		* @param perspective Perspective matrix transfromation matrix
		*/
		void setPerspective(const glm::mat4 &perspective) {
			this->perspective = perspective;
		}

		/**
		* @brief Retrieves the lookat transformation (no camera position info)
		*
		* @return Lookat transformation matrix
		*/
		const glm::mat4& getLookat() const;

		/**
		* @brief Set look at
		*
		* @param target Where are we looking 
		* @param up Points up to the heaven
		*/
		void setLookat(const glm::vec3& target, const glm::vec3& up);
		
		/**
		* @brief Retrieves the position of camera
		*
		* @return Double-precision vector of camera position
		*/
		const glm::dvec3& getPosition() const {
			return position;
		}

		/**
		* @brief Sets the position of camera
		*
		* @param position Double-precision vector of camera position
		*/
		void setPosition(const glm::dvec3 &position) {
			this->position = position;
		}

		/**
		* @brief Checks if box is at least partially inside the frustum 
		*
		* @param box Item to check 
		* return Boolean value of test result (see method name)
		*/
		bool hasInSight(CameraSightCheckDelegate& box);
	};
}
#include "Terrain.h"

#include <cassert>
#include <algorithm>
#include <vector>
#include <list>

#include <application/common/utils/Precision.h>
#include <engine/min_tech/interfaces/BSPDelegate.h>

mte::Terrain::Terrain(size_t handle, const std::vector<GLfloat> &heights, const glm::ivec2 &dimensions, glm::dvec3 scale, BSPDelegate *partitionDelegate) : Object(handle), scale(scale) {
	
	struct GLGenBuffersException : public std::exception { char const * what() const { return "glGenBuffers() failed"; } };
	struct GLBindBufferException : public std::exception { char const * what() const { return "glBindBuffer() failed"; } };
	struct GLBufferDataException : public std::exception { char const * what() const { return "glBufferData() failed"; } };
	struct GLShaderSourceException : public std::exception { char const * what() const { return "glShaderSource() failed"; } };
	struct GLCompileShaderException : public std::exception { char const * what() const { return "glCompileShader() failed"; } };
	struct GLAttachShaderException : public std::exception { char const * what() const { return "glAttachShader() failed"; } };
	struct GLLinkProgramException : public std::exception { char const * what() const { return "glLinkProgram() failed"; } };
	struct GLGetAttribLocationException : public std::exception { char const * what() const { return "glGetAttribLocation() failed"; } };
	struct GLGetUniformLocationException : public std::exception { char const * what() const { return "glGetUniformLocation() failed"; } };
	struct GLGenVertexArraysException : public std::exception { char const * what() const { return "glGenVertexArrays() failed"; } };
	struct GLBindVertexArrayException : public std::exception { char const * what() const { return "glBindVertexArray() failed"; } };
	struct GLEnableVertexAttribArrayException : public std::exception { char const * what() const { return "glEnableVertexAttribArray() failed"; } };
	struct GLVertexAttribPointerException : public std::exception { char const * what() const { return "glVertexAttribPointer() failed"; } };
	
	vertices.first.resize(dimensions[0] * dimensions[1]);
	vertices.second.resize(dimensions[0] * dimensions[1]);
	maxHeight = static_cast<float>(*std::max_element(heights.begin(), heights.end()) * scale.y);
	for (auto row = 0; row < dimensions[1]; ++row) {
		for (auto column = 0; column < dimensions[0]; ++column) {
			auto height = heights[row * dimensions[0] + column];
			glm::dvec3 coords((column - dimensions[0] / 2) * scale.x, height * scale.y, (row - dimensions[1] / 2) * scale.z);
			vertices.first[row * dimensions[0] + column] = coords;
		}
	}

	// alrighty then
	// that's good but we still need to compute the normals
	// so let's do this, for fuck's sake!
	std::vector<glm::dvec3> firstPassNormals(dimensions[0] * dimensions[1], glm::dvec3(0.0f, 1.0f, 0.0f));
	std::vector<glm::dvec3> secondPassNormals(dimensions[0] * dimensions[1], glm::dvec3(0.0f, 1.0f, 0.0f));
	for (auto row = 1; row < dimensions[1] - 1; ++row) {
		for (auto column = 1; column < dimensions[0] - 1; ++column) {
			auto& left = vertices.first[row * dimensions[0] + column - 1];
			auto& middle = vertices.first[row * dimensions[0] + column];
			auto& right = vertices.first[row * dimensions[0] + column + 1];
			auto ab = glm::normalize(left - middle);
			auto cb = glm::normalize(right - middle);
			auto result = ab + cb;
			if (fabs(result.x) < 0.0000001f && fabs(result.y) < 0.0000001f && fabs(result.z) < 0.0000001f) {
				result = glm::dvec3(0.0f, 1.0f, 0.0f);
			}

			result = glm::normalize(result);

			if (result.y < 0.0f)
				result = -result;
			firstPassNormals[row * dimensions[0] + column] = result;
		}
	}
	for (auto column = 1; column < dimensions[0] - 1; ++column) {
		for (auto row = 1; row < dimensions[1] - 1; ++row) {
			auto& upper = vertices.first[(row - 1) * dimensions[0] + column];
			auto& middle = vertices.first[row * dimensions[0] + column];
			auto& lower = vertices.first[(row + 1) * dimensions[0] + column];
			auto ab = glm::normalize(upper - middle);
			auto cb = glm::normalize(lower - middle);
			auto result = ab + cb;
			if (fabs(result.x) < 0.0000001f && fabs(result.y) < 0.0000001f && fabs(result.z) < 0.0000001f) {
				result = glm::dvec3(0.0f, 1.0f, 0.0f);
			}
			result = glm::normalize(result);

			if (result.y < 0.0f)
				result = -result;
			secondPassNormals[row * dimensions[0] + column] = result;
		}
	}
	std::transform(firstPassNormals.cbegin(), firstPassNormals.cend(), secondPassNormals.cbegin(), vertices.second.begin(),
		[](const auto& first, const auto& second) { return glm::normalize(first + second); });

	static int coefs[NUM_LODS] = { 1, 2, 4 };

	// what we have to do is to connect smoothly terrain parts, even if they have different lod
	auto minLodCoef = coefs[NUM_LODS - 1];
	for (auto row = 0; row < dimensions[1]; ++row) {
		for (auto column = 0; column < dimensions[0]; ++column) {
			auto rowModLodCoef = row % minLodCoef;
			auto columnModLodCoef = column % minLodCoef;
			if (columnModLodCoef == 0 && rowModLodCoef != 0) {
				// vertical interpolation
				auto& upper = vertices.first[(row - rowModLodCoef) * dimensions[0] + column];
				auto& lower = vertices.first[(row + (minLodCoef - rowModLodCoef)) * dimensions[0] + column];
				auto ratio = 1.0 * rowModLodCoef / minLodCoef;
				vertices.first[row * dimensions[0] + column] = upper * (1.0 - ratio) + lower * (ratio);
			}
			else if (rowModLodCoef == 0 && columnModLodCoef != 0) {
				// horizontal interpolation
				auto& left = vertices.first[row * dimensions[0] + (column - columnModLodCoef)];
				auto& right = vertices.first[row * dimensions[0] + column + (minLodCoef - columnModLodCoef)];
				auto ratio = 1.0 * columnModLodCoef / minLodCoef;
				vertices.first[row * dimensions[0] + column] = left * (1.0 - ratio) + right * (ratio);
			}
		}
	}

#ifdef _DEBUG 
	for (int level = LOD_LOW; level >= LOD_MEDIUM; --level) {
#else
	for (int level = LOD_LOW; level >= LOD_HIGH; --level) {
#endif
		
		auto width = dimensions[0] / coefs[level];
		auto height = dimensions[1] / coefs[level];
		if (coefs[level] == 1) {
			--width;
			--height;
		}

		std::vector<GLuint> indices((width) * (height) * 2 * 3);
		for (auto row = 0; row < width; ++row) {
			for (auto column = 0; column < height; ++column) {
				indices[row * (width) * 6 + column * 6 + 0] = row * coefs[level] * dimensions[0] + column * coefs[level];
				indices[row * (width) * 6 + column * 6 + 1] = row * coefs[level] * dimensions[0] + column * coefs[level] + dimensions[0] * coefs[level] + 1 * coefs[level];
				indices[row * (width) * 6 + column * 6 + 2] = row * coefs[level] * dimensions[0] + column * coefs[level] + dimensions[0] * coefs[level];
				indices[row * (width) * 6 + column * 6 + 3] = row * coefs[level] * dimensions[0] + column * coefs[level];
				indices[row * (width) * 6 + column * 6 + 4] = row * coefs[level] * dimensions[0] + column * coefs[level] + 1 * coefs[level];
				indices[row * (width) * 6 + column * 6 + 5] = row * coefs[level] * dimensions[0] + column * coefs[level] + dimensions[0] * coefs[level] + 1 * coefs[level];
			}
		}

		std::list<int> indicesPartsIndices;
		indicesParts.clear();
		partitionDelegate->executePartition(*this, vertices, indices, indicesParts, indicesPartsIndices);
		indexBufferObjectsIDs[level].resize(indicesPartsIndices.size());
		indexBufferObjectsIndicesCount[level].resize(indicesPartsIndices.size());

		if (level == LOD_MEDIUM) {
			for (auto index : indicesPartsIndices) {
				auto& primitives = indicesParts[index];
				auto lower = std::numeric_limits<double>::max();
				auto upper = std::numeric_limits<double>::min();
				for (auto index : primitives) {
					auto& vertex = vertices.first[index];
					if (lower > vertex.y)
						lower = vertex.y;
					if (upper < vertex.y)
						upper = vertex.y;
				}
				verticalBounds.emplace_back(lower, upper);
			}
		}
		if (indexBufferObjectsIDs[level].size())
			glGenBuffers(indexBufferObjectsIDs[level].size(), &indexBufferObjectsIDs[level][0]);
		if (glGetError() != GL_NO_ERROR)
			throw GLGenBuffersException();
		auto offset = 0;
		for (auto index : indicesPartsIndices) {
			bufferLookup[index] = offset;
			auto& primitives = indicesParts[index];
			// create a buffer for this chunk
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferObjectsIDs[level][offset]);
			if (glGetError() != GL_NO_ERROR)
				throw GLBindBufferException();
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, primitives.size() * sizeof(GLuint), &primitives[0], GL_STATIC_DRAW);
			auto error = glGetError();
			if (error = GL_NO_ERROR)
				throw GLBufferDataException();
			indexBufferObjectsIndicesCount[level][offset] = indicesParts[index].size();
			++offset;
		}
	}

	std::vector<GLfloat> verticesCooked;
	for (auto i = vertices.first.begin(), j = vertices.second.begin(); i != vertices.first.end(); ++i, ++j) {
		auto offset = verticesCooked.size();
		verticesCooked.resize(offset + 6);
		uti::doubleToFloats(i->x, verticesCooked[offset + 0], verticesCooked[offset + 3]);
		uti::doubleToFloats(i->y, verticesCooked[offset + 1], verticesCooked[offset + 4]);
		uti::doubleToFloats(i->z, verticesCooked[offset + 2], verticesCooked[offset + 5]);
		verticesCooked.push_back(static_cast<float>(j->x));
		verticesCooked.push_back(static_cast<float>(j->y));
		verticesCooked.push_back(static_cast<float>(j->z));
	}

	glGenBuffers(1, &vertexBufferObjectID);
	if (glGetError() != GL_NO_ERROR)
		throw GLGenBuffersException();
	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObjectID);
	if (glGetError() != GL_NO_ERROR)
		throw GLBindBufferException();
	glBufferData(GL_ARRAY_BUFFER, verticesCooked.size() * sizeof(verticesCooked[0]), &verticesCooked[0], GL_STATIC_DRAW);
	if (glGetError() != GL_NO_ERROR)
		throw GLBufferDataException();

	// shaders related
	auto vertexShaderSourceCode =
		"attribute vec3 a_coord_high;\n"
		"attribute vec3 a_coord_low;\n"
		"attribute vec3 a_normal;\n"
		"varying vec3 v_color;\n"
		"varying vec3 v_secondary_color;\n"
		"varying float v_dist;\n"
		"varying vec3 v_normal;\n"
		"varying vec3 v_sight;\n"
		"uniform float u_max_height;\n"
		"uniform mat4 u_v;\n"
		"uniform mat4 u_p;\n"
		"uniform vec3 u_camera_high;\n"
		"uniform vec3 u_camera_low;\n"
		"uniform vec3 u_light_direction;\n"
		"void main() {\n"
		"  vec3 high_dif = vec3(a_coord_high.xyz - u_camera_high);\n"
		"  vec3 low_dif = vec3(a_coord_low.xyz - u_camera_low);\n"
		"  vec4 coord = vec4(high_dif + low_dif, 1.0);\n"
		"  vec4 viewCoord = u_v * coord;\n"
		"  gl_Position = u_p * viewCoord;\n"
		"  v_color = 1.0 - a_coord_high.y / u_max_height + a_coord_low.y / u_max_height;\n"
		"  v_secondary_color = 0.5 * fract(sin(dot(a_coord_low.xy ,vec2(12.9898,78.233))) * 43758.5453);\n"
		"  v_dist = max(-viewCoord.z / 50000000.0, 0.0);\n"
		"  v_normal = a_normal;\n"
		"  vec3 sight_high = a_coord_high - u_camera_high;\n"
		"  vec3 sight_low = a_coord_low - u_camera_low;\n"
		"  v_sight = sight_high + sight_low;\n"
		"}\n";
	vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShaderID, 1, &vertexShaderSourceCode, NULL);
	if (glGetError() != GL_NO_ERROR)
		throw GLShaderSourceException();
	glCompileShader(vertexShaderID);
	if (glGetError() != GL_NO_ERROR)
		throw GLCompileShaderException();
	//log(vertexShaderID);
	auto  fragmentShaderSourceCode =
		"varying vec3 v_color;\n"
		"varying vec3 v_secondary_color;\n"
		"varying float v_dist;\n"
		"varying vec3 v_normal;\n"
		"varying vec3 v_sight;\n"
		"uniform vec3 u_light_direction;\n"
		"uniform mat4 u_v;\n"
		"uniform int u_edge_mode;\n"
		"void main() {\n"
		"  if (u_edge_mode == 1)\n"
		"  {\n"
		"    float result = dot(v_normal, normalize(v_sight));\n"
		"    if (result > -0.05 && result < 0.05)\n"
		"      gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);\n"
		"    else\n"
		"      gl_FragColor = vec4(0.8, 0.8, 0.8, 1.0);\n"
		"  }\n"
		"  else\n"
		"  {\n"
		"    float intensity = max(0.0, dot(v_normal, u_light_direction)) + smoothstep(0.91, 1.0, v_color.r);\n"
		"    float color = v_secondary_color + clamp(fract(sin(dot(v_normal.xy ,vec2(12.9898,78.233))) * 43758.5453), 0.3, 0.4);\n"
		"    vec3 reflected = normalize(reflect(v_sight, v_normal));\n"
		"    vec4 ambientAndDiffuse = vec4(0.4, 0.4, 0.4, 1.0) + intensity * mix(vec4(1.0, 1.0, 1.0, 1.0), vec4(color, color, color, 1.0), max(0.0, 0.3 - min(1.0, v_dist)));\n"
		"    vec4 specular = pow(max(0.0, dot(reflected, u_light_direction)), 50.0) * vec4(0.4, 0.4, 0.4, 1.0);\n"
		"    gl_FragColor = ambientAndDiffuse + mix(specular, vec4(0.0, 0.0, 0.0, 1.0), max(0.0, 0.7 - min(1.0, v_dist)));\n"
		"  }\n"
		"}\n";
	fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShaderID, 1, &fragmentShaderSourceCode, NULL);
	if (glGetError() != GL_NO_ERROR)
		throw GLShaderSourceException();
	glCompileShader(fragmentShaderID);
	if (glGetError() != GL_NO_ERROR)
		throw GLCompileShaderException();
	//log(fragmentShaderID);
	shaderProgramID = glCreateProgram();
	glAttachShader(shaderProgramID, vertexShaderID);
	if (glGetError() != GL_NO_ERROR)
		throw GLCompileShaderException();
	glAttachShader(shaderProgramID, fragmentShaderID);
	if (glGetError() != GL_NO_ERROR)
		throw GLCompileShaderException();
	glLinkProgram(shaderProgramID);
	if (glGetError() != GL_NO_ERROR)
		throw GLLinkProgramException();
	int isLinkedSuccessfully;
	glGetProgramiv(shaderProgramID, GL_LINK_STATUS, &isLinkedSuccessfully);
	if (!isLinkedSuccessfully) {
		if (glGetError() != GL_NO_ERROR)
			throw GLLinkProgramException();
	}
	auto vertexHighAttributeName = "a_coord_high";
	auto vertexLowAttributeName = "a_coord_low";
	vertexHighAttributeID = glGetAttribLocation(shaderProgramID, vertexHighAttributeName);
	vertexLowAttributeID = glGetAttribLocation(shaderProgramID, vertexLowAttributeName);

	auto normalAttributeName = "a_normal";
	normalAttributeID = glGetAttribLocation(shaderProgramID, normalAttributeName);
	if (glGetError() != GL_NO_ERROR)
		throw GLGetAttribLocationException();
	if (normalAttributeID == -1) {
		throw GLGetAttribLocationException();
	}
	
	viewUniformID = glGetUniformLocation(shaderProgramID, "u_v");
	projectionUniformID = glGetUniformLocation(shaderProgramID, "u_p");
	maxHeightUniformID = glGetUniformLocation(shaderProgramID, "u_max_height");
	lightDirectionUniformID = glGetUniformLocation(shaderProgramID, "u_light_direction");
	edgeModeUniformID = glGetUniformLocation(shaderProgramID, "u_edge_mode");

	if (glGetError() != GL_NO_ERROR)
		throw GLGetUniformLocationException();
	if (viewUniformID == -1) {
		throw GLGetUniformLocationException();
	}
	if (projectionUniformID == -1) {
		throw GLGetUniformLocationException();
	}

	auto cameraHighUniformName = "u_camera_high";
	cameraHighUniformID = glGetUniformLocation(shaderProgramID, cameraHighUniformName);
	auto cameraLowUniformName = "u_camera_low";
	cameraLowUniformID = glGetUniformLocation(shaderProgramID, cameraLowUniformName);

	glGenVertexArrays(1, &vertexArrayObjectID);
	if (glGetError() != GL_NO_ERROR)
		throw GLGenVertexArraysException();
	glBindVertexArray(vertexArrayObjectID);
	if (glGetError() != GL_NO_ERROR)
		throw GLBindVertexArrayException();
	glEnableVertexAttribArray(vertexHighAttributeID);
	if (glGetError() != GL_NO_ERROR)
		throw GLEnableVertexAttribArrayException();
	glEnableVertexAttribArray(vertexLowAttributeID);
	if (glGetError() != GL_NO_ERROR)
		throw GLEnableVertexAttribArrayException();
	glEnableVertexAttribArray(normalAttributeID);
	if (glGetError() != GL_NO_ERROR)
		throw GLEnableVertexAttribArrayException();
	
	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObjectID);
	if (glGetError() != GL_NO_ERROR)
		throw GLBindBufferException();
	glVertexAttribPointer(vertexHighAttributeID, NUM_AXIS, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * (NUM_AXIS * 2 + NUM_CHANNELS), 0);
	if (glGetError() != GL_NO_ERROR)
		throw GLVertexAttribPointerException();
	glVertexAttribPointer(vertexLowAttributeID, NUM_AXIS, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * (NUM_AXIS * 2 + NUM_CHANNELS), (const GLvoid*)(NUM_AXIS * sizeof(GLfloat)));
	if (glGetError() != GL_NO_ERROR)
		throw GLVertexAttribPointerException();
	glVertexAttribPointer(normalAttributeID, NUM_CHANNELS, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * (NUM_AXIS * 2 + NUM_CHANNELS), (const GLvoid*)(NUM_AXIS * 2 * sizeof(GLfloat)));
	if (glGetError() != GL_NO_ERROR)
		throw GLVertexAttribPointerException();

	// bsp info accessed via smart pointer, so...
	class BSPInfoDummyProxy : public BSPInfo {
	public:
		Terrain& terrain;
		BSPInfoDummyProxy(Terrain& terrain) : terrain(terrain) {};
		virtual unsigned int numberOfTriangles(int nodeIndex) override { 
			return terrain.numberOfTriangles(nodeIndex); 
		}
		virtual bool triangleAtIndex(int nodeIndex, int index, glm::dvec3& a, glm::dvec3& b, glm::dvec3& c) override {
			return terrain.triangleAtIndex(nodeIndex, index, a, b, c);
		}
		virtual glm::dvec2 getVerticalBounds(int node) override {
			return terrain.getVerticalBounds(node);
		}
	};
	bspInfo = std::shared_ptr<BSPInfo>(new BSPInfoDummyProxy(*this));

	glBindVertexArray(0);
}

glm::dvec2 mte::Terrain::getVerticalBounds(int node) {
	return verticalBounds[bufferLookup[node]];
}

void mte::Terrain::draw(const glm::mat4 &projection, const glm::mat4 &view, const glm::vec3 &luminaryDirection, const glm::dvec3 &cameraPosition, int node, int lod) noexcept {

#ifdef _DEBUG 
	if (lod != LOD_MEDIUM && lod != LOD_LOW)
		lod = LOD_MEDIUM;
#endif
	
	glUseProgram(shaderProgramID);
	glBindVertexArray(vertexArrayObjectID);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObjectID);

	glUniformMatrix4fv(viewUniformID, 1, GL_FALSE, &view[0][0]);
	glUniformMatrix4fv(projectionUniformID, 1, GL_FALSE, &projection[0][0]);

	glm::vec3 cameraHigh;
	glm::vec3 cameraLow;
	uti::doubleToFloats(cameraPosition[0], cameraHigh[0], cameraLow[0]);
	uti::doubleToFloats(cameraPosition[1], cameraHigh[1], cameraLow[1]);
	uti::doubleToFloats(cameraPosition[2], cameraHigh[2], cameraLow[2]);
	glUniform3fv(cameraHighUniformID, 1, &cameraHigh[0]);
	glUniform3fv(cameraLowUniformID, 1, &cameraLow[0]);

	glUniform1f(maxHeightUniformID, maxHeight);

	glUniform3fv(lightDirectionUniformID, 1, &luminaryDirection[0]);

	glUniform1i(edgeModeUniformID, 0);

	// draw a chunk of terrain
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferObjectsIDs[lod][bufferLookup[node]]);
	glDrawElements(GL_TRIANGLES, indexBufferObjectsIndicesCount[lod][bufferLookup[node]], GL_UNSIGNED_INT, 0);
	
	glBindVertexArray(0);
	glUseProgram(0);
}

mte::Terrain::~Terrain() {
	vertexHighAttributeID = 0;
	vertexLowAttributeID = 0;
	//colorAttributeID = 0;
	maxHeightUniformID = 0;
	viewUniformID = 0;
	projectionUniformID = 0;
	glDeleteShader(fragmentShaderID);
	assert(("glDeleteShader() error", glGetError() == GL_NO_ERROR));
	fragmentShaderID = 0;
	glDeleteShader(vertexShaderID);
	assert(("glDeleteShader() error", glGetError() == GL_NO_ERROR));
	vertexShaderID = 0;
	glDeleteProgram(shaderProgramID);
	assert(("glDeleteProgram() error", glGetError() == GL_NO_ERROR));
	shaderProgramID = 0;
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	assert(("glBindBuffer() error", glGetError() == GL_NO_ERROR));
	
	for (int level = LOD_HIGH; level < NUM_LODS; ++level)
		if (indexBufferObjectsIDs[level].size())
			glDeleteBuffers(indexBufferObjectsIDs[level].size(), &indexBufferObjectsIDs[level][0]);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	assert(("glBindBuffer() error", glGetError() == GL_NO_ERROR));
	glDeleteBuffers(1, &vertexBufferObjectID);
	assert(("glDeleteBuffers() error", glGetError() == GL_NO_ERROR));
	vertexBufferObjectID = 0;
}

unsigned int mte::Terrain::numberOfTriangles(int nodeIndex) {
	return indicesParts[nodeIndex].size() / 3;
}

bool mte::Terrain::triangleAtIndex(int nodeIndex, int index, glm::dvec3& a, glm::dvec3& b, glm::dvec3& c) {
	auto& primitives = indicesParts[nodeIndex];
	int offset = (index << 1) + index;
	if (primitives[offset + 0] == primitives[offset + 1] && primitives[offset + 1] == primitives[offset + 2])
		return false;
	a = vertices.first[primitives[offset + 0]];
	b = vertices.first[primitives[offset + 1]];
	c = vertices.first[primitives[offset + 2]];
	return true;
}

std::shared_ptr<mte::BSPInfo> mte::Terrain::getBSPInfo() {
	return bspInfo;
}
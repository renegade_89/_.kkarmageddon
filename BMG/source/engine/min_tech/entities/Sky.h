#pragma once

#include <ogl/GL/glew.h>
#include <ogl/GL/glut.h>
#include <ogl/glm/gtc/matrix_transform.hpp>

#include "Object.h"

#include <vector>

/**
* @brief Sky currently consists of a star field and a moon
*
*/

namespace mte {
	class Sky : public Object {
	private:
		GLuint starsVertexBufferObjectID; /**< Stars coords buffer id */
		GLint starsVertexAttributeID; /**< Coords attribute location/index */
		GLuint starsVertexShaderID; /**< ID of a vertex shader used for stars drawing */
		GLuint starsFragmentShaderID; /**< ID of a fragment shader used for stars drawing */
		GLuint starsShaderProgramID; /**< ID of a shader program used for stars drawing */
		GLint starsViewProjectionUniformLocation; /**< Index/location of a view-projection transformation (stars positions are fixed) */
		unsigned int numberOfStars; /**< Number of stars up in the sky above */

		GLuint luminaryVertexArrayObjectID; /**< Handle to moon vao: vao saves vertex attributes settings */
		GLuint luminaryVertexBufferObjectID; /**< Moon quad vertice buffer id */
		GLuint luminaryIndexBufferObjectID; /**< Moon quad indices buffer id (todo: maybe using indices is an overkill here) */
		GLint luminaryVertexAttributeID; /**< Moon quad vertex coord attribute location/index */
		GLint luminaryTextureCoordinateAttributeID; /**< Moon quad vertex texture coord attribute location/index */
		GLuint luminaryVertexShaderID; /**< ID of a vertex shader used for moon drawing */
		GLuint luminaryFragmentShaderID; /**< ID of a fragment shader used for moon drawing */
		GLuint luminaryShaderProgramID; /**< ID of a shader program used for moon drawing */
		GLint luminaryViewProjectionUniformLocation; /**< Index/location of a view-projection transformation (moon position fixed) */
		GLuint luminaryTextureID; /**< ID of a texture used for a moon */
		GLint luminaryTextureUniformLocation; /**< Texture coordinate attribute location/index */
		unsigned int numberOfLuminaryIndices;  /**< Number of indices in moon index buffer */

	public:
		/**
		* @brief Constructs sky parts: star field and moon
		*
		* @param handle ID of a future object
		* @param starfield Stars coordinates provided from client side
		* @param luminaryLandscape Texture data for a moon provided from calling side
		* @param luminaryDimensions Moon size
		* @param luminaryPosition Moon position
		*/
		Sky(size_t handle, const std::vector<glm::vec3> &starfield, const std::vector<GLfloat> &luminaryLandscape, const glm::ivec2 &luminaryDimensions, glm::vec3 luminaryPosition);

		/**
		* @brief Draw the terrain
		*
		* @param projection Projection transformation matrix
		* @param view View transformation matrix (terrain doesn't have any model transformation)
		* @param luminaryPosition Position of our source of light
		* @param cameraPosition Camera position coordinates passed separately
		* @param node World node index to draw only part of terrain which resides in that particular world index
		* @param lod Desired level of detail in object rendering
		*/
		virtual void draw(const glm::mat4 &projection, const glm::mat4 &view, const glm::vec3 &luminaryDirection, const glm::dvec3 &cameraPosition, int node = -1, int lod = LOD_HIGH) noexcept override;
		
		/**
		* @brief Provides bsp data for this object (null in this case)
		*
		* @return Shared pointer to a defined structure
		*/
		virtual std::shared_ptr<BSPInfo> getBSPInfo() override;

		/**
		* @brief Performs all needed cleunup for buffers, shaders etc
		*
		*/
		virtual ~Sky();
	};
}
#pragma once

#include <ogl/GL/glew.h>
#include <ogl/GL/glut.h>
#include <ogl/glm/gtc/matrix_transform.hpp>

#include "Object.h"

#include <vector>

/**
* @brief Fake distant objects: "mountains", and "gradient" transition between our terrain and "mountains"
*
*/

namespace mte {
	class Background : public Object {
	private:
		GLuint vertexBufferObjectID; /**< Handle to vertex buffer object */
		GLuint indexBufferObjectID; /**< Handle to index buffer object */
		GLuint vertexArrayObjectID; /**< Saves attributes settings */
		GLint indicesCount; /**< Number of indices to process */
		GLint vertexAttributeID; /**< Coords attribute location*/
		GLint textureCoordinateAttributeID; /**< Tex coords attribute location */
		GLuint vertexShaderID; /**< Vertex shader id */
		GLuint fragmentShaderID; /**< Fragment shader id */
		GLuint shaderProgramID; /**< Texture object id */
		GLuint textureID; /**< Texture object id */
		GLint textureUniformLocation; /**< Texture uniform location */
		GLint horizontalOffsetUniformLocation; /**< Offset uniform which is calculated based on our horizontal angle of view */
		GLint verticalOffsetUniformLocation; /**< Offset uniform calculated based on out vertical angle of view */
		float viewDistance; /**< Also used for vertical offset calculation */

	private:
		GLuint gradientVertexBufferObjectID; /**< Handle to vertex buffer object */
		GLuint gradientIndexBufferObjectID; /**< Handle to index buffer object */
		GLuint gradientVertexArrayObjectID; /**< Handle to VAO */
		GLint gradientIndicesCount; /**< Handle to vertex buffer object */
		GLint gradientVertexAttributeID; /**< Coords attribute index */
		GLuint gradientVertexShaderID; /**< Vertex shader id for field drawing */
		GLuint gradientFragmentShaderID; /**< Fragment shader id for field drawing */
		GLuint gradientShaderProgramID; /**< Shader program id for field drawing */
		GLint gradientVerticalOffsetUniformLocation; /**< Don't care about horizontal offset, but vertical still matters */

	public:
		/**
		* @brief Performs all needed initialization for future drawing loop
		*
		* @param handle ID of a future object
		* @param heights Heightmap of distant slopes and cliffs
		* @param dimensions Heightmap length x vertical size of the whole graphical thing
		* @param heightRatio Vertical size divided by screen height (useful when working with some parametric coords)
		* @param widthRatio Screen width divided by the whole horizon strip length (useful when working with some parametric coords)
		* @param fov Field of view, floating point value used to calculate view distance and some adjustments
		*/
		Background(size_t handle, std::vector<GLfloat> heights, const glm::ivec2 &dimensions, float heightRatio, float widthRatio, float fov);

		/**
		* @brief Draw the terrain
		*
		* @param projection Projection transformation matrix
		* @param view View transformation matrix (terrain doesn't have any model transformation)
		* @param luminaryPosition Position of our source of light
		* @param cameraPosition Camera position coordinates passed separately
		* @param node World node index to draw only part of terrain which resides in that particular world index
		* @param lod Desired level of detail in object rendering
		*/
		virtual void draw(const glm::mat4 &projection, const glm::mat4 &view, const glm::vec3 &luminaryDirection, const glm::dvec3 &cameraPosition, int node = -1, int lod = LOD_HIGH) noexcept override;
		
		/**
		* @brief Provides bsp data for this object (null in this case)
		*
		* @return Shared pointer to a defined structure
		*/
		virtual std::shared_ptr<BSPInfo> getBSPInfo() override;

		/**
		* @brief Performs all needed cleunup for buffers, shaders etc
		*
		*/
		virtual ~Background();
	};
}
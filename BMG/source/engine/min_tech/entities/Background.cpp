#include "Background.h"

#include <iostream>

mte::Background::Background(size_t handle, std::vector<GLfloat> heights, const glm::ivec2 &dimensions, float heightRatio, float widthRatio, float fov) : Object(handle) {
	viewDistance = 1 / tan(fov / 2.0);

	// vertices

	// a little adjustment, since we're combining perspective and parallel projections in a scene
	auto adjustment = (0.5f / (viewDistance * tan(fov / 4)) - 1.0f) * 0.5f;
	std::vector<float> vertices = {
		-1.00f, -heightRatio, 0.0f, -adjustment, 1.0f,
		-1.00f, +heightRatio, 0.0f, -adjustment, 0.0f,
		+1.00f, +heightRatio, 0.0f, widthRatio + adjustment, 0.0f,
		+1.00f, -heightRatio, 0.0f, widthRatio + adjustment, 1.0f
	};

	// indices
	std::vector<GLuint> indices = {
		0, 1, 2, 0, 2, 3
	};

	// load
	glGenBuffers(1, &vertexBufferObjectID);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObjectID);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float), &vertices[0], GL_STATIC_DRAW);
	glGenBuffers(1, &indexBufferObjectID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferObjectID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLint), &indices[0], GL_STATIC_DRAW);
	indicesCount = indices.size();

	// texture
	// a little hack to make smooth transition between the end point and start point during wrapping
	float temp = (heights[dimensions.x - 1] + heights[0]) / 2;
	heights[dimensions.x - 3] = temp;
	heights[dimensions.x - 2] = temp;
	heights[dimensions.x - 1] = temp;
	heights[0] = temp;
	heights[1] = temp;
	heights[2] = temp;
	auto blur = [&heights, &dimensions](int i) -> float {
		return  heights[i % dimensions.x] * 0.55f +
			heights[(i - 1) % dimensions.x] * 0.11f +
			heights[(i + 1) % dimensions.x] * 0.11f +
			heights[(i - 2) % dimensions.x] * 0.05f +
			heights[(i + 2) % dimensions.x] * 0.05f +
			heights[(i - 3) % dimensions.x] * 0.03f +
			heights[(i + 3) % dimensions.x] * 0.03f +
			heights[(i - 4) % dimensions.x] * 0.02f +
			heights[(i + 4) % dimensions.x] * 0.02f +
			heights[(i - 5) % dimensions.x] * 0.01f +
			heights[(i + 5) % dimensions.x] * 0.01f +
			heights[(i - 6) % dimensions.x] * 0.005f +
			heights[(i + 6) % dimensions.x] * 0.005f;
	};
	for (int pass = 0; pass < 6; ++pass) {
		float bE = blur(dimensions.x - 7);
		float bF = blur(dimensions.x - 6);
		float b0 = blur(dimensions.x - 5);
		float b1 = blur(dimensions.x - 4);
		float b2 = blur(dimensions.x - 3);
		float b3 = blur(dimensions.x - 2);
		float b4 = blur(dimensions.x - 1);
		float b5 = blur(dimensions.x - 0);
		float b6 = blur(dimensions.x + 1);
		float b7 = blur(dimensions.x + 2);
		float b8 = blur(dimensions.x + 3);
		float b9 = blur(dimensions.x + 4);
		float bA = blur(dimensions.x + 5);
		float bB = blur(dimensions.x + 6);
		float bC = blur(dimensions.x + 7);
		heights[dimensions.x - 7] = bE;
		heights[dimensions.x - 6] = bF;
		heights[dimensions.x - 5] = b0;
		heights[dimensions.x - 4] = b1;
		heights[dimensions.x - 3] = b2;
		heights[dimensions.x - 2] = b3;
		heights[dimensions.x - 1] = b4;
		heights[0] = b5;
		heights[1] = b6;
		heights[2] = b7;
		heights[3] = b8;
		heights[4] = b9;
		heights[5] = bA;
		heights[6] = bB;
		heights[7] = bC;
	}
	// end of hack
	std::vector<GLfloat> data(dimensions.x * 3);
	for (auto column = 0U; column < dimensions.x; ++column) {
		data[column * 3 + 0] = heights[column];
		data[column * 3 + 1] = heights[column];
		data[column * 3 + 2] = heights[column];
	}
	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_1D, textureID);
	glTexImage1D(GL_TEXTURE_1D, 0, GL_RGB, dimensions.x, 0, GL_RGB, GL_FLOAT, &data[0]);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	// shaders
	auto vertexShaderSourceCode =
		"attribute vec3 a_coord;\n"
		"attribute vec2 a_tex_coord;\n"
		"uniform float u_h_offset;\n"
		"uniform float u_v_offset;\n"
		"varying vec2 v_tex_coord;\n"
		"void main() {\n"
		"  gl_Position = vec4(a_coord.x, a_coord.y + u_v_offset, a_coord.z, 1.0);\n"
		"  v_tex_coord = a_tex_coord + vec2(u_h_offset, 0.0);\n"
		"}\n";
	vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShaderID, 1, &vertexShaderSourceCode, NULL);
	glCompileShader(vertexShaderID);
	auto fragmentShaderSourceCode =
		"varying vec2 v_tex_coord;\n"
		"uniform sampler1D u_tex;\n"
		"void main() {\n"
		"  vec4 color = texture(u_tex, v_tex_coord.x);\n"
		"  if (color.r > v_tex_coord.y)\n"
		"    discard;"
		"  gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);\n"
		"}\n";
	fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShaderID, 1, &fragmentShaderSourceCode, NULL);
	glCompileShader(fragmentShaderID);
	shaderProgramID = glCreateProgram();
	glAttachShader(shaderProgramID, vertexShaderID);;
	glAttachShader(shaderProgramID, fragmentShaderID);
	glLinkProgram(shaderProgramID);
	auto isLinkedSuccessfully = 0;
	glGetProgramiv(shaderProgramID, GL_LINK_STATUS, &isLinkedSuccessfully);
	if (!isLinkedSuccessfully) {
		exit(EXIT_FAILURE);
	}

	// locations
	auto vertexAttributeName = "a_coord";
	vertexAttributeID = glGetAttribLocation(shaderProgramID, vertexAttributeName);
	auto textureCoordinateAttributeName = "a_tex_coord";
	textureCoordinateAttributeID = glGetAttribLocation(shaderProgramID, textureCoordinateAttributeName);
	textureUniformLocation = glGetUniformLocation(shaderProgramID, "u_tex");
	horizontalOffsetUniformLocation = glGetUniformLocation(shaderProgramID, "u_h_offset");
	verticalOffsetUniformLocation = glGetUniformLocation(shaderProgramID, "u_v_offset");

	// vao
	glGenVertexArrays(1, &vertexArrayObjectID);
	glBindVertexArray(vertexArrayObjectID);
	glEnableVertexAttribArray(vertexAttributeID);
	glEnableVertexAttribArray(textureCoordinateAttributeID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferObjectID);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObjectID);
	glVertexAttribPointer(vertexAttributeID, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * (3 + 2), 0);
	glVertexAttribPointer(textureCoordinateAttributeID, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * (3 + 2), (const GLvoid*)(3 * sizeof(float)));

	// okay, we're done with those distant shapes, but we still need a smooth transition
	// between the terrain and the horizon

	// gradient quad vertices
	const float FAR_AWAY = -10000.0f;
	std::vector<float> gradientVertices = {
		-1.0f, FAR_AWAY, 0.0f,
		-1.0f, -heightRatio, 0.0f,
		+1.0f, -heightRatio, 0.0f,
		+1.0f, FAR_AWAY, 0.0f
	};

	// gradient object indices
	std::vector<GLuint> gradientIndices = {
		0, 1, 2, 0, 2, 3
	};

	// send the data to video card
	glGenBuffers(1, &gradientVertexBufferObjectID);
	glBindBuffer(GL_ARRAY_BUFFER, gradientVertexBufferObjectID);
	glBufferData(GL_ARRAY_BUFFER, gradientVertices.size() * sizeof(float), &gradientVertices[0], GL_STATIC_DRAW);
	glGenBuffers(1, &gradientIndexBufferObjectID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gradientIndexBufferObjectID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, gradientIndices.size() * sizeof(GLint), &gradientIndices[0], GL_STATIC_DRAW);
	gradientIndicesCount = gradientIndices.size();

	// shaders
	auto gradientVertexShaderSourceCode =
		"attribute vec3 a_coord;\n"
		"uniform float u_v_offset;\n"
		"void main() {\n"
		"  gl_Position = vec4(a_coord.x, a_coord.y + u_v_offset, a_coord.z, 1.0);\n"
		"}\n";
	gradientVertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(gradientVertexShaderID, 1, &gradientVertexShaderSourceCode, NULL);
	glCompileShader(gradientVertexShaderID);
	auto gradientFragmentShaderSourceCode =
		"void main() {\n"
		"  gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);\n"
		"}\n";
	gradientFragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(gradientFragmentShaderID, 1, &gradientFragmentShaderSourceCode, NULL);
	glCompileShader(gradientFragmentShaderID);
	gradientShaderProgramID = glCreateProgram();
	glAttachShader(gradientShaderProgramID, gradientVertexShaderID);;
	glAttachShader(gradientShaderProgramID, gradientFragmentShaderID);
	glLinkProgram(gradientShaderProgramID);
	isLinkedSuccessfully = 0;
	glGetProgramiv(gradientShaderProgramID, GL_LINK_STATUS, &isLinkedSuccessfully);
	if (!isLinkedSuccessfully) {
		exit(EXIT_FAILURE);
	}

	// locations
	auto gradientVertexAttributeName = "a_coord";
	gradientVertexAttributeID = glGetAttribLocation(gradientShaderProgramID, gradientVertexAttributeName);
	gradientVerticalOffsetUniformLocation = glGetUniformLocation(gradientShaderProgramID, "u_v_offset");

	// vao
	glGenVertexArrays(1, &gradientVertexArrayObjectID);
	glBindVertexArray(gradientVertexArrayObjectID);
	glEnableVertexAttribArray(gradientVertexAttributeID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gradientIndexBufferObjectID);
	glBindBuffer(GL_ARRAY_BUFFER, gradientVertexBufferObjectID);
	glVertexAttribPointer(gradientVertexAttributeID, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 3, 0);

	glBindVertexArray(0);
}

void mte::Background::draw(const glm::mat4 &projection, const glm::mat4 &view, const glm::vec3 &luminaryDirection, const glm::dvec3 &cameraPosition, int node, int lod) noexcept {
	// distant shapes drawing
	glUseProgram(shaderProgramID);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_1D, textureID);
	glUniform1i(textureUniformLocation, 0);
	// compute offsets
	float x = view[0][2];
	float y = view[1][2];
	float z = view[2][2];
	// yaw
	auto yaw = atan2(x, z);
	auto horizontalOffset = - yaw / (2.0f * 3.1459263f);
	glUniform1f(horizontalOffsetUniformLocation, horizontalOffset);
	// pitch
	auto a = sqrt(1 - y * y);
	auto b = y;
	auto verticalOffset = viewDistance * b / a;
	glUniform1f(verticalOffsetUniformLocation, verticalOffset);
	glBindVertexArray(vertexArrayObjectID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferObjectID);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObjectID);
	glDrawElements(GL_TRIANGLES, indicesCount, GL_UNSIGNED_INT, 0);

	// ground gradient drawing
	glUseProgram(gradientShaderProgramID);
	glUniform1f(gradientVerticalOffsetUniformLocation, verticalOffset);
	glBindVertexArray(gradientVertexArrayObjectID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gradientIndexBufferObjectID);
	glBindBuffer(GL_ARRAY_BUFFER, gradientVertexBufferObjectID);
	glDrawElements(GL_TRIANGLES, gradientIndicesCount, GL_UNSIGNED_INT, 0);

	// reset
	glBindVertexArray(0);
	glUseProgram(0);
}

mte::Background::~Background() {
	vertexAttributeID = 0;
	glDeleteTextures(1, &textureID);
	glDeleteShader(fragmentShaderID);
	assert(("glDeleteShader() error", glGetError() == GL_NO_ERROR));
	fragmentShaderID = 0;
	glDeleteShader(vertexShaderID);
	assert(("glDeleteShader() error", glGetError() == GL_NO_ERROR));
	vertexShaderID = 0;
	glDeleteProgram(shaderProgramID);
	assert(("glDeleteProgram() error", glGetError() == GL_NO_ERROR));
	shaderProgramID = 0;
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	assert(("glBindBuffer() error", glGetError() == GL_NO_ERROR));
	glDeleteBuffers(1, &indexBufferObjectID);
	assert(("glDeleteBuffers() error", glGetError() == GL_NO_ERROR));
	indexBufferObjectID = 0;
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	assert(("glBindBuffer() error", glGetError() == GL_NO_ERROR));
	glDeleteBuffers(1, &vertexBufferObjectID);
	assert(("glDeleteBuffers() error", glGetError() == GL_NO_ERROR));
	vertexBufferObjectID = 0;
	indicesCount = 0;

	gradientVertexAttributeID = 0;
	glDeleteShader(gradientFragmentShaderID);
	assert(("glDeleteShader() error", glGetError() == GL_NO_ERROR));
	gradientFragmentShaderID = 0;
	glDeleteShader(gradientVertexShaderID);
	assert(("glDeleteShader() error", glGetError() == GL_NO_ERROR));
	gradientVertexShaderID = 0;
	glDeleteProgram(gradientShaderProgramID);
	assert(("glDeleteProgram() error", glGetError() == GL_NO_ERROR));
	gradientShaderProgramID = 0;
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	assert(("glBindBuffer() error", glGetError() == GL_NO_ERROR));
	glDeleteBuffers(1, &gradientIndexBufferObjectID);
	assert(("glDeleteBuffers() error", glGetError() == GL_NO_ERROR));
	gradientIndexBufferObjectID = 0;
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	assert(("glBindBuffer() error", glGetError() == GL_NO_ERROR));
	glDeleteBuffers(1, &gradientVertexBufferObjectID);
	assert(("glDeleteBuffers() error", glGetError() == GL_NO_ERROR));
	gradientVertexBufferObjectID = 0;
	gradientIndicesCount = 0;
}

std::shared_ptr<mte::BSPInfo> mte::Background::getBSPInfo() {
	return nullptr;
}
#pragma once

#include "Plane.h"

mte::Plane::Plane() : a(0.0), b(0.0), c(0.0), d(0.0) {

}

mte::Plane::Plane(const glm::dvec3& point, const glm::dvec3& normal) : a(normal.x), b(normal.y), c(normal.z) {
	d = -(a * point.x + b * point.y + c * point.z);
}

mte::Plane::Plane(const glm::dvec3& l, const glm::dvec3& m, const glm::dvec3& n) {
	auto normal = glm::cross(m - l, n - l);
	a = -normal.x;
	b = -normal.y;
	c = -normal.z;
	d = -(a * n.x + b * n.y + c * n.z);
}

double mte::Plane::test(const glm::dvec3 point) const {
	return a * point.x + b * point.y + c * point.z + d;
}

#include "BoundingBox.h"

#include <application/common/utils/Precision.h>

mte::BoundingBox::BoundingBox() {
	vertices.resize(8);
	
	std::vector<GLint> indices = {
		0, 1, 2, 0, 2, 3,
		4, 0, 3, 4, 3, 7,
		7, 3, 2, 7, 2, 6,
		6, 2, 1, 6, 1, 5,
		5, 1, 0, 5, 0, 4,
		5, 4, 7, 5, 7, 6
	};
}

unsigned int mte::BoundingBox::numberOfVertices() const {
	return vertices.size();
}

const glm::dvec3& mte::BoundingBox::vertexAtIndex(unsigned int index) const {
	return vertices[index];
}

void mte::BoundingBox::transform(const glm::dmat4 &model) {
	modelTransformation = model;
	static std::vector<glm::dvec4> data = {
		glm::dvec4(-0.5, +0.5, +0.5, 1.0),
		glm::dvec4(-0.5, +0.5, -0.5, 1.0),
		glm::dvec4(+0.5, +0.5, -0.5, 1.0),
		glm::dvec4(+0.5, +0.5, +0.5, 1.0),
		glm::dvec4(-0.5, -0.5, +0.5, 1.0),
		glm::dvec4(-0.5, -0.5, -0.5, 1.0),
		glm::dvec4(+0.5, -0.5, -0.5, 1.0),
		glm::dvec4(+0.5, -0.5, +0.5, 1.0)
	};
	for (unsigned int i = 0; i < data.size(); ++i)
		vertices[i] = glm::dvec3(modelTransformation * data[i]);
}

mte::BoundingBox::~BoundingBox() {
}
#pragma once

#include <ogl/GL/glew.h>
#include <ogl/GL/glut.h>
#include <ogl/glm/gtc/matrix_transform.hpp>

#include "Object.h"
#include "../interfaces/CameraSightCheckDelegate.h"

#include <vector>
#include <exception> 

/**
* @brief Sometimes it's usefull to perform a debug output of objects' bounding boxes
*
*/

namespace mte {
	class BoundingBox : public CameraSightCheckDelegate {
	private:
		GLuint vertexBufferObjectID; /**< Handle to opengl vertex buffer */
		GLuint indexBufferObjectID; /**< Handle to opengl index buffer */
		GLuint vertexArrayObjectID; /**< Handle to vao: vao saves vertex attributes settings (but it doesn't save buffers binding!) */
		GLint indicesCount; /**< Stores number of elements in index buffer */
		GLint vertexHighAttributeID; /**< Index of most significant part of vertex coordinate attribute */
		GLint vertexLowAttributeID; /**< Index of least significant part of vertex coordinate attribute */
		GLint cameraHighUniformID; /**< Index of most significant part of vertex coordinate attribute */
		GLint cameraLowUniformID; /**< Index of least significant part of vertex coordinate attribute */
		GLint transformationUniformID; /**< Location of view-projection uniform*/
		GLuint vertexShaderID; /**< Handle to opengl vertex shader */
		GLuint fragmentShaderID; /**< Handle to opengl fragment shader */
		GLuint shaderProgramID; /**< Handle to opengl shader program */
		glm::dmat4 modelTransformation; /**< Handle to opengl shader program */
		std::vector<glm::dvec3> vertices; /**< We need to duplicate the geometry data (for particular LOD) in system memory also, for collision tests etc */

	public:
		/**
		* @brief Performs all needed initialization: constructs bounding box geometry, loads all neded buffers and shaders
		*
		*/
		BoundingBox();

		/**
		* @brief Recalculates the vertex data
		*        TODO: this is not very nice, fix it later
		*
		* @param model Model transformation matrix
		*/
		void transform(const glm::dmat4 &model);

		/**
		* @brief Provides calling side with number of vertices in this object
		*
		* @return Unsigned integer value of number of vertices in this bounding box
		*/
		virtual unsigned int numberOfVertices() const override;
		
		/**
		* @brief Provides calling side with coorxinates for gixen vertex index
		*
		* @param Unsigned integer value of vertex index
		* @return Reference to unmodifiable vertex data
		*/
		virtual const glm::dvec3& vertexAtIndex(unsigned int index) const override;

		/**
		* @brief All needed cleanup here
		*
		*/
		virtual ~BoundingBox();
	};
}
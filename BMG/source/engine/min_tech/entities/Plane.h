#pragma once

#include <ogl/glm/glm.hpp>

/**
* @brief Plane entity from school algebra
*
*/

namespace mte {
	class Plane {
		double a, b, c, d; /**< Canonical plane equation coefficients */

	public:
		/**
		* @brief Performs some default initialization
		*
		*/
		Plane();

		/**
		* @brief Constructs a plane if given a point on a plae and perpendicular vector
		*
		* @param point Point coordinates on a plane
		* @param normal Vector which is perependicular to a plane
		*/
		Plane(const glm::dvec3& point, const glm::dvec3& normal);

		/**
		* @brief Constructs a plane if given three points lying on it
		*
		* @param l First point
		* @param m Second point
		* @param n Third point
		*/
		Plane(const glm::dvec3& l, const glm::dvec3& m, const glm::dvec3& n);

		/**
		* @brief That test a point against the plane by putting the coords in plane equation and computing the result
		*
		* @param point Point coordinates for a test
		* @return Plane equation evaluation for a given coordinates 
		*/
		double test(const glm::dvec3 point) const;
	};
}
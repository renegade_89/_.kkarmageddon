#pragma once

#include <ogl/glm/gtc/matrix_transform.hpp>

#include <set>
#include <memory>

/**
* @brief Defines an interface to an object (TODO: revise the interface)
*
*/

namespace mte {
	class BSPInfo;
	class Object {
	public:
		enum LevelOfDetail {LOD_HIGH, LOD_MEDIUM, LOD_LOW, NUM_LODS}; /**< Object can be drawn in high/medium/low detail */
	
	protected:
		size_t handle; /**< Object's handle */
		std::shared_ptr<BSPInfo> bspInfo;

	public:
		/**
		* @brief Saves objects handle
		*
		*/
		Object(size_t handle) : handle(handle) {}

		/**
		* @brief Getter for an object handle
		*
		* @return Unsigned integer value, object's handle
		*/
		size_t getHandle() { return handle; }

		/**
		* @brief Draws an object
		*
		* @param projection Projection transformation matrix
		* @param modelview Model/view transformation matrix
		* @param luminaryPosition Position of our source of light
		* @param cameraPosition World coordinates of camera 
		* @param node Index of the world node to draw only the part of object that is located in this particular world node
		* @param lod Integer value indicating the desired level of detail at which the object will be rendered
		*/
		virtual void draw(const glm::mat4 &projection, const glm::mat4 &modelview, const glm::vec3 &luminaryDirection = glm::vec3(0.0f, 1.0f, 0.0f), const glm::dvec3 &cameraPosition = glm::dvec3(0, 0, 0), int node = -1, int lod = LOD_HIGH) noexcept = 0;
		
		virtual std::shared_ptr<BSPInfo> getBSPInfo() = 0;

		/**
		* @brief "Life is really simple, but we insist on making it complicated" (c) Confucius
		*
		*/
		virtual ~Object() noexcept = default;
	};
}
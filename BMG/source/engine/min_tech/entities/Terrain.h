#pragma once

#include <ogl/GL/glew.h>
#include <ogl/GL/glut.h>
#include <ogl/glm/gtc/matrix_transform.hpp>

#include <engine/min_tech/entities/Object.h>
#include <engine/min_tech/interfaces/BSPInfo.h>

#include <vector>
#include <exception> 
#include <unordered_map>
#include <array>

/**
* @brief Incapsulates dirty details of terrain drawing
*
*/

namespace mte {
	class BSPDelegate;
	class Terrain : public Object, private BSPInfo {
	private:
		enum Axis { AXIS_X, AXIS_Y, AXIS_Z, NUM_AXIS }; /**< Enumerated world axis */
		enum Channel { CHANNEL_RED, CHANNEL_GREEN, CHANNEL_BLUE, NUM_CHANNELS }; /**< Enumerated color channels */

	private:
		GLuint vertexBufferObjectID; /**< Handle to vertex buffer object */
		
		// Achtung! terrain is separated by world nodes + each chunk is processed to obtain two more chunks of lower detail
		std::array<std::vector<GLuint>, LevelOfDetail::NUM_LODS> indexBufferObjectsIDs; /**< Arrays of index buffer object ids, one array per LOD */
		std::array<std::vector<GLuint>, LevelOfDetail::NUM_LODS> indexBufferObjectsIndicesCount; /**< Arrays of objects indicescount, one array per LOD */
		std::vector<std::vector<GLuint>> indicesParts; /**< Contains indices for each chunk */
		std::vector<glm::dvec2> verticalBounds; /**< Contains vertical bounds for each chunk */
		GLuint vertexArrayObjectID;  /**< Handle to vao: vao saves vertex attributes settings (but it doesn't save buffers binding!) */
		
		// Double-preciison values are converted to 2 floats in this implementation
		// Yes! some weak systems do not support double precision in shaders!
		// ...
		// Alrighty, then...
		GLint vertexHighAttributeID; /* Location of the attribute of most significant part of vertex coordinate */
		GLint vertexLowAttributeID; /* Location of the attribute of less significant part of vertex coordinate */
		GLint normalAttributeID; /**< Location of vertex color attribute */
		GLint cameraHighUniformID; /* Location of the uniform of most significant part of camera coordinate */
		GLint cameraLowUniformID; /* Location of the uniform of less significant part of camera coordinate */
		GLint viewUniformID;
		GLint projectionUniformID;
		GLint maxHeightUniformID;
		GLint lightDirectionUniformID;
		GLint edgeModeUniformID;
		GLuint vertexShaderID; /**< Vertex shader handle used for nice cleanup */
		GLuint fragmentShaderID; /**< Vertex fragment handle used for nice cleanup */
		GLuint shaderProgramID; /**< Shader program identifier used for drawing and correct cleanup */
		glm::dvec3 scale; /** Heightmap scale; this is basically a distance (horizontal or vertical) between to adjacent vertices of terrain */
		std::pair<std::vector<glm::dvec3>, std::vector<glm::dvec3>> vertices; /** Geometry stored additionally in system memory to perform various computations on cpu */
		std::unordered_map<int, int> bufferLookup; /** Maps an absolute world node index to an index of chunk in internal vectors */
		float maxHeight;

	public:
		/**
		* @brief Constructs terrain geometry, loads all neded buffers and shaders
		*
		* @param heights Single-precision height map of our terrain
		* @param dimensions Horizontal and vertical sizes of a heightmap
		* @param scale Defines the size of actual terrain
		* @param partitionDelegate Since terrain should be partioned but doesn't posses partitioning logic, this should be done by a delegate
		*/
		Terrain(size_t handle, const std::vector<GLfloat> &heights, const glm::ivec2 &dimensions, glm::dvec3 scale, BSPDelegate *partitionDelegate);

		/**
		* @brief Draw the terrain
		*
		* @param projection Projection transformation matrix
		* @param view View transformation matrix (terrain doesn't have any model transformation)
		* @param luminaryPosition Position of our source of light
		* @param cameraPosition Camera position coordinates passed separately
		* @param node World node index to draw only part of terrain which resides in that particular world index
		* @param lod Desired level of detail in object rendering
		*/
		virtual void draw(const glm::mat4 &projection, const glm::mat4 &view, const glm::vec3 &luminaryDirection, const glm::dvec3 &cameraPosition, int node, int lod = LOD_HIGH) noexcept override;

		/**
		* @brief Provides bsp data for this object
		*
		* @return Shared pointer to a defined structure
		*/
		virtual std::shared_ptr<BSPInfo> getBSPInfo() override;

	private:
		/**
		* @brief Passes to calling side number of triangles in a world node
		*
		* @param nodeIndex Index of the orld node
		* @return Unsigned integer value containing the number of objet's triangles in that particular world node
		*/
		unsigned int numberOfTriangles(int nodeIndex);

		/**
		* @brief Retrieves the triangle info by index
		*
		* @param nodeIndex Integer value of world node index of interest
		* @param index Triangle's index
		* @param a Reference to first triangle's vertex
		* @param b Reference to second triangle's vertex
		* @param c Reference to third triangle's vertex
		* @return Boolean value indicating if operation has been completed successfully
		*/
		bool triangleAtIndex(int nodeIndex, int index, glm::dvec3& a, glm::dvec3& b, glm::dvec3& c);

		/**
		* @brief Calculates the vertical bounds of objet's geometry in that particular world node
		*
		* @param node World node index
		* @return Double-precision values of lower and upper vertical bounds
		*/
		glm::dvec2 getVerticalBounds(int node);
	
	public:
		/**
		* @brief Performs all needed cleunup for buffers, shaders etc
		*
		*/
		virtual ~Terrain();
	};
}
#pragma once

#include "Camera.h"
#include "../interfaces/CameraSightCheckDelegate.h"

#include <ogl/glm/gtc/matrix_transform.hpp>

#include <cmath>
#include <iostream>

const glm::mat4& mte::Camera::getLookat() const {
	return lookat;
}

void mte::Camera::setLookat(const glm::vec3& target, const glm::vec3& up) {
	this->lookat = glm::lookAt(glm::vec3(0.0, 0.0, 0.0), target, glm::vec3(0.0, 1.0, 0.0));
	auto temp = glm::normalize(target);
	this->target = glm::dvec3(temp.x, temp.y, temp.z);
	this->up = glm::dvec3(0.0, 1.0, 0.0);
}

bool mte::Camera::hasInSight(CameraSightCheckDelegate& box) {
	size_t boxNumVertices = box.numberOfVertices();
	for (auto i = 0U; i < boxNumVertices; ++i) {
		const auto& vertex = box.vertexAtIndex(i);
		auto test = true;
		if (vertex.z > zNearNegated || vertex.z < zFarNegated) {
			test = false;
		} else {
			auto extentXNegated = vertex.z * tangent;
			auto extentYNegated = extentXNegated * height / width;
			if (vertex.x < extentXNegated || vertex.x > -extentXNegated || vertex.y < extentYNegated || vertex.y > -extentYNegated)
				test = false;
		}
		
		if (test)
			return true;
	}
	return false;
}
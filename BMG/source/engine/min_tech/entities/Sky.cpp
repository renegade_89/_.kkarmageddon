#include "Sky.h"

mte::Sky::Sky(size_t handle, const std::vector<glm::vec3> &starfield, const std::vector<GLfloat> &luminaryLandscape, const glm::ivec2 &luminaryDimensions, glm::vec3 luminaryPosition) : Object(handle) {
	glGenBuffers(1, &starsVertexBufferObjectID);
	glBindBuffer(GL_ARRAY_BUFFER, starsVertexBufferObjectID);
	glBufferData(GL_ARRAY_BUFFER, starfield.size() * sizeof(float), &starfield[0], GL_STATIC_DRAW);
	numberOfStars = starfield.size();

	// shaders
	auto starsVertexShaderSourceCode =
		"attribute vec3 a_coord;\n"
		"uniform mat4 u_vp;\n"
		"void main() {\n"
		"  gl_Position = u_vp * vec4(a_coord, 1.0);\n"
		"}\n";
	starsVertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(starsVertexShaderID, 1, &starsVertexShaderSourceCode, NULL);
	glCompileShader(starsVertexShaderID);
	auto starsFragmentShaderSourceCode =
		"void main() {\n"
		"  gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);\n"
		"}\n";

	starsFragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(starsFragmentShaderID, 1, &starsFragmentShaderSourceCode, NULL);
	glCompileShader(starsFragmentShaderID);
	starsShaderProgramID = glCreateProgram();
	glAttachShader(starsShaderProgramID, starsVertexShaderID);;
	glAttachShader(starsShaderProgramID, starsFragmentShaderID);
	glLinkProgram(starsShaderProgramID);
	auto isLinkedSuccessfully = 0;
	glGetProgramiv(starsShaderProgramID, GL_LINK_STATUS, &isLinkedSuccessfully);
	if (!isLinkedSuccessfully) {
		exit(EXIT_FAILURE);
	}

	// locations
	auto starsVertexAttributeName = "a_coord";
	starsVertexAttributeID = glGetAttribLocation(starsShaderProgramID, starsVertexAttributeName);
	starsViewProjectionUniformLocation = glGetUniformLocation(starsShaderProgramID, "u_vp");

	// heavenly body

	// vertices
	// plane equation
	auto planeNormal = glm::normalize(-luminaryPosition);
	auto planePoint = luminaryPosition;
	auto d = -(glm::dot(planeNormal, planePoint));
	// take some point on a plane
	const auto OFFSET = glm::vec2(1.0f, 1.0f);
	auto z = (-d - planeNormal.x * (planePoint.x + OFFSET.x) - planeNormal.y * (planePoint.y + OFFSET.y)) / planeNormal.z;
	auto newPoint = glm::vec3(planePoint.x + OFFSET.x, planePoint.y + OFFSET.y, z);
	// compute existing 2 vectors and find cross product to get the third one
	auto foo = glm::normalize(newPoint - planePoint);
	auto bar = glm::normalize(-luminaryPosition);
	auto baz = glm::normalize(glm::cross(foo, bar));
	// make a quad using offsets
	const auto offset = 0.05f;
	auto zeroth = planePoint + foo * offset + baz * offset;
	auto first = planePoint + foo * offset - baz * offset;
	auto second = planePoint - foo * offset - baz * offset;
	auto third = planePoint - foo * offset + baz * offset;
	// store to the buffer
	std::vector<float> luminaryVertices = {
		zeroth.x, zeroth.y, zeroth.z, 0.0f, 0.0f,
		first.x, first.y, first.z, 1.0f, 0.0f,
		second.x, second.y, second.z, 1.0f, 1.0f,
		third.x, third.y, third.z, 0.0f, 1.0f,
	};
	// indices
	std::vector<GLuint> luminaryIndices = {
		0, 2, 1, 0, 3, 2
	};
	// load
	glGenBuffers(1, &luminaryVertexBufferObjectID);
	glBindBuffer(GL_ARRAY_BUFFER, luminaryVertexBufferObjectID);
	glBufferData(GL_ARRAY_BUFFER, luminaryVertices.size() * sizeof(float), &luminaryVertices[0], GL_STATIC_DRAW);
	glGenBuffers(1, &luminaryIndexBufferObjectID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, luminaryIndexBufferObjectID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, luminaryIndices.size() * sizeof(GLint), &luminaryIndices[0], GL_STATIC_DRAW);
	numberOfLuminaryIndices = luminaryIndices.size();
	// texture
	std::vector<GLubyte> data(luminaryDimensions.x * luminaryDimensions.y * 3);
	for (auto column = 0U; column < luminaryDimensions.x; ++column) {
		for (auto row = 0U; row <  luminaryDimensions.y; ++row) {
			auto value = static_cast<int>(luminaryLandscape[row * luminaryDimensions.x + column] * 255);
			data[row * luminaryDimensions.x * 3 + column * 3 + 0] = value;
			data[row * luminaryDimensions.x * 3 + column * 3 + 1] = value;
			data[row * luminaryDimensions.x * 3 + column * 3 + 2] = value;
		}
	}
	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &luminaryTextureID);
	glBindTexture(GL_TEXTURE_2D, luminaryTextureID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, luminaryDimensions.x, luminaryDimensions.y, 0, GL_RGB, GL_UNSIGNED_BYTE, &data[0]);
#ifdef _DEBUG 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
#else
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
#endif
	// shaders
	auto luminaryVertexShaderSourceCode =
		"attribute vec3 a_coord;\n"
		"attribute vec2 a_tex_coord;\n"
		"uniform mat4 u_vp;\n"
		"varying vec2 v_tex_coord;\n"
		"void main() {\n"
		"  gl_Position = u_vp * vec4(a_coord, 1.0);\n"
		"  v_tex_coord = a_tex_coord;\n"
		"}\n";
	luminaryVertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(luminaryVertexShaderID, 1, &luminaryVertexShaderSourceCode, NULL);
	glCompileShader(luminaryVertexShaderID);
	auto luminaryFragmentShaderSourceCode =
		"varying vec2 v_tex_coord;\n"
		"uniform sampler2D u_tex;\n"
		"void main() {\n"
		"  vec2 transformed = v_tex_coord - vec2(0.5, 0.5);\n"
		"  float squaredDistance = transformed.x * transformed.x + transformed.y * transformed.y;\n"
		"  vec4 texelColor = texture(u_tex, v_tex_coord);\n"
		"  float luminance = 1.0 - texelColor.r * texelColor.r * texelColor.r;\n"
		"  if (squaredDistance > 0.25)\n"
		"    discard;\n"
		"  gl_FragColor = mix(vec4(luminance, luminance, luminance, 1.0), vec4(0.0, 0.0, 0.0, 1.0), smoothstep(0.23, 0.25, squaredDistance));\n"
		"}\n";

	luminaryFragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(luminaryFragmentShaderID, 1, &luminaryFragmentShaderSourceCode, NULL);
	glCompileShader(luminaryFragmentShaderID);
	luminaryShaderProgramID = glCreateProgram();
	glAttachShader(luminaryShaderProgramID, luminaryVertexShaderID);;
	glAttachShader(luminaryShaderProgramID, luminaryFragmentShaderID);
	glLinkProgram(luminaryShaderProgramID);
	isLinkedSuccessfully = 0;
	glGetProgramiv(luminaryShaderProgramID, GL_LINK_STATUS, &isLinkedSuccessfully);
	if (!isLinkedSuccessfully) {
		exit(EXIT_FAILURE);
	}

	// locations
	auto luminaryVertexAttributeName = "a_coord";
	luminaryVertexAttributeID = glGetAttribLocation(luminaryShaderProgramID, luminaryVertexAttributeName);
	auto luminaryTextureCoordinateAttributeName = "a_tex_coord";
	luminaryTextureCoordinateAttributeID = glGetAttribLocation(luminaryShaderProgramID, luminaryTextureCoordinateAttributeName);
	luminaryTextureUniformLocation = glGetUniformLocation(luminaryShaderProgramID, "u_tex");
	luminaryViewProjectionUniformLocation = glGetUniformLocation(luminaryShaderProgramID, "u_vp");

	// vao
	glGenVertexArrays(1, &luminaryVertexArrayObjectID);
	glBindVertexArray(luminaryVertexArrayObjectID);
	glEnableVertexAttribArray(luminaryVertexAttributeID);
	glEnableVertexAttribArray(luminaryTextureCoordinateAttributeID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, luminaryIndexBufferObjectID);
	glBindBuffer(GL_ARRAY_BUFFER, luminaryVertexBufferObjectID);
	glVertexAttribPointer(luminaryVertexAttributeID, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * (3 + 2), 0);
	glVertexAttribPointer(luminaryTextureCoordinateAttributeID, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * (3 + 2), (const GLvoid*)(3 * sizeof(float)));

}

void mte::Sky::draw(const glm::mat4 &projection, const glm::mat4 &view, const glm::vec3 &luminaryDirection, const glm::dvec3 &cameraPosition, int node, int lod) noexcept {
	glUseProgram(starsShaderProgramID);
	
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, starsVertexBufferObjectID);
	glEnableVertexAttribArray(starsVertexAttributeID);
	glVertexAttribPointer(starsVertexAttributeID, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 3, 0);
	glm::mat4 resultingTransformation = projection * view;
	glUniformMatrix4fv(starsViewProjectionUniformLocation, 1, GL_FALSE, &resultingTransformation[0][0]);
	glDrawArrays(GL_POINTS, 0, numberOfStars);

	glUseProgram(luminaryShaderProgramID);
	glBindVertexArray(luminaryVertexArrayObjectID);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, luminaryTextureID);
	glUniform1i(luminaryTextureUniformLocation, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, luminaryIndexBufferObjectID);
	glBindBuffer(GL_ARRAY_BUFFER, luminaryVertexBufferObjectID); 
	glUniformMatrix4fv(luminaryViewProjectionUniformLocation, 1, GL_FALSE, &resultingTransformation[0][0]);
	glDrawElements(GL_TRIANGLES, numberOfLuminaryIndices, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);

	glUseProgram(0);
}

mte::Sky::~Sky() {
	starsVertexAttributeID = 0;
	glDeleteShader(starsFragmentShaderID);
	assert(("glDeleteShader() error", glGetError() == GL_NO_ERROR));
	starsFragmentShaderID = 0;
	glDeleteShader(starsVertexShaderID);
	assert(("glDeleteShader() error", glGetError() == GL_NO_ERROR));
	starsVertexShaderID = 0;
	glDeleteProgram(starsShaderProgramID);
	assert(("glDeleteProgram() error", glGetError() == GL_NO_ERROR));
	starsShaderProgramID = 0;
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	assert(("glBindBuffer() error", glGetError() == GL_NO_ERROR));
	glDeleteBuffers(1, &starsVertexBufferObjectID);
	assert(("glDeleteBuffers() error", glGetError() == GL_NO_ERROR));
	starsVertexBufferObjectID = 0;

	luminaryVertexAttributeID = 0;
	glDeleteTextures(1, &luminaryTextureID);
	glDeleteShader(luminaryFragmentShaderID);
	assert(("glDeleteShader() error", glGetError() == GL_NO_ERROR));
	luminaryFragmentShaderID = 0;
	glDeleteShader(luminaryVertexShaderID);
	assert(("glDeleteShader() error", glGetError() == GL_NO_ERROR));
	luminaryVertexShaderID = 0;
	glDeleteProgram(luminaryShaderProgramID);
	assert(("glDeleteProgram() error", glGetError() == GL_NO_ERROR));
	luminaryShaderProgramID = 0;
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	assert(("glBindBuffer() error", glGetError() == GL_NO_ERROR));
	glDeleteBuffers(1, &luminaryIndexBufferObjectID);
	assert(("glDeleteBuffers() error", glGetError() == GL_NO_ERROR));
	luminaryIndexBufferObjectID = 0;
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	assert(("glBindBuffer() error", glGetError() == GL_NO_ERROR));
	glDeleteBuffers(1, &luminaryVertexBufferObjectID);
	assert(("glDeleteBuffers() error", glGetError() == GL_NO_ERROR));
	luminaryVertexBufferObjectID = 0;
	numberOfLuminaryIndices = 0;
}

std::shared_ptr<mte::BSPInfo> mte::Sky::getBSPInfo() {
	return nullptr;
}
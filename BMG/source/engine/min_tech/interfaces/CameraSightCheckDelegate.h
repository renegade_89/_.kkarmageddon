#pragma once

#include <ogl/glm/glm.hpp>

/**
* @brief We can try to discard object drawing if it's out of sight and the cetain info is provided through virtual calls (see below)
*
*/

namespace mte {
	class CameraSightCheckDelegate {
	public:
		/**
		* @brief Report to calling side the quantity of bounding vertices
		*
		* @return Non-negative result
		*/
		virtual unsigned int numberOfVertices() const = 0;
		
		/**
		* @brief Retrieve a bounding vertex coordinate data by index
		*
		* @param index Index of bounding vertex
		* @return Reference to unmodifiable vertex coords
		*/
		virtual const glm::dvec3& vertexAtIndex(unsigned int index) const = 0;

		virtual ~CameraSightCheckDelegate() = default;
	};
}
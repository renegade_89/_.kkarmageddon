#pragma once

#include <vector>
#include <ogl/glm/gtc/matrix_transform.hpp>

#include <engine/min_tech/entities/Object.h>

/**
* @brief If object desires to preform bsp partition of itself, it can invoke a delegate method defined in ths interface!
*
*/

namespace mte {
	class BSPDelegate {
	public:
		/**
		* @brief Invoke BSP partition routine
		*
		* @param object Pointer to an object to tear apart! (TODO: FIX RAW POINTER USED HERE!)
		* @param vertices Array of vertex attribute data (TODO: Should we used templates here instead of har coding vertex data format???)
		* @param indices Array of indices which represent triangle primitives of the mesh
		* @param indicesParts Output array of primitives subsets, sorted by world node index!
		* @param indicesPartsIndices Output array of a sequence of non-empty indicesParts entries (TODO: THAT LOOKS UNUSEFUL, CAN WE FIX IT?)
		*/
		virtual void executePartition(Object &object, std::pair<std::vector<glm::dvec3>, std::vector<glm::dvec3>>& vertices, std::vector<GLuint>& indices, std::vector<std::vector<GLuint>> &indicesParts, std::list<int> &indicesPartsIndices) noexcept = 0;
		
		/**
		* @brief "It does not matter how slowly you go as long as you do not stop" (c) Confucius
		*
		*/
		virtual ~BSPDelegate() = default;
	};
}
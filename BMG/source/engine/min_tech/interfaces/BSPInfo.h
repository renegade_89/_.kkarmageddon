#pragma once

#include <vector>
#include <ogl/glm/gtc/matrix_transform.hpp>

#include <engine/min_tech/entities/Object.h>

namespace mte {
	class BSPInfo {
	public:
		
		/**
		* @brief Provides number of object's triangles in that particular world node
		*
		* @param nodeIndex Integer value of world node index of interest
		* @return Unsigned integer value of number of triangles in that world node
		*/
		virtual unsigned int numberOfTriangles(int nodeIndex) = 0;

		/**
		* @brief Retrieves the triangle info by index
		*
		* @param nodeIndex Integer value of world node index of interest
		* @param index Triangle's index
		* @param a Reference to first triangle's vertex
		* @param b Reference to second triangle's vertex
		* @param c Reference to third triangle's vertex
		* @return Boolean value indicating if operation has been completed successfully
		*/
		virtual bool triangleAtIndex(int nodeIndex, int index, glm::dvec3& a, glm::dvec3& b, glm::dvec3& c) = 0;

		/**
		* @brief Calculates the vertical bounds of objet's geometry in that particular world node
		*
		* @param node World node index
		* @return Double-precision values of lower and upper vertical bounds
		*/
		virtual glm::dvec2 getVerticalBounds(int node) = 0;

		/**
		* @brief "When anger rises, think of the consequences"(c) Confucius
		*
		*/
		virtual ~BSPInfo() = default;
	};
}
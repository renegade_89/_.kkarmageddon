#pragma once

#include <vector>
#include <ogl/glm/gtc/matrix_transform.hpp>

/**
* @brief Our engine is capable of detecting collisions... but it doesn't know to handle them!
*		 Game logic (or 'controller') is responsible for such handling.
*		 So we must define an interface to provide communication protocol between such distinct parties.
*
*/

namespace mte {
	class CollisionTestDelegate {
	public:
		/**
		* @brief Reports how many guys can actually collide with environment (TODO: what about collision with each other?) 
		*
		* @return Non-negative number
		*/
		virtual unsigned int numberOfActors() const = 0;
		
		/**
		* @brief Compute a full transformation for a 1x1x1 box for that particular guy at this moment
		*
		* @param index Actor's number
		* @return Transformation matrix for actor's bounding box
		*/
		virtual glm::dmat4 boundingBoxTransformationForActor(int index) const = 0;

		/**
		* @brief Invoked if collision was detected for an index-th actor
		*
		* @param index Actor's index
		* @return Non-negative number
		*/
		virtual void onCollision(int index) = 0;
		
		/**
		* @brief "Everything has beauty, but not everyone sees it" (c) Confucius
		*
		*/
		virtual ~CollisionTestDelegate() = default;
	};
}
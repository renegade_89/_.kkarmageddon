#include "Common.h"

namespace cold {
	const unsigned int WINDOW_WIDTH = 800;
	const unsigned int WINDOW_HEIGHT = 600;
	const double FOV = 60.0;
	const unsigned int HORIZON_HEIGHT = 20;
	const unsigned int TIMER_PERIOD = 20;
	const int MAP_SIZE = 257; // 2 ^ n + 1
	const int LUMINARY_SIZE = 128;

	namespace SI {
		const double MM = 20.0;
		const double CM = 100 * MM;
		const double M = 10 * CM;
		const double KM = 1000 * M;
	}
}
#pragma once

/**
 * @brief Some global constants
 */

namespace cold {
	extern const unsigned int WINDOW_WIDTH; /**< Integer value of window width in pixels */
	extern const unsigned int WINDOW_HEIGHT; /**< Integer value of window height in pixels */
	extern const double FOV; /**< Double precision value of field of view */
	extern const unsigned int HORIZON_HEIGHT;  /**< Distant objects max vertical size */
	extern const unsigned int TIMER_PERIOD; /**< Integer value of milliseconds between each iteration */
	extern const int MAP_SIZE; /**< Map size in units along one dimension */
	extern const int LUMINARY_SIZE; /**< Moon texture size along one dimension */

	namespace SI {
		extern const double MM; /**< milimeter */
		extern const double CM; /**< centimeter */
		extern const double M; /**< meter */
		extern const double KM; /**< kilometer */
	}
}
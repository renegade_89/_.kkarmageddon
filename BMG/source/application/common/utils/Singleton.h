#pragma once

/**
 * @brief Straightforward singleton template, just to start with something
 */

namespace uti {
	template <typename T>
	class Singleton
	{
	public:
		/**
		 * @brief Client access to singleton
		 *
		 * @return Reference to global instance of our object
		 */
		static T& getInstance()
		{
			return *getInstancePointerReference();
		}

		/**
		 * @brief Destroys singleton object
		 */
		static void destroyInstance()
		{
			T* globalInstance = &getInstance();
			if (globalInstance != nullptr) {
				delete globalInstance;
				getInstancePointerReference() = nullptr;
			}
		}

	private:
		/**
		 * @brief Used internally to free the memory and reset pointer
		 *
		 * @return Reference to a pointer to object instance
		 */
		static T*& getInstancePointerReference() {
			static T* globalInstance = nullptr;
			if (globalInstance == nullptr) {
				globalInstance = new T();
			}
			return globalInstance;
		}

	protected:
		/**
		 * @brief Disable direct creation
		 *
		 */
		Singleton() = default;

		/**
		 * Disable direct delete
		 *
		 */
		~Singleton() = default;

	public:
		/**
		 * @brief No copyng for singleton
		 *
		 * @param object Const reference to an object
		 */
		Singleton(Singleton const &object) = delete;

		/**
		* @brief No assignment for singleton
		*
		* @param object Const reference to an object
		*/
		Singleton& operator=(Singleton const &object) = delete;
	};
}
#pragma once

#include <cstdint>

/**
* @brief It's convenient to provide a guid for each object, since object's blueprint id doesn't identifies him/her uniquely 
*        Standard GUID structure
*/

namespace uti {
	struct GUID {
		uint32_t g; /**< First data chunk, 32 bits */
		uint16_t u; /**< Second data chunk, 16 bits */
		uint16_t i; /**< Third data chunk, 16 bits */
		uint8_t d[8]; /**< Eight trailing 1-byte data chunks */

		/**
		* @brief Initializes itself with random bytes (todo: is it okay?)
		*
		* @see https://en.wikipedia.org/wiki/Universally_unique_identifier
		*/
		GUID();

		/**
		* @brief Performs comparison with provided guid
		*
		* @param guid Reference to costant guid structure provided by client
		*/
		bool operator==(const GUID& guid) const;
	};
}
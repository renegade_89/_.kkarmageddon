#pragma once

/**
 * @brief This may become a set of helper functions in future ...
          But for now it only holds the converter from double to 2 floats
 */

namespace uti {
	/**
	* @brief Encodes the double into two floats, a high and low
	* 
	* @param value Double precision value to process
	* @param high Reference to a single precision value to to represent the numbers 65,536 to 549,755,748,352 ((223 - 1) * 65536) in 65536 increments
	* @param low Reference to a single precision value to hold the part of the double after the decimal point, which means that the resolution of the number is 2-7, 0.0078125
	* @see http://help.agi.com/AGIComponents/html/BlogPrecisionsPrecisions.htm
	*/
	inline void doubleToFloats(double value, float &high, float &low) {
		/*

		"...A double is encoded into two floats, a high and low.

		In the low float, 7 of the 23 fraction bits are used for the part of the double after the decimal point.
		Which means that the resolution of the number is 2-7, 0.0078125.
		This is less than a cm.
		The remaining 16 bits are used to represent an integer value from 0 to 65535 (216 - 1) in 1 increments.

		The high part uses all 23 bits to represent the numbers 65,536 to 549,755,748,352 ((223 - 1) * 65536) in 65536 increments.
		If you are familiar with the IEEE 754 specification, there is an additional unsaid bit of precision.
		Such that there are actually 24 fraction bits.
		Why isn't that bit used here? That bit is used to capture overflows when two lows or two highs are added or subtracted from each other.

		The maximum whole number that can be encoded is 549,755,748,352..."
		http://help.agi.com/AGIComponents/html/BlogPrecisionsPrecisions.htm
		*/
		if (value >= 0.0) {
			auto doubleHigh = floor(value / 65536.0) * 65536.0;
			high = static_cast<float>(doubleHigh);
			low = static_cast<float>(value - doubleHigh);
		}
		else {
			auto doubleHigh = floor(-value / 65536.0) * 65536.0;
			high = static_cast<float>(-doubleHigh);
			low = static_cast<float>(value + doubleHigh);
		}
	};
}
#include "GUID.h"

#include <cstdlib>

uti::GUID::GUID() {
	// utilizes c-style rand, may refactor later
	g = static_cast<uint32_t>(rand());
	u = static_cast<uint16_t>(rand());
	i = static_cast<uint16_t>(rand());
	d[0] = static_cast<uint8_t>(rand());
	d[1] = static_cast<uint8_t>(rand());
	d[2] = static_cast<uint8_t>(rand());
	d[3] = static_cast<uint8_t>(rand());
	d[4] = static_cast<uint8_t>(rand());
	d[5] = static_cast<uint8_t>(rand());
	d[6] = static_cast<uint8_t>(rand());
	d[7] = static_cast<uint8_t>(rand());
}

bool uti::GUID::operator==(const GUID& guid) const {
	// simply compare members, one by one
	return ((g == guid.g) &&
		(u == guid.u) &&
		(i == guid.i) &&
		(d[0] == guid.d[0]) &&
		(d[1] == guid.d[1]) &&
		(d[2] == guid.d[2]) &&
		(d[3] == guid.d[3]) &&
		(d[4] == guid.d[4]) &&
		(d[5] == guid.d[5]) &&
		(d[6] == guid.d[6]) &&
		(d[7] == guid.d[7]));
}
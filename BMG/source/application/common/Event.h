#pragma once

/**
 * @brief This document should contain all available event IDs
 */

namespace cold {
	enum class Event { EVENT_DEBUG_COLLISION_TEST_POSITIVE, NUM_EVENTS };/**< Test event is the only available... will add later */
}
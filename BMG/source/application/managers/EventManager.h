#pragma once

#include <application/common/utils/Singleton.h>
#include <application/common/Event.h>
#include <application/common/utils/GUID.h>

#include <memory>
#include <unordered_map>
#include <list>

/**
 * @brief Allows objects to subscribe (and unsubscribe) for certain event
 */

namespace cold {
	class Object;
	class EventManager : public uti::Singleton<EventManager> {
	private:
		std::unordered_map<Event, std::list<std::pair<uti::GUID, std::weak_ptr<Object>>>> subscribers; /**< Holds a list of subscribers for each event type. */

	public:
		/**
		* @brief Deleted copy constructor since we are going to utilize event manager as singleton
		*
		* @param object Const reference to original object
		*/
		EventManager(const EventManager& object) = delete;

		/**
		* @brief Deleted assignment operator since we are going to utilize event manager as singleton
		*
		* @param object Const reference to original object
		*/
		EventManager& operator=(const EventManager& object) = delete;

		/**
		* @brief Add subscriber for a certain event type
		*
		* @param object Const reference to subscriber
		* @param event Eumerated value of event type of interest
		*/
		void subscribe(std::shared_ptr<Object> object, Event event);

		/**
		* @brief Remove object from subscribers list for a certain event type
		*
		* @param object Const reference to subscriber
		* @param event Eumerated value of event type of lost interest
		*/
		void unsubscribe(std::shared_ptr<Object> object, Event event);

		/**
		* @brief For given event type, process all subscribers 
		*
		* @param event Type of event to raise
		*/
		void raise(Event event) noexcept;

		/**
		* @brief Event is related to specific object, process it
		*
		* @param object Pointer to an object, which will soo recieve some nice event
		* @param event Type of event to raise
		*/
		void send(std::shared_ptr<Object> object, Event event) noexcept;

		/**
		* @brief Cleanup all expired objects
		*
		*/
		void update() noexcept;

	private:
		/**
		* @brief Performs all needed initialization stuff
		*
		*/
		EventManager() = default;

		/**
		* Nothing to cleanup here but hey, we made it private!
		*/
		~EventManager() = default;

	public:
		friend class uti::Singleton<EventManager>; /**< Should be granted with access, he's responsible for construction and destruction */

	};
}
#include "EventManager.h"

#include <application/managers/ScriptManager.h>
#include <application/mvc/model/gom/object/Object.h>

#include <algorithm>

void cold::EventManager::subscribe(std::shared_ptr<Object> object, Event event) {
	// check before adding if it has already been done actually
	auto& subset = subscribers[event];
	auto i = std::find_if(subset.begin(), subset.end(), [&object](std::pair<uti::GUID, std::weak_ptr<Object>>& pair) { return pair.first == object->getGUID(); });
	if (i == subset.end())
		subset.push_back(std::make_pair(object->getGUID(), object));
}

void cold::EventManager::unsubscribe(std::shared_ptr<Object> object, Event event) {
	// locate the subscriber and throw it out
	auto& subset = subscribers[event];
	auto i = std::find_if(subset.begin(), subset.end(), [&object](std::pair<uti::GUID, std::weak_ptr<Object>>& pair) { return pair.first == object->getGUID(); });
	if (i != subset.end())
		subset.erase(i);
}

void cold::EventManager::raise(Event event) noexcept {
	// currently invokes the corresponding handler script for each subscriber
	auto& subset = subscribers[event];
	for (auto& pair : subset) {
		std::shared_ptr<Object> temp = std::shared_ptr<Object>(pair.second);
		ScriptManager::getInstance().getInstance().run("object_" + std::to_string(temp->getID()) + "_on_event", temp.get(), static_cast<int>(event));
	}
}

void cold::EventManager::send(std::shared_ptr<Object> object, Event event) noexcept {
	// currently invokes the corresponding handler script 
	auto& subset = subscribers[event];
	auto i = std::find_if(subset.begin(), subset.end(), [&object](auto& pair) { return pair.first == object->getGUID(); });
	if (i != subset.end())
		ScriptManager::getInstance().getInstance().run("object_" + std::to_string(object->getID()) + "_on_event", object.get(), static_cast<int>(event));
}

void cold::EventManager::update() noexcept {
	// if weak pointer is expired, then object no longer exist and we must delete the entry
	for (auto& subset : subscribers) {
		auto done = false;
		do {
			auto expired = std::find_if(subset.second.begin(), subset.second.end(), [](std::pair<uti::GUID, std::weak_ptr<Object>>& pair) { return pair.second.expired(); });
			if (expired != subset.second.end())
				subset.second.erase(expired);
			else
				done = true;
		} while (!done);
	}
}
#pragma once

#include <application/common/utils/Singleton.h>

extern "C" {
	#include "lua.h"
	#include "lauxlib.h"
	#include "lualib.h"
}

#include <LuaBridge.h>

#include <string>
#include <iostream>

/**
 * @brief Wraps lua bridge and provides a template interface to run any script function
 */

namespace cold {
	class ScriptManager : public uti::Singleton<ScriptManager> {
		lua_State* state; /**< Lua thread state structure. */

	public:
		/**
		* @brief Deleted copy constructor since we are going to utilize script manager as singleton
		*
		* @param object Const reference to original object
		*/
		ScriptManager(const ScriptManager& object) = delete;

		/**
		* @brief Deleted assignment operator since we are going to utilize script manager as singleton
		*
		* @param object Const reference to original object
		*/
		ScriptManager& operator=(const ScriptManager& object) = delete;
		
		/**
		* @brief Simply calls static bind methods one by one for all classes that should be available in scipts
		*
		*/
		void exportClassesToLua();

		/**
		* @brief Defines a blueprint for lua function invocation via lua bridge
		*
		* @param routine Constant reference to a string containing function name
		* @param args Arbitrary arguments for the lua function
		*/
		template <typename... Args>
		void run(const std::string& routine, Args&&... args) {
			try {
				luabridge::LuaRef ref = luabridge::getGlobal(state, routine.c_str());
				ref(std::forward<Args>(args)...);
			} catch (luabridge::LuaException const& e) {
				std::cerr << e.what();
			}
		}

		/**
		* @brief Gets special lua bridge namespace object
		*
		* @return Object for a binding
		*/
		luabridge::Namespace getNamespace();

	private:
		/**
		* @brief Performs all needed initialization stuff
		*
		*/
		ScriptManager();

		/**
		* @brief Performs all needed cleanup
		*
		*/
		~ScriptManager();

		friend class uti::Singleton<ScriptManager>; /**< Should be granted with access, he's responsible for construction and destruction */
	};
}
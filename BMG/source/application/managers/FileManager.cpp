#include "FileManager.h"

#include <application/mvc/model/GameModel.h>
#include <application/mvc/model/gom/object/Object.h>
#include <application/mvc/model/location/Location.h>
#include <application/mvc/model/input_model/InputModel.h>
#include <application/mvc/model/gom/object/components/TerrainComponent.h>
#include <application/mvc/model/gom/object/components/PositionComponent.h>
#include <application/mvc/model/gom/object/components/DirectionComponent.h>
#include <application/mvc/model/gom/object/components/BoundingBoxComponent.h>
#include <application/mvc/model/gom/GameObjectManager.h>

#include "pugixml.hpp"

#include <sstream>
#include <iostream>
#include <iomanip>
#include <vector>

// evil macro
#define READ_GUID(node, guid) \
	for (auto attribute = node.first_attribute(); attribute; attribute = attribute.next_attribute()) { \
		if (strcmp(attribute.name(), "g") == 0) guid.g = atoi(attribute.value()); \
		else if (strcmp(attribute.name(), "u") == 0) guid.u = atoi(attribute.value()); \
		else if (strcmp(attribute.name(), "i") == 0) guid.i = atoi(attribute.value()); \
		else if (strcmp(attribute.name(), "d0") == 0) guid.d[0] = atoi(attribute.value()); \
		else if (strcmp(attribute.name(), "d1") == 0) guid.d[1] = atoi(attribute.value()); \
		else if (strcmp(attribute.name(), "d2") == 0) guid.d[2] = atoi(attribute.value()); \
		else if (strcmp(attribute.name(), "d3") == 0) guid.d[3] = atoi(attribute.value()); \
		else if (strcmp(attribute.name(), "d4") == 0) guid.d[4] = atoi(attribute.value()); \
		else if (strcmp(attribute.name(), "d5") == 0) guid.d[5] = atoi(attribute.value()); \
		else if (strcmp(attribute.name(), "d6") == 0) guid.d[6] = atoi(attribute.value()); \
		else if (strcmp(attribute.name(), "d7") == 0) guid.d[7] = atoi(attribute.value()); \
	}

void cold::FileManager::loadSave(GameModel& model, const std::string& name) {
	// load save means: read level file name from save and load level if it's not current level, read state data from save and apply it
	// for a clean start this save file should contain only level file name field
	

	// load the game state from the 'slot'
	pugi::xml_document document;
	pugi::xml_parse_result result = document.load_file(name.c_str());
	auto root = document.child("level");
	
	auto attribute = root.first_attribute();
	if (strcmp(attribute.name(), "name") == 0)
		loadLevel(model, attribute.value());

	for (auto object : root.children("object")) {
		auto id = -1;
		for (auto attribute = object.first_attribute(); attribute; attribute = attribute.next_attribute()) {
			if (strcmp(attribute.name(), "id") == 0) {
				id = atoi(attribute.value());
				break;
			}
		}
		if (id >= 0 && id < Object::NUM_OBJECT_IDS) {
			//auto temp = std::make_shared<Object>();
			uti::GUID guid;

			auto guidNode = object.child("guid");
			READ_GUID(guidNode, guid);

			// alrighty then, we have an id and guid
			// with a help of this info we can retrieve the needed object 
			// and update it's state

			auto temp = model.location->get(id, guid);

			for (auto component : object.children("component")) {
				auto data = component.first_child();
				for (auto attribute = component.first_attribute(); attribute; attribute = attribute.next_attribute()) {
					if (strcmp(attribute.name(), "type") == 0) {
						if (strcmp(attribute.value(), "direction") == 0) {
							// parse direction data
							auto at = data.child("at");
							for (auto attribute = at.first_attribute(); attribute; attribute = attribute.next_attribute()) {
								if (strcmp(attribute.name(), "x") == 0) {
									temp->directionComponent->at.x = atof(attribute.value());
								}
								else if (strcmp(attribute.name(), "y") == 0) {
									temp->directionComponent->at.y = atof(attribute.value());
								}
								else if (strcmp(attribute.name(), "z") == 0) {
									temp->directionComponent->at.z = atof(attribute.value());
								}
							}
							auto up = data.child("up");
							for (auto attribute = up.first_attribute(); attribute; attribute = attribute.next_attribute()) {
								if (strcmp(attribute.name(), "x") == 0) {
									temp->directionComponent->up.x = atof(attribute.value());
								}
								else if (strcmp(attribute.name(), "y") == 0) {
									temp->directionComponent->up.y = atof(attribute.value());
								}
								else if (strcmp(attribute.name(), "z") == 0) {
									temp->directionComponent->up.z = atof(attribute.value());
								}
							}
						}
						else if (strcmp(attribute.value(), "position") == 0) {
							// parse position data
							for (auto attribute = data.first_attribute(); attribute; attribute = attribute.next_attribute()) {
								if (strcmp(attribute.name(), "x") == 0) {
									temp->positionComponent->position.x = atof(attribute.value());
								}
								else if (strcmp(attribute.name(), "y") == 0) {
									temp->positionComponent->position.y = atof(attribute.value());
								}
								else if (strcmp(attribute.name(), "z") == 0) {
									temp->positionComponent->position.z = atof(attribute.value());
								}
							}
						}
					}
				}
			}
		}
	}
}

void cold::FileManager::writeSave(const GameModel& model, const std::string& name) {
	writeLevel(model, name);
}

void cold::FileManager::writeLevel(const GameModel& model, const std::string& name) {
	pugi::xml_document document;
	auto node = document.append_child(pugi::node_declaration);
	node.append_attribute("version") = "1.0";
	node.append_attribute("encoding") = "ISO-8859-1";
	node.append_attribute("standalone") = "yes";
	// root node - model
	auto root = document.append_child("model");
	// model's child node - location
	auto location = root.append_child("location");;
	// location's child nodes - objects
	for (auto pair : model.location->objects) {
		auto child = location.append_child("object");
		auto object = pair.second;
		std::stringstream stream;
		// id
		stream << object->id;
		child.append_attribute("id") = stream.str().c_str();
		stream.str(std::string());

		// guid
		auto guid = child.append_child("guid");
		std::ios format(NULL);
		format.copyfmt(stream);
		stream << std::hex;
		stream << std::setfill('0') << std::setw(8);
		stream << object->guid.g;
		guid.append_attribute("g") = stream.str().c_str();
		stream.str(std::string());
		stream << std::setfill('0') << std::setw(4);
		stream << object->guid.u;
		guid.append_attribute("u") = stream.str().c_str();
		stream.str(std::string());
		stream << std::setfill('0') << std::setw(4);
		stream << object->guid.i;
		guid.append_attribute("i") = stream.str().c_str();
		stream.str(std::string());
		for (int i = 0; i < 8; ++i) {
			stream << "d" << i;
			std::string buffer = stream.str();
			stream.str(std::string());
			stream << std::setfill('0') << std::setw(2);
			stream << (unsigned int)object->guid.d[i];
			guid.append_attribute(buffer.c_str()) = stream.str().c_str();
			stream.str(std::string());
		}
		stream.copyfmt(format);
		// bounding box component
		if (object->boundingBoxComponent) {
			auto component = child.append_child("component");
			component.append_attribute("type") = "bounding_box";
			auto data = component.append_child("data");
			std::stringstream stream;
			stream << object->boundingBoxComponent->dimensions[0];
			data.append_attribute("width") = stream.str().c_str();
			stream.str(std::string());
			stream << object->boundingBoxComponent->dimensions[1];
			data.append_attribute("height") = stream.str().c_str();
			stream.str(std::string());
			stream << object->boundingBoxComponent->dimensions[2];
			data.append_attribute("length") = stream.str().c_str();
			stream.str(std::string());
		}
		// direction component
		if (object->directionComponent) {
			auto component = child.append_child("component");
			component.append_attribute("type") = "direction";
			auto data = component.append_child("data");
			std::stringstream stream;
			auto at = data.append_child("at");
			stream << object->directionComponent->at.x;
			at.append_attribute("x") = stream.str().c_str();
			stream.str(std::string());
			stream << object->directionComponent->at.y;
			at.append_attribute("y") = stream.str().c_str();
			stream.str(std::string());
			stream << object->directionComponent->at.z;
			at.append_attribute("z") = stream.str().c_str();
			stream.str(std::string());
			auto up = data.append_child("up");
			stream << object->directionComponent->up.x;
			up.append_attribute("x") = stream.str().c_str();
			stream.str(std::string());
			stream << object->directionComponent->up.y;
			up.append_attribute("y") = stream.str().c_str();
			stream.str(std::string());
			stream << object->directionComponent->up.z;
			up.append_attribute("z") = stream.str().c_str();
			stream.str(std::string());
		}
		// position component
		if (object->positionComponent) {
			auto component = child.append_child("component");
			component.append_attribute("type") = "position";
			auto data = component.append_child("data");
			std::stringstream stream;
			stream << object->positionComponent->position.x;
			data.append_attribute("x") = stream.str().c_str();
			stream.str(std::string());
			stream << object->positionComponent->position.y;
			data.append_attribute("y") = stream.str().c_str();
			stream.str(std::string());
			stream << object->positionComponent->position.z;
			data.append_attribute("z") = stream.str().c_str();
			stream.str(std::string());
		}
		// terrain component
		if (object->terrainComponent) {
			auto component = child.append_child("component");
			component.append_attribute("type") = "terrain";
			auto data = component.append_child("data");
			std::stringstream stream;
			stream << object->terrainComponent->width;
			data.append_attribute("width") = stream.str().c_str();
			stream.str(std::string());
			stream << object->terrainComponent->height;
			data.append_attribute("height") = stream.str().c_str();
			stream.str(std::string());
			auto scale = data.append_child("scale");
			stream << object->terrainComponent->scale.x;
			scale.append_attribute("x") = stream.str().c_str();
			stream.str(std::string());
			stream << object->terrainComponent->scale.y;
			scale.append_attribute("y") = stream.str().c_str();
			stream.str(std::string());
			stream << object->terrainComponent->scale.z;
			scale.append_attribute("z") = stream.str().c_str();
			stream.str(std::string());
			auto map = data.append_child("map");
			for (auto i = 0; i < object->terrainComponent->height; ++i)
				for (auto j = 0; j < object->terrainComponent->width; ++j) {
					std::stringstream stream;
					auto cell = map.append_child("cell");
					stream << i;
					cell.append_attribute("row") = stream.str().c_str();
					stream.str(std::string());
					stream << j;
					cell.append_attribute("column") = stream.str().c_str();
					stream.str(std::string());
					stream << object->terrainComponent->map[i * object->terrainComponent->width + j];
					cell.append_attribute("height") = stream.str().c_str();
					stream.str(std::string());
				}
		}
	}
	document.save_file(name.c_str());
}

void cold::FileManager::loadLevel(GameModel& model, const std::string& name) {
	model.location->removeAllObjects();

	pugi::xml_document document;
	pugi::xml_parse_result result = document.load_file(name.c_str());
	auto root = document.child("model");
	for (auto location : root.children("location")) {
		for (auto object : location.children("object")) {
			auto id = -1;
			for (auto attribute = object.first_attribute(); attribute; attribute = attribute.next_attribute()) {
				if (strcmp(attribute.name(), "id") == 0) {
					id = atoi(attribute.value());
					break;
				}
			}
			if (id >= 0 && id < Object::NUM_OBJECT_IDS) {
				auto temp = std::make_shared<Object>();

				auto guid = object.child("guid");
				READ_GUID(guid, temp->guid)

				for (auto component : object.children("component")) {
					auto data = component.first_child();
					for (auto attribute = component.first_attribute(); attribute; attribute = attribute.next_attribute()) {
						if (strcmp(attribute.name(), "type") == 0) {
							if (strcmp(attribute.value(), "terrain") == 0) {
								int mapWidth = 0;
								int mapHeight = 0;
								// parse terrain data
								for (auto attribute = data.first_attribute(); attribute; attribute = attribute.next_attribute()) {
									if (strcmp(attribute.name(), "width") == 0) {
										mapWidth = atoi(attribute.value());
									}
									else if (strcmp(attribute.name(), "height") == 0) {
										mapHeight = atoi(attribute.value());
									}
								}
								glm::dvec3 scaleFactors;
								auto scale = data.child("scale");
								for (auto attribute = scale.first_attribute(); attribute; attribute = attribute.next_attribute()) {
									if (strcmp(attribute.name(), "x") == 0) {
										scaleFactors.x = atof(attribute.value());
									}
									else if (strcmp(attribute.name(), "y") == 0) {
										scaleFactors.y = atof(attribute.value());
									}
									else if (strcmp(attribute.name(), "z") == 0) {
										scaleFactors.z = atof(attribute.value());
									}
								}
								std::vector<float> heightMap(mapWidth * mapHeight);
								auto map = data.child("map");
								for (auto cell : map.children("cell")) {
									auto row = -1;
									auto column = -1;
									auto height = -1.0f;
									for (auto attribute = cell.first_attribute(); attribute; attribute = attribute.next_attribute()) {
										if (strcmp(attribute.name(), "row") == 0) {
											row = atoi(attribute.value());
										}
										else if (strcmp(attribute.name(), "column") == 0) {
											column = atoi(attribute.value());
										}
										else if (strcmp(attribute.name(), "height") == 0) {
											height = (float)atof(attribute.value());
										}
									}
									heightMap[row * mapWidth + column] = height;
								}
								std::vector<int8_t> dummy(mapWidth * mapHeight);
								// alrighty then, construct the actual object
								temp->terrainComponent = std::shared_ptr<TerrainComponent>(new TerrainComponent(heightMap, mapWidth, mapHeight, scaleFactors, dummy));
							}
							else if (strcmp(attribute.value(), "bounding_box") == 0) {
								// parse bounding box data
								glm::dvec3 boundigBox;
								for (auto attribute = data.first_attribute(); attribute; attribute = attribute.next_attribute()) {
									if (strcmp(attribute.name(), "width") == 0) {
										boundigBox.x = atof(attribute.value());
									}
									else if (strcmp(attribute.name(), "height") == 0) {
										boundigBox.y = atof(attribute.value());
									}
									else if (strcmp(attribute.name(), "length") == 0) {
										boundigBox.z = atof(attribute.value());
									}
								}
								temp->boundingBoxComponent = std::shared_ptr<BoundingBoxComponent>(new BoundingBoxComponent(boundigBox.x, boundigBox.y, boundigBox.z));
							}
							else if (strcmp(attribute.value(), "direction") == 0) {
								// parse direction data
								glm::dvec3 at, up;
								auto atNode = data.child("at");
								for (auto attribute = atNode.first_attribute(); attribute; attribute = attribute.next_attribute()) {
									if (strcmp(attribute.name(), "x") == 0) {
										at.x = atof(attribute.value());
									}
									else if (strcmp(attribute.name(), "y") == 0) {
										at.y = atof(attribute.value());
									}
									else if (strcmp(attribute.name(), "z") == 0) {
										at.z = atof(attribute.value());
									}
								}
								auto upNode = data.child("up");
								for (auto attribute = upNode.first_attribute(); attribute; attribute = attribute.next_attribute()) {
									if (strcmp(attribute.name(), "x") == 0) {
										up.x = atof(attribute.value());
									}
									else if (strcmp(attribute.name(), "y") == 0) {
										up.y = atof(attribute.value());
									}
									else if (strcmp(attribute.name(), "z") == 0) {
										up.z = atof(attribute.value());
									}
								}
								temp->directionComponent = std::shared_ptr<DirectionComponent>(new DirectionComponent(at.x, at.y, at.z, up.x, up.y, up.z));
							}
							else if (strcmp(attribute.value(), "position") == 0) {
								// parse position data
								glm::dvec3 position;
								for (auto attribute = data.first_attribute(); attribute; attribute = attribute.next_attribute()) {
									if (strcmp(attribute.name(), "x") == 0) {
										position.x = atof(attribute.value());
									}
									else if (strcmp(attribute.name(), "y") == 0) {
										position.y = atof(attribute.value());
									}
									else if (strcmp(attribute.name(), "z") == 0) {
										position.z = atof(attribute.value());
									}
								}
								temp->positionComponent = std::shared_ptr<PositionComponent>(new PositionComponent(position.x, position.y, position.z));
							}
						}
					}
				}
				temp->id = id;
				model.location->addObject(temp);
			}
		}
	}
}

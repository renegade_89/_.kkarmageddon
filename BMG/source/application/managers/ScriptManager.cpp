#include "ScriptManager.h"

extern "C" {
	#include "lua.h"
	#include "lualib.h"
	#include "lauxlib.h"
}

#include <iostream>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <cctype>

#include <application/mvc/model/gom/object/Object.h>
#include <application/mvc/model/gom/object/components/PositionComponent.h>
#include <application/common/Event.h>

cold::ScriptManager::ScriptManager() {
	// initialize everything and load our incantations 
	state = luaL_newstate();
	luaL_openlibs(state);
	// todo: should do recursively for all scripts in directory
	luaL_dofile(state, "scripts/init.lua");
	luaL_dofile(state, "scripts/object_2_on_event.lua");
	lua_pcall(state, 0, 0, 0);
}

cold::ScriptManager::~ScriptManager() {
	// finally not empty destructor!
	lua_close(state);
}

void cold::ScriptManager::exportClassesToLua() {
	// if you want to use some entity in your script, better add binding here
	Object::bind();
	PositionComponent::bind();
}

luabridge::Namespace cold::ScriptManager::getNamespace() {
	// just getter
	return luabridge::getGlobalNamespace(state);
}
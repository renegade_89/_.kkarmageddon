#pragma once

#include <application/common/utils/Singleton.h>

#include <string>
#include <memory>

/**
* @brief Loading/saving stuff here
*/

namespace cold {
	class GameModel;
	class FileManager : public uti::Singleton<FileManager> {
	public:
		/**
		* @brief Loads the corresponding level file and game state from the save file
		*
		* @param model Reference to a game model to update
		* @param name Save file name
		*/
		void loadSave(GameModel& model, const std::string& name);
		
		/**
		* @brief Writes to corresponding level file and saves state in separate file
		*
		* @param model Reference to a game model to save
		* @param name Save file name
		*/
		void writeSave(const GameModel& model, const std::string& name);

	private:
		/**
		* @brief Loads the corresponding level file
		*
		* @param model Reference to a game model to update
		* @param name Level file name
		*/
		void loadLevel(GameModel& model, const std::string& name);
		
		/**
		* @brief Writes the corresponding level file
		*
		* @param model Reference to a game model to store
		* @param name Level file name
		*/
		void writeLevel(const GameModel& model, const std::string& name);

		friend class uti::Singleton<FileManager>;
	};
}
#pragma once

/**
 * @brief Basic interface for application objects.
          All we need is to run such object.
 */

namespace cold {
	class Application {
	public:
		/**
		 * @brief Runs an application object
		 *
		 * @return Integer value indicating if operation was successful
		 */
		virtual int run() noexcept = 0;

		/**
		 * @brief "To see the right and not to do it is cowardice!" (c) Confucius
		 *
		 */
		virtual ~Application() = default;
	};
}
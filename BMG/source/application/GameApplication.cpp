#include "GameApplication.h"
#include "glut/client/Client.h"
#include "glut/server/Server.h"
#include <application/mvc/MVCClient.h>
#include <application/mvc/controller/GameController.h>
#include <application/mvc/controller/base/Controller.h>
#include <engine/min_tech/MinTechEngine.h>
#include <application/managers/ScriptManager.h>
#include <application/managers/EventManager.h>

#include <cstdlib>
#include <memory>

cold::GameApplication::GameApplication() {
	Server::getInstance();
	Engine::getInstance();
	ScriptManager::getInstance();
	EventManager::getInstance();

	ScriptManager::getInstance().exportClassesToLua();

	std::unique_ptr<Client> client(new MVCClient(std::unique_ptr<cold::Controller>(new GameController())));
	Server::getInstance().subscribe(std::move(client));
}

int cold::GameApplication::run() noexcept {
	Server::getInstance().launch();
	return EXIT_SUCCESS;
}

cold::GameApplication::~GameApplication() {
	EventManager::destroyInstance();
	ScriptManager::destroyInstance();
	Engine::destroyInstance();
	// ... remember, Sully, when I promised to kill you last?
	Server::destroyInstance();
}
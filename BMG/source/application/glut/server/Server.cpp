#include "Server.h"
#include <application/glut/client/Client.h>

#include <application/common/Common.h>

#include <ogl/GL/glew.h>
#include <ogl/GL/glut.h>

#include <cstdlib>
#include <iostream>
#include <cassert>

cold::Server::Server() {
	// this is glut stuff, nothing interesting here
	auto argumentsCount = 1;
	auto fileName = new char[MAX_PATH];
	GetModuleFileNameA(NULL, fileName, MAX_PATH);

	glutInit(&argumentsCount, &fileName);
	glutInitDisplayMode(GLUT_RGBA | GLUT_ALPHA | GLUT_DOUBLE | GLUT_DEPTH);
	assert(("You must define a valid window size", WINDOW_WIDTH > 0 && WINDOW_HEIGHT > 0));
	glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
	glutCreateWindow(fileName);
	//glutFullScreen();

	auto glew_status = glewInit();
	if (GLEW_OK != glew_status) {
		exit(EXIT_FAILURE);
	}

	if (!GLEW_VERSION_2_0) {
		exit(EXIT_FAILURE);
	}

	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutSpecialFunc(special);
	glutTimerFunc(TIMER_PERIOD, timer, 1);

	delete[] fileName;
	fileName = NULL;
}

cold::Server::~Server() {
	// I have nothing to say
	std::cout << "Shutting down GLUT..." << std::endl;
}

void cold::Server::subscribe(std::unique_ptr<Client> client) {
	// subscribe client to glut events
	assert(("Cannot perform subscription for invalid client", client != nullptr));
	getInstance().subscribers.push_back(std::move(client));
}

void cold::Server::reshape(int width, int height) {
	// notify all attached clients
	assert(("Cannot resize the window", width > 0 && height > 0));
	auto& instance = getInstance();
	for (auto& subscriber : instance.subscribers)
		subscriber->onResize(width, height);
}

#include <csetjmp>
static jmp_buf env;

void cold::Server::launch() noexcept {
	// use set jump hack since we cannot exit glut main loop
	if (!setjmp(env))
		glutMainLoop();
}

void cold::Server::special(int key, int x, int y) {
	// press home button to exit app
	if (key == GLUT_KEY_HOME)
		longjmp(env, 1);
}

void cold::Server::display() noexcept {
	// notify our subscribers that it's time to draw
	auto& instance = getInstance();
	for (auto& subscriber : instance.subscribers)
		subscriber->onDraw();
}

void cold::Server::timer(int id) noexcept {
	// 'update'
	glutTimerFunc(TIMER_PERIOD, timer, 1);
	auto& instance = getInstance();
	for (auto& subscriber : instance.subscribers)
		subscriber->onTick(id);

	glutPostRedisplay();
}
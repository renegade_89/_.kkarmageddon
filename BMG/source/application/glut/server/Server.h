#pragma once

#include <application/common/utils/Singleton.h>

#include <list>
#include <memory>

/**
 * @brief Serves attached clients: keeps on notifying them etc
 */

namespace cold {
	class Client;
	class Server : public uti::Singleton<Server> {

	private:
		std::list<std::unique_ptr<Client>> subscribers; /**< Collection of event delegates. */

	public:
		/**
		 * @brief Attaches client, so it can receive events
		 *
		 * @param client Pointer to some unique client
		 */
		void subscribe(std::unique_ptr<Client> client);

		/**
		 * @brief Starts main loop
		 */
		void launch() noexcept;

		/**
		 * @brief Deleted copy constructor since we are going to utilize server as singleton
		 *
		 * @param object Const reference to original object
		 */
		Server(const Server& object) = delete;
		
		/**
		* @brief Deleted assignment operator since we are going to utilize server as singleton
		*
		* @param object Const reference to original object
		*/
		Server& operator=(const Server& object) = delete;
		
	private:
		/**
		* @brief Performs all needed initialization stuff
		*
		*/
		Server();
		
		/**
		 * "Study the past, and you will divine the future" (c) Confucius
		 */
		~Server();

		/**
		 * @brief Callback for reshape
		 * 
		 * @param width Integer value representing new window width
		 * @param height Integer value representing new window height
		 */
		static void reshape(int width, int height);

		/**
		* @brief Callback for special
		*
		* @param key Integer value holding pressed key code
		* @param x Integer value holding horizontal position of mouse pointer
		* @param y Integer value holding vertical position of mouse pointer
		*/
		static void special(int key, int x, int y);

		/**
		* @brief Callback for display
		*
		*/
		static void display() noexcept;

		/**
		* @brief Callback for timer
		*
		* @param id Integer value representing timer id
		*/
		static void timer(int id) noexcept;

		friend class uti::Singleton<Server>; /**< Should be granted with access, he's responsible for construction and destruction */
	};
}
#pragma once

/**
 * @brief Interface used to attach to server in order to interact with user and system
 */

namespace cold {
	class Client {
	public:
		/**
		 * @brief Triggered on reshape
		 *
		 * @param width Integer value representing new window width
		 * @param height Integer value representing new window height
		 */
		virtual void onResize(int width, int height) = 0;

		/**
		* @brief Triggered on display event
		*
		*/
		virtual void onDraw() noexcept = 0;

		/**
		* @brief Triggered on timer event
		*
		* @param id Integer value representing timer id associated with the event
		*/
		virtual void onTick(int id) noexcept = 0;

		/**
		* @brief "Only the wisest and stupidest of men never change" (c) Confucius
		*
		*/
		virtual ~Client() = default;
	};
}
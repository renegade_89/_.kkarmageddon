#pragma once

#include "base/Application.h"

/**
 * @brief Upper level, responsible for initialization, running the actual game, and shutdown (e.g. kill singletons etc)
 *
 */

namespace cold {
	class GameApplication : public Application {

	public:
		/**
		* @brief Creates all singletons
		*
		*/
		GameApplication();
		
		/**
		* @brief Launches the actual game
		*
		* @return Integer value indicating result code
		*/
		virtual int run() noexcept override;
		
		/**
		* @brief Destroys all singletons
		*
		*/
		virtual ~GameApplication();
	};
}

#include "AbstractController.h"

cold::AbstractController::AbstractController() : view(NULL) {
	// any initialization stuff should be here
}

std::shared_ptr<cold::View> cold::AbstractController::getView() noexcept {
	// just getter
	return view;
}

cold::AbstractController::~AbstractController() {
	// all needed cleanup here
}
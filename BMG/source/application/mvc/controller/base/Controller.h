#pragma once

/**
* @brief Basic interface for a controller.
*        Controller is responsible for view creation, logic, and model updates.
*/

#include <memory>

namespace cold {
	class View;
	class Controller {
	public:
		/**
		* @brief Performs all needed updates and logic
		*
		*/
		virtual void doLogic() noexcept = 0;

		/**
		* @brief Returns controller's root view
		*
		* @return Smart pointer to view object
		*/
		virtual std::shared_ptr<View> getView() noexcept = 0;

		/**
		* @brief "I hear and I forget. I see and I remember. I do and understand" (c) Confucius
		*
		*/
		virtual ~Controller() = default;
	};
}
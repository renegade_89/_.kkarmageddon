#pragma once

/**
* @brief Common functionality for controllers.
*        Controller is responsible for view creation, logic, and model updates.
*/

#include <application/mvc/controller/base/Controller.h>
#include <application/mvc/model/base/Model.h>

#include <memory>

namespace cold {
	class AbstractController : public Controller {
	protected:
		std::shared_ptr<View> view; /**< Smart pointer to a shared view object. */

	public:
		/**
		* @brief Performs all needed initialization stuff
		*
		*/
		AbstractController();

		/**
		* @brief Returns controller's root view
		*
		* @return Smart pointer to view object
		*/
		virtual std::shared_ptr<View> getView() noexcept override;

		/**
		* @brief "Wisdom, compassion, and courage are the three universially recognized moral qualities of men"(c) Confucius
		*
		*/
		virtual ~AbstractController();
	};
}
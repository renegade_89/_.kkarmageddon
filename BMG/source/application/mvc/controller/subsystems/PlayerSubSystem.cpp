#include <application/mvc/controller/subsystems/PlayerSubSystem.h>
#include <application/mvc/model/GameModel.h>
#include <application/mvc/model/input_model/InputModel.h>
#include <application/mvc/model/gom/object/Object.h>
#include <application/mvc/model/location/Location.h>
#include <application/mvc/model/gom/object/components/PositionComponent.h>
#include <application/mvc/model/gom/object/components/DirectionComponent.h>
#include <application/common/Common.h>

#include <ogl/glm/gtc/matrix_transform.hpp>

#include <cassert>
#include <iostream>

const double cold::PlayerSubSystem::PI = 3.14159263;
const double cold::PlayerSubSystem::EPSILON = 0.01;

void cold::PlayerSubSystem::execute(std::shared_ptr<GameModel> model) noexcept {
	auto mouseDX = 0;
	auto mouseDY = 0;
	
	assert(("Cannot execute logic on empty model", model != nullptr));
	assert(("Cannot process user input", model->getInputModel() != nullptr));
	model->getInputModel()->getObservationDirectionState(mouseDX, mouseDY);
	assert(("Cannot access current location", model->getCurrentLocation() != nullptr));
	auto player = model->getCurrentLocation()->getFirstOf(Object::OBJECT_ID_TEST_PLAYER_0);
	assert(("Player object missing", player != nullptr));
	assert(("Player object missing direction component", player->getDirectionComponent() != nullptr));
	auto direction = player->getDirectionComponent();
	assert(("Player object missing position component", player->getPositionComponent() != nullptr));
	auto position = player->getPositionComponent();

	// mouse dx means turning left or right, depending on the sign
	if (mouseDX != 0) {
		double alpha = 180.0 * mouseDX / WINDOW_WIDTH;
		auto& at = direction->getAt();
		auto transformation = glm::rotate(glm::dmat4(1.0f), alpha, glm::dvec3(0.0f, 1.0f, 0.0f));
		glm::dvec3 result(glm::dvec4(at, 1.0) * transformation);
		direction->setAt(result);
	}
	
	// turning our head up or down, more restrictions here
	// uses some sort of cylinder system (todo: quaternion camera)
	if (mouseDY != 0) {
		auto angle = 0.0;
		auto projection = glm::dvec2(direction->getAt().x, direction->getAt().z);
		if (direction->getAt().y != 0)
			angle = acos(sqrt(projection.x * projection.x + projection.y * projection.y) / sqrt(direction->getAt().x * direction->getAt().x + direction->getAt().y * direction->getAt().y + direction->getAt().z * direction->getAt().z));
		if (direction->getAt().y < 0)
			angle = -angle;

		angle -= PI * mouseDY / WINDOW_WIDTH;
		if (angle > PI / 2.0f - EPSILON)
			angle = PI / 2.0f - EPSILON;
		if (angle < - PI / 2.0f + EPSILON)
			angle = - PI / 2.0f + EPSILON;

		auto newY = fabs(sqrt(projection.x * projection.x + projection.y * projection.y) / cos(angle));
		newY = sqrt(fabs(newY * newY - direction->getAt().x * direction->getAt().x - direction->getAt().z * direction->getAt().z));
		if (angle < 0)
			newY = -newY;
		direction->setAt(glm::dvec3(direction->getAt().x, newY, direction->getAt().z));
	}

	// process movement keys
	const auto coef = 100 * SI::CM;
	auto heading = model->getInputModel()->getMovementDirectionState();
	switch (heading) {
	case cold::InputModel::DIRECTION_STATE_WEST: 
		position->setPosition(position->getPosition() - direction->getSide() * coef);
		break;
	case cold::InputModel::DIRECTION_STATE_NORTH: 
		position->setPosition(position->getPosition() + direction->getAt() * coef);
		break;
	case cold::InputModel::DIRECTION_STATE_EAST: 
		position->setPosition(position->getPosition() + direction->getSide() * coef);
		break;
	case cold::InputModel::DIRECTION_STATE_SOUTH: 
		position->setPosition(position->getPosition() - direction->getAt() * coef);
		break;
	case cold::InputModel::DIRECTION_STATE_NORTH_WEST: 
		position->setPosition(position->getPosition() + (direction->getAt() - direction->getSide()) / sqrt(2) * coef);
		break;
	case cold::InputModel::DIRECTION_STATE_NORTH_EAST: 
		position->setPosition(position->getPosition() + (direction->getAt() + direction->getSide()) / sqrt(2) * coef);
		break;
	case cold::InputModel::DIRECTION_STATE_SOUTH_WEST: 
		position->setPosition(position->getPosition() + (-direction->getAt() - direction->getSide()) / sqrt(2) * coef);
		break;
	case cold::InputModel::DIRECTION_STATE_SOUTH_EAST: 
		position->setPosition(position->getPosition() + (-direction->getAt() + direction->getSide()) / sqrt(2) * coef);
		break;
	}
}
#include <application/mvc/controller/subsystems/CollisionControlSubSystem.h>

#include <application/mvc/model/GameModel.h>
#include <application/mvc/model/location/Location.h>
#include <application/mvc/model/gom/object/Object.h>
#include <application/mvc/model/gom/object/components/PositionComponent.h>
#include <application/mvc/model/gom/object/components/DirectionComponent.h>
#include <application/mvc/model/gom/object/components/BoundingBoxComponent.h>
#include <application/managers/ScriptManager.h>
#include <application/managers/EventManager.h>
#include <engine/min_tech/MinTechEngine.h>


cold::CollisionControlSubSystem::CollisionControlSubSystem() : SubSystem() {
	// turn on/off bounding boxes drawing (for debug purposes)
	Engine::getInstance().toggleBoundingBoxesDrawing();
}

void cold::CollisionControlSubSystem::execute(std::shared_ptr<GameModel> model) noexcept {
	// the actual collision test should be performed by engine, since real geometry resides on his side
	this->model = model;
	Engine::getInstance().performCollisionTest(*this);
	this->model = nullptr;
}

unsigned int cold::CollisionControlSubSystem::numberOfActors() const {
	// temporarily just one actor, the player
	return 1;
}

glm::dmat4 cold::CollisionControlSubSystem::boundingBoxTransformationForActor(int index) const {
	// compute the full transofrmation matrix for a boundig box unit, according to actor's state ad properties
	auto player = model->getCurrentLocation()->getFirstOf(Object::OBJECT_ID_TEST_PLAYER_0);
	auto& dimensions = player->getBoundingBoxComponent()->getDimensions();
	auto& direction = player->getDirectionComponent()->getAt();
	auto& position = player->getPositionComponent()->getPosition();
	auto angle = atan2(direction.x, direction.z);
	
	auto translate = glm::translate(glm::dmat4(1.0), position);
	auto rotate = glm::rotate(glm::dmat4(1.0), angle / 3.1415926 * 180.0, glm::dvec3(0.0, 1.0, 0.0));
	auto scale = glm::scale(glm::dmat4(1.0), glm::dvec3(dimensions.x, dimensions.y, dimensions.z));
	return translate * rotate * scale;
}

void cold::CollisionControlSubSystem::onCollision(int index) {
	// in case a collision was detected actually, the engine will delegate the handling back to us
	// (currently, the handler in a script just moves the object to his previos positio before collision)
	if (index == 0) {
		auto player = model->getCurrentLocation()->getFirstOf(Object::OBJECT_ID_TEST_PLAYER_0);
		EventManager::getInstance().send(player, Event::EVENT_DEBUG_COLLISION_TEST_POSITIVE);
	}
}
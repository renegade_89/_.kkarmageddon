#pragma once

/**
* @brief Player's sub system
*        Better place all player related logic here
*/

#include <application/mvc/controller/subsystems/base/SubSystem.h>

namespace cold {
	class PlayerSubSystem : public SubSystem{
		static const double PI; /**< PI constant value */
		static const double EPSILON; /**< Some little threshold value */

	public:
		/**
		* @brief Currently keeping track of users movements and updating dedicated components
		*
		*/
		virtual void execute(std::shared_ptr<GameModel> model) noexcept override;
	};
}
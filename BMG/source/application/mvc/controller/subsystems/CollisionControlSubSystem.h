#pragma once

#include <application/mvc/controller/subsystems/base/SubSystem.h>
#include <engine/min_tech/interfaces/CollisionTestDelegate.h>

/**
* @brief Everything related to collisions handling in client is located here
*        Actual collision tests are performed by engine but it needs some sort of delegate in client code.
*/

namespace cold {
	class CollisionControlSubSystem : public SubSystem, public mte::CollisionTestDelegate {
		std::shared_ptr<GameModel> model; /**< Shared to pointer to our model, needed for delegate methods */

	public:
		/**
		* @brief Should perform all needed initialiation stuff
		*
		*/
		CollisionControlSubSystem();

		/**
		* @brief Invokes engine's collision test routines
		*
		* @param model Shared handle of model in our application
		*/
		virtual void execute(std::shared_ptr<GameModel> model) noexcept override;

		/**
		* @brief Returns number of objects, which may collide with something/someone in our game
		*
		* @return Unsigned integer value of actors quantity
		*/
		virtual unsigned int numberOfActors() const override;

		/**
		* @brief Computes final transformation matrix for specific actor bounding box
		*
		* @param index Integer value holding index of an actor
		* @return 4x4 matrix of doubles i.e. transformation matrix
		*/
		virtual glm::dmat4 boundingBoxTransformationForActor(int index) const override;

		/**
		* @brief This delegate method is to be triggered by engine if collision(s) is detected for some actor
		*
		* @param index Integer value holding index of an actor
		*/
		virtual void onCollision(int index) override;
	};
}
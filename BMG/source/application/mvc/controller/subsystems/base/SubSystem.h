#pragma once

/**
* @brief Basic interface for a controller's sub system.
*        This is introduced since if there are a lot of objects with various componenets it will be nice
*			to separate controller's logic into several subsystems and pass a model to them
*/

#include <memory>

namespace cold {
	class GameModel;
	class SubSystem {
	public:
		SubSystem() = default;
		
		/**
		* @brief Executes logic on certain subset of model items (may modify them so no consts here)
		*
		*/
		virtual void execute(std::shared_ptr<GameModel> model) noexcept = 0;

		/**
		* @brief performs all needed clenup
		*
		*/
		virtual ~SubSystem() = default;
	};
}
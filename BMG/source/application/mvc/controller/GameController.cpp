#include <application/mvc/controller/GameController.h>
#include <application/mvc/model/GameModel.h>
#include <application/mvc/model/input_model/InputModel.h>
#include <application/mvc/view/GameView.h>

#include <application/managers/EventManager.h>
#include <application/managers/FileManager.h>

#include "subsystems/PlayerSubSystem.h"
#include "subsystems/CollisionControlSubSystem.h"

#include <iostream>
#include <cassert>

cold::GameController::GameController() {
	// creates model and constructs a test view, which will be feeded with our model
	model = std::shared_ptr<GameModel>(new GameModel(""));
	view = std::shared_ptr<View>(new GameView(model));
	subsystems.emplace_back(new PlayerSubSystem());
	subsystems.emplace_back(new CollisionControlSubSystem());
}

void cold::GameController::doLogic() noexcept {
	// perform all needed logic
	assert(("Cannot execute logic on empty model", model != nullptr));
	for (auto& subsystem : subsystems)
		subsystem->execute(model);

	// todo: do we need both 'subsystems' and 'managers'? can't it be just one type?
	EventManager::getInstance().update();

	// not sure about this anymore, but... we'll look at it later
	model->update(-1);
}

cold::GameController::~GameController() {
	// all needed cleanup here
}
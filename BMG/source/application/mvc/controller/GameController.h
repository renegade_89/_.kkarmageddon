#pragma once

#include "base/AbstractController.h"

#include <vector>

/**
* @brief Contains demo logic.
*
*/

namespace cold {
	class GameModel;
	class SubSystem;
	class GameController : public AbstractController {
		std::shared_ptr<GameModel> model; /**< Smart pointer to a shared model object. */
		std::vector<std::unique_ptr<SubSystem>> subsystems; /**< Smart pointer to a shared model object. */

	public:
		/**
		* @brief Performs all needed initialization stuff
		*
		*/
		GameController();

		/**
		* @brief Performs all needed updates and logic
		*
		*/
		virtual void doLogic() noexcept override;

		/**
		* @brief "Silence is a true friend, which never betrays" (c) Confucius
		*
		*/
		virtual ~GameController();
	};
}
#pragma once

#include <string>

/**
* @brief Acts as common interface for view's model
*/
namespace cold {
	class Model {
	public:
		/**
		* @brief Updates the model smoothly
		*
		* @param delta mount of milliseconds that passed after the last call
		*/
		virtual void update(int delta) noexcept = 0;

		/**
		* @brief "Wherever you go, go with your heart" (c) Confucius
		*/
		virtual ~Model() = default;
	};
}
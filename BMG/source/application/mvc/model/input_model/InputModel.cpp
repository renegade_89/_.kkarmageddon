#include "InputModel.h"

#include <cassert>

void cold::InputModel::update() noexcept {
	auto returnCode = lpdimouse->GetDeviceState(sizeof(DIMOUSESTATE), (LPVOID)&mouse_state);
	assert(("Can't get mouse device object state", returnCode == DI_OK));
	returnCode = lpdikeyboard->GetDeviceState(sizeof(dikeys) / sizeof(dikeys[0]), dikeys);
	assert(("Can't get keyboard device object state", returnCode == DI_OK));
	if (dikeys[DIK_W] & 0x80) {
		if (dikeys[DIK_A] & 0x80) {
			// north west
			movementDirectionState = DIRECTION_STATE_NORTH_WEST;
		} else if (dikeys[DIK_D] & 0x80) {
			// north east
			movementDirectionState = DIRECTION_STATE_NORTH_EAST;
		} else {
			// north
			movementDirectionState = DIRECTION_STATE_NORTH;
		}
	} else if (dikeys[DIK_S] & 0x80) {
		if (dikeys[DIK_A] & 0x80) {
			// south west
			movementDirectionState = DIRECTION_STATE_SOUTH_WEST;
		} else if (dikeys[DIK_D] & 0x80) {
			// south east
			movementDirectionState = DIRECTION_STATE_SOUTH_EAST;
		} else {
			// south
			movementDirectionState = DIRECTION_STATE_SOUTH;
		}
	} else if (dikeys[DIK_A] & 0x80) {
		// west
		movementDirectionState = DIRECTION_STATE_WEST;
	} else if (dikeys[DIK_D] & 0x80) {
		// east
		movementDirectionState = DIRECTION_STATE_EAST;
	} else {
		movementDirectionState = DIRECTION_STATE_STAY;
	}
	if (dikeys[DIK_1] & 0x80) {
		saveRequested = true;
	} else {
		saveRequested = false;
	}
	if (dikeys[DIK_2] & 0x80) {
		loadRequested = true;
	}
	else {
		loadRequested = false;
	}
}

cold::InputModel::InputModel() : lpdi(NULL), lpdimouse(NULL), movementDirectionState(DIRECTION_STATE_INVALID) {
	struct DirectInputCreateException : public std::exception { char const * what() const { return "Can't create direct input object"; } };
	struct CreateDeviceException : public std::exception { char const * what() const { return "Can't create device object"; } };
	struct SetCooperativeLevelException : public std::exception { char const * what() const { return "Can't set cooperative level"; } };
	struct SetDataFormatException : public std::exception { char const * what() const { return "Can't set data format"; } };
	struct AcquireException : public std::exception { char const * what() const { return "Can't set data format"; } };
	
	// first create the direct input object
	auto returnCode = DirectInput8Create(GetModuleHandle(NULL), DIRECTINPUT_VERSION, IID_IDirectInput8, (void **)&lpdi, NULL);
	if (returnCode != DI_OK)
		throw DirectInputCreateException();
	
	// create a mouse device  /////////////////////////////////////
	returnCode = lpdi->CreateDevice(GUID_SysMouse, &lpdimouse, NULL);
	if (returnCode != DI_OK)
		throw CreateDeviceException();
	// set cooperation level
	returnCode = lpdimouse->SetCooperativeLevel(GetActiveWindow(), DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
	if (returnCode != DI_OK)
		throw SetCooperativeLevelException();
	// set data format
	returnCode = lpdimouse->SetDataFormat(&c_dfDIMouse);
	if (returnCode != DI_OK)
		throw SetDataFormatException();
	// acquire the mouse
	returnCode = lpdimouse->Acquire();
	if (returnCode != DI_OK)
		throw AcquireException();
	memset(&mouse_state, 0, sizeof(mouse_state));

	returnCode = lpdi->CreateDevice(GUID_SysKeyboard, &lpdikeyboard, NULL);
	if (returnCode != DI_OK)
		throw CreateDeviceException();
	returnCode = lpdikeyboard->SetCooperativeLevel(GetActiveWindow(), DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
	if (returnCode != DI_OK)
		throw SetCooperativeLevelException();
	returnCode = lpdikeyboard->SetDataFormat(&c_dfDIKeyboard);
	if (returnCode != DI_OK)
		throw SetDataFormatException();
	returnCode = lpdikeyboard->Acquire();
	if (returnCode != DI_OK)
		throw AcquireException();
	memset(dikeys, 0, sizeof(dikeys) / sizeof(dikeys[0]) * sizeof(dikeys[0]));
}

cold::InputModel::~InputModel() {
	auto returnCode = lpdikeyboard->Unacquire();
	assert(("Unacquire failed for keyboard", returnCode == DI_OK));
	returnCode = lpdikeyboard->Release();
	assert(("Cannot release the keyboard", returnCode == DI_OK));

	// first unacquire the mouse
	returnCode = lpdimouse->Unacquire();
	assert(("Unacquire failed for mouse", returnCode == DI_OK));
	// now release the mouse
	returnCode = lpdimouse->Release();
	assert(("Cannot release the keyboard", returnCode == DI_OK));

	// release directinput
	returnCode = lpdi->Release();
	assert(("Cannot release direct input object", returnCode == DI_OK));
}
#pragma once

/**
* @brief Some sort of a wrapper over direct input functionality
*		 This is not the final design, class be removed in future
*
*/

#define DIRECTINPUT_VERSION 0x0800

#include <dinput.h>

#include <cstdint>
#include <utility>
#include <exception> 

namespace cold {
	class InputModel {
	public:
		enum DirectionState { DIRECTION_STATE_INVALID, DIRECTION_STATE_STAY, DIRECTION_STATE_NORTH, DIRECTION_STATE_NORTH_EAST, DIRECTION_STATE_EAST, DIRECTION_STATE_SOUTH_EAST, DIRECTION_STATE_SOUTH, DIRECTION_STATE_SOUTH_WEST, DIRECTION_STATE_WEST, DIRECTION_STATE_NORTH_WEST, NUM_DIRECTION_STATES }; /**< Where are we going? */

	private:
		LPDIRECTINPUT8 lpdi; /**< Interface to enumerate, create, and retrieve the status of Microsoft DirectInput devices, initialize the DirectInput object etc */
		LPDIRECTINPUTDEVICE8 lpdimouse; /**< Interface to gain and release access to mouse, manage it's properties and information, set behavior, perform initialization etc */
		LPDIRECTINPUTDEVICE8 lpdikeyboard; /**< Interface to gain and release access to keyboard, manage it's properties and information, set behavior, perform initialization etc */
		DIMOUSESTATE mouse_state; /** Describes the state of a mouse device that has up to four buttons, or another device that is being accessed as if it were a mouse device */
		DirectionState movementDirectionState; /**< Where are you going? */
		static const int NUM_KEYS = 256; /**< 256 keys should be enough for our demo */
		uint8_t dikeys[NUM_KEYS]; /**< This data should represent the state of the keyboard */
		bool saveRequested; /**< Boolean flag set if user has requested game saving */
		bool loadRequested; /**< Boolean flag set if user has requested game save file loading */

	public:	
		/**
		* @brief Performs direct input initialization, acquires input devices
		*
		*/
		InputModel();

		/**
		* @brief Shuts down direct input
		*
		*/
		~InputModel();

		/**
		* @brief Provides last detected movement direction to calling side
		*
		* @return Enumerated value representing the direction
		*/
		DirectionState getMovementDirectionState() const { return movementDirectionState; }

		/**
		* @brief Retrieves currently detected mouse shift
		*
		* @return Integer value holding horizontal shift
		* @return Integer value holding vertical shift
		*/
		void getObservationDirectionState(int &dx, int &dy) const { dx = mouse_state.lX, dy = mouse_state.lY; }

		/**
		* @brief Checks if save request flag is set
		*
		* @return Boolean value indicating if save was requested
		*/
		bool isSaveRequested() const { return saveRequested; }

		/**
		* @brief Checks if load request flag is set
		*
		* @return Boolean value indicating if load was requested
		*/
		bool isLoadRequested() const { return loadRequested; }

		/**
		* @brief Asynchronous read of devices states
		*
		*/
		void update() noexcept;
	};
}
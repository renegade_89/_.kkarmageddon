#pragma once

/**
* @brief Container for game objects
*
*/

#include <application/common/utils/GUID.h>

#include <map>
#include <memory>

namespace cold {
	class Object;
	class FileManager;
	using ObjectCollection = std::multimap<int, std::shared_ptr<Object>>;
	class Location {
		ObjectCollection objects; /**< Collection of shared handles to location's objects */
	
	public:
		/**
		* @brief Export needed functions to lua
		*
		*/
		static void bind();

		/**
		* @brief Adds an object to this location
		*
		* @param object Shared handle to some game object
		*/
		void addObject(std::shared_ptr<Object> object);

		/**
		* @brief Removes an object from this location
		*
		* @param object Shared handle to some game object
		*/
		void removeObject(std::shared_ptr<Object> object);

		/**
		* @brief Removes all obects in this location
		*
		*/
		void removeAllObjects();

		/**
		* @brief Retrieves the first object of given id for reading
		*
		* @param objectID Integer value which identifies the object type
		* @return Shared handler to non modifiable object (e.g. to present it)
		*/
		std::shared_ptr<const Object> getFirstOf(int objectID) const noexcept;

		/**
		* @brief Retrieves the first object of given id for read / write
		*
		* @param objectID Integer value which identifies the object type
		* @return Shared handler to modifiable object (e.g. toprocess and update it)
		*/
		std::shared_ptr<Object> getFirstOf(int objectID) noexcept;

		/**
		* @brief Retrieves iterators to objects with given id
		*
		* @param objectID Integer value which identifies the object type
		* @return Pair of iterators? startinf and ending 
		*/
		std::pair<ObjectCollection::const_iterator, ObjectCollection::const_iterator> getRangeOf(int objectID) const;

		/**
		* @brief Returns pointer to an object if its id and guid is already known (is this overkill?) 
		*
		* @param objectID Integer value which identifies the object type
		* @param guid Object unique identifier
		* @return Shared pointer to unmodifiable object (e.g. toprocess and update it)
		*/
		std::shared_ptr<const Object> get(int objectID, const uti::GUID &guid) const noexcept;
		
		/**
		* @brief Returns pointer to an object if its id and guid is already known (is this overkill?)
		*
		* @param objectID Integer value which identifies the object type
		* @param guid Object unique identifier
		* @return Shared pointer to modifiable object (e.g. toprocess and update it)
		*/
		std::shared_ptr<Object> get(int objectID, const uti::GUID &guid) noexcept;

		friend class FileManager;
	};
}
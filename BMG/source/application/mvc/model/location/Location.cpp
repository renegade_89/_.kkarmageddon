#include <application/mvc/model/location/Location.h>
#include <application/mvc/model/gom/object/Object.h>
#include <application/managers/ScriptManager.h>
#include <application/managers/EventManager.h>

#include <cassert>

void cold::Location::bind() {
}

void cold::Location::addObject(std::shared_ptr<Object> object) {
	assert(("Cannot add null object", object != nullptr));
	objects.insert(std::make_pair(object->getID(), object));
	if (object->getBoundingBoxComponent())
		EventManager::getInstance().subscribe(object, Event::EVENT_DEBUG_COLLISION_TEST_POSITIVE);
}

void cold::Location::removeObject(std::shared_ptr<Object> object) {
	auto range = objects.equal_range(object->getID());
	for (auto i = range.first; i != range.second; ++i)
		if (i->second.get() == object.get()) {
			objects.erase(i);
			return;
		}
}

void cold::Location::removeAllObjects() {
	objects.clear();
}

std::shared_ptr<const cold::Object> cold::Location::getFirstOf(int objectID) const noexcept {
	auto range = objects.equal_range(objectID);
	assert(("Objects of that type not found", range.first != objects.end()));
	return range.first->second;
}

std::shared_ptr<cold::Object> cold::Location::getFirstOf(int objectID) noexcept {
	auto range = objects.equal_range(objectID);
	assert(("Objects of that type not found", range.first != objects.end()));
	return range.first->second;
}

std::shared_ptr<const cold::Object> cold::Location::get(int objectID, const uti::GUID &guid) const noexcept {
	auto range = objects.equal_range(objectID);
	assert(("Objects of that type not found", range.first != objects.end()));
	for (auto current = range.first; current != range.second; std::advance(current, 1)) {
		if (current->second->getGUID() == guid)
			return current->second;
	}
	return nullptr;
}

std::shared_ptr<cold::Object> cold::Location::get(int objectID, const uti::GUID &guid) noexcept {
	auto range = objects.equal_range(objectID);
	assert(("Objects of that type not found", range.first != objects.end()));
	for (auto current = range.first; current != range.second; std::advance(current, 1)) {
		if (current->second->getGUID() == guid)
			return current->second;
	}
	return nullptr;
}

std::pair<cold::ObjectCollection::const_iterator, cold::ObjectCollection::const_iterator> cold::Location::getRangeOf(int objectID) const {
	return objects.equal_range(objectID);
}
#include <application/mvc/model/GameModel.h>
#include <application/mvc/model/location/Location.h>
#include <application/mvc/model/gom/GameObjectManager.h>
#include <application/mvc/model/gom/object/Object.h>
#include <application/mvc/model/gom/object/components/TerrainComponent.h>
#include <application/mvc/model/gom/object/components/PositionComponent.h>
#include <application/mvc/model/input_model/InputModel.h>
#include <application/common/Common.h>

#include <application/managers/FileManager.h>

#include <ogl/glm/gtc/matrix_transform.hpp>

#include <sstream>

cold::GameModel::GameModel(const std::string& fileName) : location(new Location()) {
	if (fileName.empty()) {
		//  - terrain
		std::shared_ptr<Object> terrain = cold::GameObjectManager::getInstance().createObject(Object::OBJECT_ID_TEST_TERRAIN_0);
		terrain->getTerrainComponent()->setScale(glm::dvec3(20 * SI::M, 300 * SI::M, 20 * SI::M));
		location->addObject(terrain);
		// - player
		std::shared_ptr<Object> player = cold::GameObjectManager::getInstance().createObject(Object::OBJECT_ID_TEST_PLAYER_0);
		player->getPositionComponent()->setPosition(glm::dvec3(0.0, 5 * 300 * SI::M, 0.0));
		location->addObject(player);
		
		// - stars
		for (int i = 0; i < 1000; ++i) {
			std::shared_ptr<Object> star = cold::GameObjectManager::getInstance().createObject(Object::OBJECT_ID_STAR);
			location->addObject(star);
		}
		// - heavenly body
		std::shared_ptr<Object> luminary = cold::GameObjectManager::getInstance().createObject(Object::OBJECT_ID_LUMINARY);
		location->addObject(luminary);

		// - distant objects
		std::shared_ptr<Object> horizon = cold::GameObjectManager::getInstance().createObject(Object::OBJECT_ID_HORIZON);
		location->addObject(horizon);

	} else {
		// load from file
		FileManager::getInstance().loadSave(*this, fileName);
	}
	inputModel = std::shared_ptr<InputModel>(new InputModel());
}

std::shared_ptr<const cold::Location> cold::GameModel::getCurrentLocation() const noexcept {
	return location;
}

std::shared_ptr<cold::Location> cold::GameModel::getCurrentLocation() noexcept {
	return location;
}

void cold::GameModel::update(int delta) noexcept {
	// update location (locations?)
	if (getInputModel()->isSaveRequested())
		FileManager::getInstance().writeSave(*this, "0.LEVEL");

	inputModel->update();
}

cold::GameModel::~GameModel() {
	// clean up
}
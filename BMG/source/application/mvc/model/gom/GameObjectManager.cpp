#include <application/mvc/model/gom/GameObjectManager.h>
#include <application/mvc/model/gom/object/Object.h>
#include <application/mvc/model/gom/object/components/TerrainComponent.h>
#include <application/mvc/model/gom/object/components/PositionComponent.h>
#include <application/mvc/model/gom/object/components/DirectionComponent.h>
#include <application/mvc/model/gom/object/components/BoundingBoxComponent.h>
#include <application/common/Common.h>

#include <ogl/glm/gtc/noise.hpp>

#include <libs/noise/k_noise_lib.h>
#include <libs/algorithm/k_alg_lib.h>

#include <algorithm>
#include <vector>
#include <cstdint>
#include <cmath>
#include <cassert>

std::unique_ptr<cold::Object> cold::GameObjectManager::createObject(int objectID) {
	// for now we don't have too much objects, so they all are constructed explicitly by id here
	assert(("Object id is invalid, consult user manual", objectID > Object::OBJECT_ID_INVALID && objectID < Object::NUM_OBJECT_IDS));
	switch (objectID) {
	case Object::OBJECT_ID_TEST_TERRAIN_0: {
		auto object = std::make_unique<Object>();
		const auto MIN_LEVEL = 0.3f;
		const auto MAX_LEVEL = 0.75f;	
		// generate landscape
		std::vector<float> noise_a(MAP_SIZE * MAP_SIZE);
		std::vector<float> noise_b(MAP_SIZE * MAP_SIZE);
		k_noise_init();
		k_noise_generate_noise(noise_a.data(), MAP_SIZE, MAP_SIZE, 2.0f + 2.0f * rand() / RAND_MAX, 3);
		k_noise_generate_noise(noise_b.data(), MAP_SIZE, MAP_SIZE, 12.0f + 2.0f * rand() / RAND_MAX, 4);
		std::transform(noise_a.begin(), noise_a.end(), noise_b.begin(), noise_a.begin(), 
			[](auto a, auto b) -> auto { return (a * 0.75f + a * b * 0.25f) * 0.5f; });
		k_noise_generate_noise(noise_b.data(), MAP_SIZE, MAP_SIZE, 1.0f, k_noise_DEFAULT_NUM_OCTAVES);
		std::transform(noise_a.begin(), noise_a.end(), noise_b.begin(), noise_a.begin(),
			[](auto a, auto b) -> auto { return a + b * 0.5f; });
		k_noise_transform(noise_a.data(), MAP_SIZE, MAP_SIZE, 0.0f, 1.0f);
		k_noise_generate_noise(noise_b.data(), MAP_SIZE, MAP_SIZE, 12.0f + 2.0f * rand() / RAND_MAX, 4);
		std::transform(noise_a.begin(), noise_a.end(), noise_b.begin(), noise_a.begin(),
			[](auto a, auto b) -> auto { return a * 0.85f + b * 0.15f; });
		k_noise_generate_paraboloid(noise_b.data(), MAP_SIZE, MAP_SIZE);
		std::transform(noise_a.begin(), noise_a.end(), noise_b.begin(), noise_a.begin(),
			[](auto a, auto b) -> auto { return a * b < 0.1333334f ? 0.0f : a * b > 1.0f ? 1.0f : a * b; });
		k_noise_transform(noise_a.data(), MAP_SIZE, MAP_SIZE, 0.0f, 1.0f);
		k_noise_shutdown();
		
		// generate locations
		/*const int NUM_LOCATIONS = 5;
		std::vector<int> indices;
		for (int i = 0; i < MAP_SIZE * MAP_SIZE; i += MAP_SIZE / 16) {
			if (noise_a[i] > MIN_LEVEL && noise_a[i] < MAX_LEVEL)
				indices.push_back(i);
		}
		assert(("Cannot locate nodes on map", indices.size() >= NUM_LOCATIONS));
		std::random_shuffle(indices.begin(), indices.end());
		indices.resize(NUM_LOCATIONS);*/
		
		std::vector<int8_t> paths(MAP_SIZE * MAP_SIZE);
		// generate graph and paths
		/*std::vector<double> adjacency(NUM_LOCATIONS * NUM_LOCATIONS);
		auto weight = [](float a, float b) -> double { if (a < MIN_LEVEL || b < MIN_LEVEL || a > MAX_LEVEL || b > MAX_LEVEL) return 1000000.0; else return pow(5, 20 * (fabs(a - b) + 0.1 * b)); };
		auto heuristic = [](double goalX, double goalY, double currentX, double currentY) -> double {
			return ((goalX - currentX) * (goalX - currentX) + (goalY - currentY) * (goalY - currentY));
		};
		k_alg_generate_graph(noise_a.data(), MAP_SIZE, MAP_SIZE, indices.data(), NUM_LOCATIONS, adjacency.data(), weight);
		
		k_alg_generate_paths(noise_a.data(), MAP_SIZE, MAP_SIZE, indices.data(), NUM_LOCATIONS, adjacency.data(), paths.data(), weight, heuristic);
		*/

		// initialize terrain component
		object->id = objectID;
		std::transform(noise_a.begin(), noise_a.end(), noise_a.begin(),
			[](auto a) -> auto { return a < 0.77f ? a : (a + powf(10.0f, a - 0.76999f) - 1.0f); });
		object->terrainComponent = std::shared_ptr<TerrainComponent>(new TerrainComponent(noise_a, MAP_SIZE, MAP_SIZE, glm::dvec3(1.0, 1.0, 1.0), paths));
		return object;
	}
	break;
	case Object::OBJECT_ID_TEST_PLAYER_0: {
		auto object = std::make_unique<Object>();
		
		object->id = objectID;
		object->positionComponent = std::shared_ptr<PositionComponent>(new PositionComponent(0, 0, 0));
		object->directionComponent = std::shared_ptr<DirectionComponent>(new DirectionComponent(0.0f, 0.0f, -1.0f));
		object->boundingBoxComponent = std::shared_ptr<BoundingBoxComponent>(new BoundingBoxComponent(80 * SI::CM, 190 * SI::CM, 45 * SI::CM));
		return object;
	}
	break;
	case Object::OBJECT_ID_HORIZON: {
		auto object = std::make_unique<Object>();
		
		object->id = objectID;
		auto width = static_cast<unsigned int>(WINDOW_WIDTH / FOV * 360.0);
		std::vector<float> noise(width * 1);

		// generate noise using glm lib
		auto height = 10; // 10 options
		std::vector<float> data(width * height);
		auto xFactor = 1.0f / (width - 1);
		auto yFactor = 1.0f / (height - 1);
		auto a = 6 + 1.0f * rand() / RAND_MAX * 4.0f; // magic number #1
		auto b = 2 + 1.0f * rand() / RAND_MAX * 1.5f; // magic number #2
		for (auto row = 0U; row < height; ++row) {
			for (auto column = 0U; column < width; ++column) {
				auto x = xFactor * column;
				auto y = yFactor * row;
				auto sum = 0.0f;
				auto frequency = a;
				auto scale = b;
				for (auto octave = 0U; octave < 4; ++octave) {
					glm::vec2 point(x * frequency, y * frequency);
					auto value = glm::perlin(point) / scale;
					sum += value;
					auto result = (sum + 1.0f) / 2.0f;
					data[row * width + column] = result;
					frequency *= 2.0f;
					scale *= b;
				}
			}
		}
		auto offset = (rand() % height) * width;
		for (auto column = 0U; column < width; ++column)
			noise[column] = data[offset + column];
		auto max = *std::max_element(noise.cbegin(), noise.cend());
		auto min = *std::min_element(noise.cbegin(), noise.cend());
		std::transform(noise.begin(), noise.end(), noise.begin(),
			[&max, &min](auto data) -> auto { return (data - min) / (max - min) * 1.0f; });
		object->terrainComponent = std::shared_ptr<TerrainComponent>(new TerrainComponent(noise, width, 1, glm::dvec3(1.0, 1.0, 1.0)));
		return object;
	}
	break;
	case Object::OBJECT_ID_STAR: {
		auto object = std::make_unique<Object>();
		
		object->id = objectID;
		glm::vec4 position(1.0f * rand() / RAND_MAX - 0.5f, 0.5f * rand() / RAND_MAX, 1.0f * rand() / RAND_MAX - 0.5f, 1.0f);
		position = glm::normalize(position);
		object->positionComponent = std::shared_ptr<PositionComponent>(new PositionComponent(position.x, position.y, position.z));
		return object;
	}
	break;
	case Object::OBJECT_ID_LUMINARY: {
		auto object = std::make_unique<Object>();
		
		object->id = objectID;
		glm::vec4 position(1.0f * rand() / RAND_MAX - 0.5f, 0.5f * rand() / RAND_MAX, 1.0f * rand() / RAND_MAX - 0.5f, 1.0f);
		position = glm::normalize(position);
		object->positionComponent = std::shared_ptr<PositionComponent>(new PositionComponent(position.x, position.y, position.z));
		
		std::vector<float> landscape(LUMINARY_SIZE * LUMINARY_SIZE);
		k_noise_init();
		k_noise_generate_noise(landscape.data(), LUMINARY_SIZE, LUMINARY_SIZE, 2.0f + 12.0f * rand() / RAND_MAX, 2 + rand() % 3);
		k_noise_transform(landscape.data(), LUMINARY_SIZE, LUMINARY_SIZE, 0.0f, 1.0f);
		k_noise_shutdown();
		object->terrainComponent = std::shared_ptr<TerrainComponent>(new TerrainComponent(landscape, LUMINARY_SIZE, LUMINARY_SIZE, glm::dvec3(1.0, 1.0, 1.0)));
		return object;
	}
	break;
	default: {
		return nullptr;
	}
	break;
	}
	return nullptr;
}
#pragma once

/**
* @brief This entity is esponsible for objects construction
*
*/

#include <application/common/utils/Singleton.h>

#include <memory>

namespace cold {
	class Object;
	class GameObjectManager : public uti::Singleton<GameObjectManager> {
	public:
		/**
		* @brief Provides a handle to a new object of given blueprint id to a callig side
		*
		* @return Unique handle to constructed object
		*/
		std::unique_ptr<Object> createObject(int objectID);

		friend class uti::Singleton<GameObjectManager>;
	};
}
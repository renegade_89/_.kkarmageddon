#include "BoundingBoxComponent.h"

#include <sstream>

cold::BoundingBoxComponent::BoundingBoxComponent(double width, double height, double length): dimensions(width, height, length) {
	// any initialization stuff here
}

const glm::dvec3& cold::BoundingBoxComponent::getDimensions() const {
	// getter
	return dimensions;
}

void cold::BoundingBoxComponent::setDimensions(glm::dvec3& dimensions) {
	// setter
	this->dimensions = dimensions;
}

#pragma once

#include <ogl/glm/glm.hpp>

/**
* @brief If an object has some bounding box parameters, it must be provided with such a component
*
*/

namespace cold {
	class FileManager;
	class BoundingBoxComponent {
	private:
		glm::dvec3 dimensions; /**< glm vector of 3 double values to hold width, height, and depth of our object */

	public:
		/**
		* @brief Initializes a bounding box component
		*
		* @param width Double value of object's width parameter
		* @param height Double value of object's height parameter
		* @param length Double value of object's depth (or length?) parameter
		*/
		BoundingBoxComponent(double width, double height, double length);

		/**
		* @brief Retrieves bounding box dimensions and passes info to caller side
		*
		* @return Reference to constant glm vector of 3 double values to hold width, height, and depth of our object
		*/
		const glm::dvec3& getDimensions() const;

		/**
		* @brief Sets the boudning box dimensions
		*
		* @parameter dimensions Reference to glm vector of 3 double values to hold width, height, and depth of our object
		*/
		void setDimensions(glm::dvec3& dimensions);

		friend class FileManager; /**< Grant direct access to save\load subsystem for speed */
	};
}
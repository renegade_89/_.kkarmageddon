#pragma once

/**
* @brief Objects that represent terrain have this one
*
*/

#include <vector>
#include <cstdint>

#include <ogl/glm/gtc/matrix_transform.hpp>

namespace cold {
	class FileManager;
	class TerrainComponent {
	public:
		enum MaskType : int8_t { MASK_TYPE_NOTHING, MASK_TYPE_ROAD, MASK_TYPE_NODE, NUM_MASK_TYPES }; /**< Currently three types of map areas: plain land, piece of road or a road node */

	private:
		int width; /**< Integer value representing width of terrain map */
		int height; /**< Integer value representing height of terrain map */
		std::vector<float> map; /* 2D map of heights */
		std::vector<MaskType> mask; /* 2D map of 'masks', but it's actually just like: terrain type at this coords please */
		glm::dvec3 scale; /* GLM vector of scale ratios */

	public:
		/**
		* @brief Constrcuts a terrain component and initializes it with provided heights and masks
		*
		* @param heights Collection of floating point numbers representing height map 
		* @param width Integer value of map width
		* @param height Integer value of map height
		* @param roads Collection of integer masks so that we know what type of area is at each place on map
		*/
		TerrainComponent(const std::vector<float> &heights, int width, int height, glm::dvec3 scale, const std::vector<int8_t> &roads);

		/**
		* @brief Constrcuts a terrain component and initializes it with provided heights
		*
		* @param heights Collection of floating point numbers representing height map
		* @param width Integer value of map width
		* @param height Integer value of map height
		*/
		TerrainComponent(const std::vector<float> &heights, int width, int height, glm::dvec3 scale);

		/**
		* @brief Retrives map width for calling side
		*
		* @return Integer value representing the width of the map 
		*/
		int getWidth() const;

		/**
		* @brief Retrives map height for calling side
		*
		* @return Integer value representing the height of the map
		*/
		int getHeight() const;

		/**
		* @brief Retrives height at given point
		*
		* @param row Integer value holding vertical coordinate of the point
		* @param column Integer value holding horizontal coordinate of the point
		* @return Floating point value of the height
		*/
		float getHeightAt(int row, int column) const;

		/**
		* @brief Retrives ground type at given point
		*
		* @param row Integer value holding vertical coordinate of the point
		* @param column Integer value holding horizontal coordinate of the point
		* @return Enumerated type of the soil at that very place, buddy
		*/
		MaskType getMaskAt(int row, int column) const;
		
		/**
		* @brief Retrives reference to unmodifiable underliying data
		*
		* @return Reference to constant array of heights
		*/
		const std::vector<float>& getRawMap() const;

		/**
		* @brief Getter for a map scale factor
		*
		* @return GLM vector of 3 double values of scale factors, one per dimension
		*/
		glm::dvec3 getScale() const;

		/**
		* @brief Sets the scaling for each axis
		*
		* @param Reference to a glm vector of 3 double values of scaling factors
		*/
		void setScale(glm::dvec3& scale);

		friend class FileManager; /**< Grant direct access to save\load subsystem for speed */
	};
}
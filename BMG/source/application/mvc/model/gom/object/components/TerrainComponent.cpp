#include "TerrainComponent.h"

#include <algorithm>
#include <sstream>
#include <cassert>

cold::TerrainComponent::TerrainComponent(const std::vector<float> &heights, int width, int height, glm::dvec3 scale, const std::vector<int8_t> &roads) : width(width), height(height), map(width * height), mask(width * height), scale(scale) {
	// jesus christ, we need more asserts!
	assert(("Height map cannot be empty", heights.size() > 0));
	assert(("Height map dimensions corrupted", heights.size() == width * height));
	assert(("Height map dimensions are invalid", width > 0 && height > 0));
	assert(("Map mask should not be empty, something went wrong...", roads.size() > 0));
	assert(("Map mask dimensions corrupted", roads.size() == width * height));
	map.assign(heights.cbegin(), heights.cend());
	std::transform(mask.begin(), mask.end(), roads.begin(), mask.begin(), [](MaskType m, int8_t value) -> MaskType { return (MaskType) value; });
}

cold::TerrainComponent::TerrainComponent(const std::vector<float> &heights, int width, int height, glm::dvec3 scale) : width(width), height(height), map(width * height), mask(width * height), scale(scale) {
	assert(("Height map cannot be empty", heights.size() > 0));
	assert(("Height map dimensions corrupted", heights.size() == width * height));
	assert(("Height map dimensions are invalid", width > 0 && height > 0));
	map.assign(heights.cbegin(), heights.cend());
}

int cold::TerrainComponent::getWidth() const {
	return width;
}

int cold::TerrainComponent::getHeight() const {
	return height;
}

float cold::TerrainComponent::getHeightAt(int row, int column) const {
	return map[row * width + column];
}

cold::TerrainComponent::MaskType cold::TerrainComponent::getMaskAt(int row, int column) const {
	return mask[row * width + column];
}

const std::vector<float>& cold::TerrainComponent::getRawMap() const {
	return map;
}

glm::dvec3 cold::TerrainComponent::getScale() const {
	return scale;
}

void cold::TerrainComponent::setScale(glm::dvec3& scale) {
	this->scale = scale;
}

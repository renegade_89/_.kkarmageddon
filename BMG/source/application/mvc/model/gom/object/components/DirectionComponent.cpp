#include "DirectionComponent.h"

#include <sstream>

cold::DirectionComponent::DirectionComponent(double x, double y, double z, double upX, double upY, double upZ) : at(x, y, z), up(upX, upY, upZ) {
	// any initialization stuff here
}

void cold::DirectionComponent::setAt(glm::dvec3& at) {
	// setter
	this->at = at;
}

void cold::DirectionComponent::setUp(glm::dvec3& up) {
	// another setter
	this->up = up;
}

const glm::dvec3 cold::DirectionComponent::getSide() const {
	// compute side vector as cross product
	return glm::cross(at, up);
}

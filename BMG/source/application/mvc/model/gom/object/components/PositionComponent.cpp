#include "PositionComponent.h"
#include <application/managers/ScriptManager.h>

cold::PositionComponent::PositionComponent(double x, double y, double z) : position(x, y, z) {
	// add some fake previous positions
	history.assign(10, position);
}

void cold::PositionComponent::setPosition(glm::dvec3& position) {
	// set new position and save the previous one
	history.push_front(this->position);
	if (history.size() > 10)
		history.pop_back();
	this->position = position;
}

bool cold::PositionComponent::stepBack() {
	// try to retrieve previous position
	if (history.size() > 0) {
		this->position = history.front();
		history.pop_front();
		return true;
	}
	return false;
}

void cold::PositionComponent::bind() {
	// export to lua via luabridge
	ScriptManager::getInstance().getNamespace()
		.beginNamespace("cold")
		.beginClass <PositionComponent>("position_component_t")
		.addFunction("step_back", &PositionComponent::stepBackLUA)
		.endClass()
		.endNamespace();
}

bool cold::PositionComponent::stepBackLUA() {
	// just a wrapper
	return stepBack();
}
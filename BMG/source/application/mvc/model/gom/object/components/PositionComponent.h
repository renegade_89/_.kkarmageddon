#pragma once

/**
* @brief Objects that may be positioned have this one
*
*/

#include <ogl/glm/glm.hpp>

#include <list>

namespace cold {
	class FileManager;
	class PositionComponent {
	private:
		glm::dvec3 position; /**< GLM vector for coords representation */
		std::list<glm::dvec3> history; /**< ... */

	public:
		/**
		* @brief Constructs a position component and initializes it with certain position
		*
		* @param x Floating point number holding x coordinate value
		* @param y Floating point number holding y coordinate value
		* @param z Floating point number holding z coordinate value
		*/
		PositionComponent(double x = 0.0f, double y = 0.0f, double z = 0.0f);

		/**
		* @brief Provides the actual coordinates to the calling side
		*
		* @return Math vector holding the coords 
		*/
		const glm::dvec3& getPosition() const { return position; }

		/**
		* @brief Updates the inner representation of position component
		*
		* @param Math vector holding the coords
		*/
		void setPosition(glm::dvec3& position);

		/**
		* @brief Rolls back object to his previous position
		*
		* @return Boolean value indicating if operation has been accomplished successfully
		*/
		bool stepBack();

		/**
		* @brief Exports to lua
		*
		*/
		static void bind();

	private:
		/**
		* @brief Wrapper for stepBack()
		*        We'll add wrappers for every exported function for integrity
		*		 The reason is that luabridge does not support smart pointers
		*        We could wrap only functions with smart pointers, but for now we'll wrap everything
		*/
		bool stepBackLUA();

		friend class FileManager; /**< Grant direct access to save\load subsystem for speed */
	};
}
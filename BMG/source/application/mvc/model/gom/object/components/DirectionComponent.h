#pragma once

/**
* @brief Objects that may be directed have this one
*
*/

#include <ogl/glm/glm.hpp>

namespace cold {
	class FileManager;
	class DirectionComponent {
	private:
		glm::dvec3 at; /**< GLM vector for look direction */
		glm::dvec3 up; /**< GLM vector for upwards irection */

	public:
		/**
		* @brief Constructs a direction component 
		*
		* @param x Floating point number holding x coordinate value to look at from origin
		* @param y Floating point number holding y coordinate value to look at from origin
		* @param z Floating point number holding z coordinate value to look at from origin
		* @param upX Floating point number holding x component of upwards vector
		* @param upY Floating point number holding y component of upwards vector
		* @param upZ Floating point number holding z component of upwards vector
		*/
		DirectionComponent(double x = 0.0f, double y = 0.0, double z = 0.0, double upX = 0.0, double upY = 1.0, double upZ = 0.0);
		
		/**
		* @brief Retrieves direction to look at
		*
		* @return Math vector holding the vector components
		*/
		const glm::dvec3& getAt() const { return at; }

		/**
		* @brief Sets direction to look at
		*
		* @param at Math vector holding the vector components
		*/
		void setAt(glm::dvec3& at);

		/**
		* @brief Retrieves upwards direction 
		*
		* @return Math vector holding the vector components
		*/
		const glm::dvec3& getUp() const { return up; }

		/**
		* @brief Sets upwards direction
		*
		* @param up Math vector holding the vector components
		*/
		void setUp(glm::dvec3& up);

		/**
		* @brief Computes the cross product to obtain side vector
		*
		* @return u Math vector of side direction 
		*/
		const glm::dvec3 getSide() const;

		friend class FileManager; /**< Grant direct access to save\load subsystem for speed */
	};
}
#pragma once

/**
* @brief Represents and object constructed of different components
*		 Combination of certain components defines object's behavior.
*		 Since this is a blueprint, we can have a couple of objects with the same id in our world !
*		 UPD: guids added!
*
*/

#include <application/common/utils/GUID.h>

#include <memory>

namespace cold {
	class TerrainComponent;
	class PositionComponent;
	class DirectionComponent;
	class BoundingBoxComponent;
	class GameObjectManager;
	class FileManager;
	class Object {
		int id; /** Integer value of object blueprint id */
		uti::GUID guid; /**< Objects with the same id will still be identified uniquely by guid */
		std::shared_ptr<TerrainComponent> terrainComponent; /**< Objects that represent terrain have this one */
		std::shared_ptr<PositionComponent> positionComponent; /**< Objects that may be positioned have this one */
		std::shared_ptr<DirectionComponent> directionComponent; /**< Objects that may be directed have this one */
		std::shared_ptr<BoundingBoxComponent> boundingBoxComponent; /**< Objects with volume have this one */

	public:
		enum  ObjectID : int {OBJECT_ID_INVALID, OBJECT_ID_TEST_TERRAIN_0, OBJECT_ID_TEST_PLAYER_0, OBJECT_ID_HORIZON, OBJECT_ID_STAR, OBJECT_ID_LUMINARY, NUM_OBJECT_IDS};  /**< All objects we have so far... temporary enum. */
	
	public:
		/**
		* @brief Retrieves object's blueprint id
		*
		* @return Integer value of id
		*/
		int getID() const noexcept;

		/**
		* @brief Retrieves object's instance id
		*
		* @return GUID struct of object
		*/
		uti::GUID getGUID() const noexcept;

		/**
		* @brief Retrieves object's terrain component for someone who is not supposed to modify it (view)
		*
		* @return Shared pointer to terrain component
		*/
		std::shared_ptr<const TerrainComponent> getTerrainComponent() const noexcept;

		/**
		* @brief Retrieves object's terrain component for someone who can modify it (controller)
		*
		* @return Shared pointer to terrain component
		*/
		std::shared_ptr<TerrainComponent> getTerrainComponent() noexcept;

		/**
		* @brief Retrieves object's position component for someone who is not supposed to modify it (view)
		*
		* @return Shared pointer to position component
		*/
		std::shared_ptr<const PositionComponent> getPositionComponent() const noexcept;

		/**
		* @brief Retrieves object's position component for someone who can modify it (controller)
		*
		* @return Shared pointer to position component
		*/
		std::shared_ptr<PositionComponent> getPositionComponent() noexcept;

		/**
		* @brief Retrieves object's direction component for someone who is not supposed to modify it (view)
		*
		* @return Shared pointer to direction component
		*/
		std::shared_ptr<const DirectionComponent> getDirectionComponent() const noexcept;

		/**
		* @brief Retrieves object's direction component for someone who can modify it (controller)
		*
		* @return Shared pointer to direction component
		*/
		std::shared_ptr<DirectionComponent> getDirectionComponent() noexcept;

		/**
		* @brief Retrieves object's bounding box component
		*
		* @return Shared pointer to unmodifiable bounding box component
		*/
		std::shared_ptr<const BoundingBoxComponent> getBoundingBoxComponent() const noexcept;

		/**
		* @brief Retrieves object's bounding box component
		*
		* @return Shared pointer to bounding box component
		*/
		std::shared_ptr<BoundingBoxComponent> getBoundingBoxComponent() noexcept;

		/**
		* @brief Exports some member functions to lua
		*
		*/
		static void bind();

	private:
		/**
		* @brief Wrapper for getPositionComponent()
		*        We'll add wrappers for every exported function for integrity
		*		 The reason is that luabridge does not support smart pointers
		*        We could wrap only functions with smart pointers, but for now we'll wrap everything
		*/
		PositionComponent* getPositionComponentLUA();

		friend class FileManager; // for faster saving
		friend class GameObjectManager; // I'm starting to think that we have too many friend classes in this project
	};
}
#include <application/mvc/model/gom/object/Object.h>
#include <application/managers/ScriptManager.h>

#include <application/mvc/model/gom/object/components/TerrainComponent.h>
#include <application/mvc/model/gom/object/components/PositionComponent.h>
#include <application/mvc/model/gom/object/components/DirectionComponent.h>
#include <application/mvc/model/gom/object/components/BoundingBoxComponent.h>

int cold::Object::getID() const noexcept {
	return id;
}

uti::GUID cold::Object::getGUID() const noexcept {
	return guid;
}

std::shared_ptr<const cold::TerrainComponent> cold::Object::getTerrainComponent() const noexcept {
	return terrainComponent;
}

std::shared_ptr<cold::TerrainComponent> cold::Object::getTerrainComponent() noexcept {
	return terrainComponent;
}

std::shared_ptr<const cold::PositionComponent>  cold::Object::getPositionComponent() const noexcept {
	return positionComponent;
}

std::shared_ptr<cold::PositionComponent>  cold::Object::getPositionComponent() noexcept {
	return positionComponent;
}

std::shared_ptr<const cold::DirectionComponent>  cold::Object::getDirectionComponent() const noexcept {
	return directionComponent;
}

std::shared_ptr<cold::DirectionComponent>  cold::Object::getDirectionComponent() noexcept {
	return directionComponent;
}

std::shared_ptr<const cold::BoundingBoxComponent> cold::Object::getBoundingBoxComponent() const noexcept {
	return boundingBoxComponent;
}

std::shared_ptr<cold::BoundingBoxComponent> cold::Object::getBoundingBoxComponent() noexcept {
	return boundingBoxComponent;
}

void cold::Object::bind() {
	ScriptManager::getInstance().getNamespace()
		.beginNamespace("cold")
		.beginClass <Object>("object_t")
		.addFunction("get_position_component", &Object::getPositionComponentLUA)
		.endClass()
		.endNamespace();
}

cold::PositionComponent* cold::Object::getPositionComponentLUA() {
	return getPositionComponent().get();
}
#pragma once

#include <application/mvc/model/base/Model.h>

#include <memory>

/**
* @brief Game model
*/
namespace cold {
	class Location;
	class InputModel;
	class FileManager;
	class GameModel : public Model {
		std::shared_ptr<Location> location; /**< Shared handle to current location */
		std::shared_ptr<InputModel> inputModel; /**< Shared handle to input state */

	public:
		/**
		* @brief Performs all needed initialization stuff
		*
		*/
		GameModel(const std::string& fileName);

		/**
		* @brief Retrieves current location for someone who cannot modify it (view?)
		*
		* @return Shared handle to current location
		*/
		std::shared_ptr<const Location> getCurrentLocation() const noexcept;

		/**
		* @brief Provides handle to input state
		*
		* @return Shared handle to input state
		*/
		std::shared_ptr<const InputModel> getInputModel() const noexcept { return inputModel; };

		/**
		* @brief Retrieves current location for someone who can modify it (controller?)
		*
		* @return Shared handle to current location
		*/
		std::shared_ptr<Location> getCurrentLocation() noexcept;

		/**
		* @brief Updates the model smoothly
		*
		* @param delta mount of milliseconds that passed after the last call
		*/
		virtual void update(int delta) noexcept override;

		/**
		* @brief Performs all needed cleanup
		*
		*/
		virtual ~GameModel();

		friend class FileManager;
	};
}
#pragma once

/**
* @brief Basic interface for a view.
*        Lists up the messages that can be sent to certain view object.
*/

namespace cold {
	class View {
	public:
		/**
		* @brief Presents a view of a model in its current state
		*
		*/
		virtual void paint() noexcept = 0;

		/**
		* @brief Handles resize
		*
		*/
		virtual void resize(int width, int height) = 0;

		/**
		* @brief "The will to win, the desire to succeed, the urge to reach you full potential... these are the keys that unlock the door to personal excellence" (c) Confucius
		*
		*/
		virtual ~View() = default;
	};
}
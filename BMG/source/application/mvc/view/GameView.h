#pragma once

#include <application/mvc/view/base/View.h>

/**
* @brief Simple terrain generation from height map, and display (for debug purposes)
*
*/

#include <memory>

namespace cold {
	class GameModel;
	class GameView : public View {
	private:
		int terrainID; /**< Stores the identifier for terrain and pass it to engine to invoke draw when needed */
		std::shared_ptr<const GameModel> model; /**< Shared handle to non modifiable model (view shouldn't modify anything) */
	
	public:
		/**
		* @brief Constructs a test view and provides handle to a model for presentation
		*
		* @param model Shared handle to non modifiable model object
		*/
		GameView(std::shared_ptr<const GameModel> model);

		/**
		* @brief Presents a view of a model in its current state
		*
		*/
		virtual void paint() noexcept override;

		/**
		* @brief Handles resize
		*
		* @param width Integer value holding width of the view
		* @param height Integer value holding height of the view
		*/
		virtual void resize(int width, int height) override;

		/**
		* @brief "Our greatest glory is not in never falling, but in rising every time we fall"(c) Confucius
		*
		*/
		virtual ~GameView();
	};
}
#include <application/mvc/view/GameView.h>
#include <application/mvc/model/GameModel.h>
#include <application/mvc/model/location/Location.h>
#include <application/mvc/model/gom/object/Object.h>
#include <application/mvc/model/gom/object/components/TerrainComponent.h>
#include <application/mvc/model/gom/object/components/PositionComponent.h>
#include <application/mvc/model/gom/object/components/DirectionComponent.h>
#include <application/mvc/model/gom/object/components/BoundingBoxComponent.h>
#include <engine/min_tech/MinTechEngine.h>
#include <application/common/utils/Singleton.h>
#include <application/common/Common.h>

#include <iostream>
#include <cassert>

cold::GameView::GameView(std::shared_ptr<const GameModel> model) : model(model) {
	Engine::getInstance().init(WINDOW_WIDTH, WINDOW_HEIGHT, FOV, 0.0001 * cold::SI::KM, 5.00 * SI::KM);

	assert(("Cannot present view for an empty model", model != nullptr));
	assert(("Cannot retrieve objects from nil location", model->getCurrentLocation() != nullptr));
	assert(("Test terrain object has not been created", model->getCurrentLocation()->getFirstOf(Object::OBJECT_ID_TEST_TERRAIN_0) != nullptr));

	auto map = model->getCurrentLocation()->getFirstOf(Object::OBJECT_ID_TEST_TERRAIN_0)->getTerrainComponent();
	assert(("Map is invalid", map != nullptr));
	terrainID = Engine::getInstance().createTerrain(map->getRawMap(), glm::ivec2(map->getWidth(), map->getHeight()), map->getScale());

	auto stars = model->getCurrentLocation()->getRangeOf(Object::OBJECT_ID_STAR);
	std::vector<glm::vec3> starsDirections;
	for (auto star = stars.first; star != stars.second; std::advance(star, 1)) {
		auto temp = glm::vec3(star->second->getPositionComponent()->getPosition());
		starsDirections.push_back(temp);
#ifdef _DEBUG 
		if (starsDirections.size() > 100)
			break;
#endif
	}
	auto luminary = model->getCurrentLocation()->getFirstOf(Object::OBJECT_ID_LUMINARY);
	auto luminaryTerrain = luminary->getTerrainComponent();
	Engine::getInstance().createSky(starsDirections, luminaryTerrain->getRawMap(), glm::ivec2(luminaryTerrain->getWidth(), luminaryTerrain->getHeight()), luminary->getPositionComponent()->getPosition());

	auto horizon = model->getCurrentLocation()->getFirstOf(Object::OBJECT_ID_HORIZON)->getTerrainComponent();
	assert(("Map is invalid", horizon != nullptr));
	Engine::getInstance().createBackground(horizon->getRawMap(), glm::ivec2(horizon->getWidth(), HORIZON_HEIGHT));

	auto player = model->getCurrentLocation()->getFirstOf(Object::OBJECT_ID_TEST_PLAYER_0);
	assert(("Player object missing direction component", player->getDirectionComponent() != nullptr));
	auto direction = player->getDirectionComponent();
	assert(("Player object missing position component", player->getPositionComponent() != nullptr));
	decltype(auto) position = player->getPositionComponent()->getPosition();
	decltype(auto) box = player->getBoundingBoxComponent()->getDimensions();
	Engine::getInstance().setCamera(position, direction->getAt(), direction->getUp());
}

void cold::GameView::paint() noexcept {
	// clear
	Engine::getInstance().clear();
	// camera
	assert(("Cannot access current location", model->getCurrentLocation() != nullptr));
	auto player = model->getCurrentLocation()->getFirstOf(Object::OBJECT_ID_TEST_PLAYER_0);
	assert(("Player object missing", player != nullptr));
	assert(("Player object missing direction component", player->getDirectionComponent() != nullptr));
	auto direction = player->getDirectionComponent();
	assert(("Player object missing position component", player->getPositionComponent() != nullptr));
	decltype(auto) position = player->getPositionComponent()->getPosition();
	Engine::getInstance().setCamera(position, direction->getAt(), direction->getUp());
	// objects
	
	Engine::getInstance().drawObjects();
	
	// swap buffers
	Engine::getInstance().flush();
}

void cold::GameView::resize(int width, int height) {
	Engine::getInstance().resize(width, height);
}

cold::GameView::~GameView() {
	Engine::getInstance().shutdown();
}

#pragma once

/**
* @brief Basic interface for application objects.
*        All we need is to be able to run them.
*/

#include <application/base/Application.h>

#include <memory>

namespace cold {
	class Controller;
	class AbstractMVCApplication : public Application {
	protected:
		std::unique_ptr<Controller> controller; /**< Smart pointer to application's root controller. */

	public:
		/**
		* @brief "The superior man is modest in his speech, but exceeds in his actions"(c) Confucius
		*
		*/
		virtual ~AbstractMVCApplication() = default;

	};
}
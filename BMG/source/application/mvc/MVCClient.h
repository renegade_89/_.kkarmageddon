#pragma once

/**
* @brief MVC-based client.
*        Events handling is delegated to designated entities.
*/

#include "base/AbstractMVCApplication.h"
#include <application/glut/client/Client.h>

namespace cold {
	class MVCClient : public AbstractMVCApplication, public Client {
	public:
		/**
		* @brief Constructor for all needed initialization stuff
		*
		*/
		MVCClient(std::unique_ptr<Controller> controller);

		/**
		* @brief Runs an application object
		*
		* @return Integer value indicating if operation was successfull
		*/
		virtual int run() noexcept override;

		/**
		* @brief Invoked on resizing
		*
		* @param width Integer value indicating new window width
		* @param height Integer value indicating new window height
		*/
		virtual void onResize(int width, int height) override;

		/**
		* @brief Invoked on rendering
		*
		*/
		virtual void onDraw() noexcept override;

		/**
		* @brief Invoked on timer event
		*
		* @param id Integer value indicating timer id
		*/
		virtual void onTick(int id) noexcept override;

		/**
		* @brief "Choose a job you love, and you will never have to work a day in your life" (c) Confucius 
		*
		*/
		virtual ~MVCClient();
	};
}
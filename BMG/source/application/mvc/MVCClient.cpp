#include "MVCClient.h"
#include <application/glut/server/Server.h>
#include <application/mvc/controller/base/Controller.h>
#include <application/mvc/view/base/View.h>

#include <cassert>

cold::MVCClient::MVCClient(std::unique_ptr<Controller> controller) {
	this->controller = std::move(controller);
}

int cold::MVCClient::run() noexcept {
	// TODO: todo
	return EXIT_SUCCESS;
}

void cold::MVCClient::onResize(int width, int height) {
	assert(("Controller not set", controller != nullptr));
	assert(("View not set", controller->getView() != nullptr));
	controller->getView()->resize(width, height);
}

void cold::MVCClient::onDraw() noexcept {
	assert(("Controller not set", controller != nullptr));
	assert(("View not set", controller->getView() != nullptr));
	controller->getView()->paint();
}

void cold::MVCClient::onTick(int id) noexcept {
	assert(("Controller not set", controller != nullptr));
	controller->doLogic();
}

cold::MVCClient::~MVCClient() {
}
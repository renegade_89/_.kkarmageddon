#pragma once

/**
* @brief All needed includes for the main
*
*/

#include "stdafx.h"

#include <application/base/Application.h>

#include <exception>
#include <iostream>
#include <memory>

std::unique_ptr<cold::Application> createApplication();

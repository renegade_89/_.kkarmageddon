#include "cold.h"

#include <application/GameApplication.h>

#include <cassert>

std::unique_ptr<cold::Application> createApplication() {
	try {
		return std::make_unique<cold::GameApplication>();
	} catch (std::exception& exception) {
		std::cerr << exception.what() << std::endl;
		exit(EXIT_FAILURE);
	}
	return nullptr;
}
#ifndef COLD_PRIORITY_QUEUE_8168EDB7_5E5D_4475_B65D_EEB590A12B2A
#define COLD_PRIORITY_QUEUE_8168EDB7_5E5D_4475_B65D_EEB590A12B2A

#include "list/k_alg_list.h"

typedef struct cold_priority_queue_tag
{
	cold_node_ptr list; /**< Pointer to inner representation - linked list */
} cold_priority_queue, *cold_priority_queue_ptr;

/**
* @brief Initialize priority queue
*
* @param queue Pointer to allocated queue
*/
void k_alg_priority_queue_init(cold_priority_queue_ptr queue);

/**
* @brief Place an item to priority queue
*
* @param queue Pointer to allocated queue
* @param priority Assigned priority for the new node
* @param data Pointer to some custom user data
*/
void k_alg_priority_queue_push(cold_priority_queue_ptr queue, double priority, const void *data);

/**
* @brief Check if priority queue is empty
*
* @param queue Pointer to allocated queue
* @return Integer value holding 1 id true, and 0 otherwise
*/
int k_alg_priority_queue_is_empty(cold_priority_queue_ptr queue);

/**
* @brief Pop the highest priority user data
*
* @param queue Pointer to allocated queue
* @return Pointer to some custom user data
*/
const void* k_alg_priority_queue_pop(cold_priority_queue_ptr queue);

/**
* @brief Perform all needed cleanup
*
* @param queue Pointer to allocated queue
*/
void k_alg_priority_queue_shutdown(cold_priority_queue_ptr queue);

#endif
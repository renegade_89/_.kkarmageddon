#ifndef COLD_LIST_646938D6_69B8_48E8_A4D7_7E65653B6966
#define COLD_LIST_646938D6_69B8_48E8_A4D7_7E65653B6966

typedef struct cold_node_tag
{
	double id; /**< Node id */
	const void* data; /**< Pointer to user data */
	struct cold_node_tag *next; /**< Pointer to next node or nil */
} cold_node, *cold_node_ptr;

/**
* @brief Initialize list
*
* @param node Pointer to allocated node
* @param id Node identifier
* @param data Pointer to user data
*/
void k_alg_list_init(cold_node_ptr node, double id, const void * data);

/**
* @brief Initialize list
*
* @param node Pointer to allocated node
* @param id Node identifier
* @param data Pointer to user data
*/
void k_alg_list_insert_head(cold_node_ptr list, cold_node_ptr node);

/**
* @brief Attach node to the list back
*
* @param list Pointer to the list first node
* @param node Pointer to allocated node
*/
void k_alg_list_insert_tail(cold_node_ptr list, cold_node_ptr node);

/**
* @brief Provide pointer to list head to calling side
*
* @param list Pointer to the list first node
* @return Pointer to first node
*/
cold_node_ptr k_alg_list_head(cold_node_ptr list);

/**
* @brief Provide pointer to list tail to calling side
*
* @param list Pointer to the list first node
* @return Pointer to last node
*/
cold_node_ptr k_alg_list_tail(cold_node_ptr list);

/**
* @brief Provide pointer to list head to calling side and pop it
*
* @param list Pointer to the list first node
* @return Pointer to popped node
*/
cold_node_ptr k_alg_list_pop_head(cold_node_ptr list);

/**
* @brief Provide pointer to list tail to calling side and pop it
*
* @param list Pointer to the list first node
* @return Pointer to popped node
*/
cold_node_ptr k_alg_list_pop_tail(cold_node_ptr list);

#endif
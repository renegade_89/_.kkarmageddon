#include "k_alg_list.h"

#include <stdlib.h>
#include <assert.h>

void k_alg_list_init(cold_node_ptr node, double id, const void *data)
{
	assert(("Trying to initialize nothing", node != NULL));
	node->id = id;
	node->data = data;
}

void k_alg_list_insert_head(cold_node_ptr list, cold_node_ptr node)
{
	assert(("Cannot insert to nil list", list != NULL));
	assert(("Cannot insert nil node", node != NULL));
	node->next = list->next;
	list->next = node;

}

void k_alg_list_insert_tail(cold_node_ptr list, cold_node_ptr node)
{
	assert(("Cannot insert to nil list", list != NULL));
	assert(("Cannot insert nil node", node != NULL));
	while (list->next != NULL)
		list = list->next;
	node->next = NULL;
	list->next = node;
}

cold_node_ptr k_alg_list_head(cold_node_ptr list)
{
	if (list == NULL || list->next == NULL)
		return NULL;
	return list->next;
}

cold_node_ptr k_alg_list_tail(cold_node_ptr list)
{
	if (list == NULL || list->next == NULL)
		return NULL;
	while (list->next != NULL)
		list = list->next;
	return list;
}

cold_node_ptr k_alg_list_pop_head(cold_node_ptr list)
{
	if (list == NULL || list->next == NULL)
		return NULL;
	cold_node_ptr node = list->next;
	list->next = node->next;
	node->next = NULL;
	return node;
}

cold_node_ptr k_alg_list_pop_tail(cold_node_ptr list)
{
	if (list == NULL || list->next == NULL)
		return NULL;
	if (list->next->next == NULL)
	{
		cold_node_ptr node = list->next;
		list->next = NULL;
		return node;
	}
	while (list->next->next != NULL)
		list = list->next;
	cold_node_ptr node = list->next;
	list->next = NULL;
	return node;
}
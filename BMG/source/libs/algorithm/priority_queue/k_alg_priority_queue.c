#include "k_alg_priority_queue.h"
#include "list/k_alg_list.h"

#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <assert.h>

void k_alg_priority_queue_init(cold_priority_queue_ptr queue)
{
	assert(("Trying to initialize nil queue", queue != NULL));
	cold_node_ptr node = (cold_node_ptr)malloc(sizeof(cold_node));
	memset(node, 0, sizeof(cold_node));
	queue->list = node;
	k_alg_list_init(queue->list, 0, NULL);
}

void k_alg_priority_queue_push(cold_priority_queue_ptr queue, double priority, const void *data)
{
	assert(("Cannot push to nil queue", queue != NULL));
	assert(("User data is not likely to be nil", data != NULL));
	cold_node_ptr node = (cold_node_ptr) malloc(sizeof(cold_node));
	memset(node, 0, sizeof(cold_node));
	k_alg_list_init(node, priority, data);
	if (queue->list == NULL )
		return;
	if (queue->list->next == NULL)
	{
		k_alg_list_insert_head(queue->list, node);
	}
	else
	{
		cold_node_ptr temp = queue->list->next;
		if (priority >= temp->id)
			k_alg_list_insert_head(queue->list, node);
		else
		{
			if (temp->next == NULL)
				k_alg_list_insert_tail(queue->list, node);
			else
			{
				while (temp->next->next != NULL && priority <= temp->next->next->id)
					temp = temp->next;
				if (temp->next->next == NULL)
				{
					if (priority >= temp->next->id)
					{
						node->next = temp->next;
						temp->next = node;
					}
					else
					{
						k_alg_list_insert_tail(queue->list, node);
					}
				}
				else
				{
					node->next = temp->next->next;
					temp->next->next = node;
				}
			}
		}
	}
}

int k_alg_priority_queue_is_empty(cold_priority_queue_ptr queue)
{
	assert(("Nil queue", queue != NULL));
	if (k_alg_list_head(queue->list) == NULL)
		return 1;
	return k_alg_list_head(queue->list)->id == 0;
}

const void* k_alg_priority_queue_pop(cold_priority_queue_ptr queue)
{
	assert(("Nil queue", queue != NULL));
	cold_node_ptr node = k_alg_list_pop_head(queue->list);
	const void *result = node->data;
	free(node);
	node = NULL;
	return result;
}

void k_alg_priority_queue_shutdown(cold_priority_queue_ptr queue)
{
	assert(("Nil queue", queue != NULL));
	while (k_alg_list_head(queue->list)) 
	{
		cold_node_ptr node = k_alg_list_pop_head(queue->list);
		const void *result = node->data;
		free(node);
		node = NULL;
	}
	if (queue->list)
	{
		free(queue->list);
		queue->list = NULL;
	}
}
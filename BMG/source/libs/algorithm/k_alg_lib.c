#include "priority_queue/k_alg_priority_queue.h"

#include <math.h>
#include <malloc.h>
#include <stdlib.h>
#include <memory.h>
#include <stdint.h>
#include <assert.h>

void k_alg_init()
{

}

static double k_alg_real_distance(int index_0, int index_1, const float buffer[], int buffer_width, double(*weight_callback)(float a, float b));

typedef struct edge_tag
{
	double weight;
	int index;
} edge, *edge_ptr;

static int k_alg_compare(const void* a, const void* b);

void k_alg_generate_graph(const float buffer[], int width, int height, const int vertices[], int num_vertices, double adjacency[], double (*weight_callback)(float a, float b))
{
	assert(("Height buffer is nil", buffer != NULL));
	assert(("Invalid height buffer width", width > 0));
	assert(("Invalid height buffer height", height > 0));
	assert(("Nodes error", vertices != NULL));
	assert(("Nodes not available", num_vertices > 0));
	assert(("Adjacency table not allocated", adjacency != NULL));
	assert(("Cannot calculate edges weights", weight_callback != NULL));
	edge_ptr edges = (edge_ptr) malloc(num_vertices * (num_vertices) / 2 * sizeof(edge));
	memset(edges, 0, num_vertices * (num_vertices) / 2 * sizeof(edge));
	int edge_counter = 0;
	for (int i = 0; i < num_vertices; ++i) 
	{
		for (int j = 0; j <= i; ++j) 
		{
			if (i != j) 
			{
				float distance = (float) k_alg_real_distance(vertices[i], vertices[j], buffer, width, weight_callback);
				edges[edge_counter].weight = distance;
				edges[edge_counter].index = i * num_vertices + j;
				++edge_counter;
			}
		}
	}
	qsort(edges, edge_counter, sizeof(edge), k_alg_compare);
	int maxTreeID = -1;
	for (int k = 0; k < edge_counter; ++k)
	{
		int i = edges[k].index / num_vertices;
		int j = edges[k].index % num_vertices;
		if (adjacency[i + i * num_vertices] == 0 && adjacency[j + j * num_vertices] == 0) {
			// generate new tree id
			adjacency[i + i * num_vertices] = adjacency[j + j * num_vertices] = (double) maxTreeID--;
			adjacency[i * num_vertices + j] = edges[k].weight;
		}
		else if (adjacency[i + i * num_vertices] == 0) {
			adjacency[i + i * num_vertices] = adjacency[j + j * num_vertices];
			adjacency[i * num_vertices + j] = edges[k].weight;
		}
		else if (adjacency[j + j * num_vertices] == 0) {
			adjacency[j + j * num_vertices] = adjacency[i + i * num_vertices];
			adjacency[i * num_vertices + j] = edges[k].weight;
		}
		else if ((int) adjacency[i + i * num_vertices] != (int) adjacency[j + j * num_vertices]) {
			int oldID = (int) adjacency[i + i * num_vertices];
			for (int k = 0; k < num_vertices; ++k)
				if (adjacency[k + k * num_vertices] == (double) oldID)
					adjacency[k + k * num_vertices] = adjacency[j + j * num_vertices];
			adjacency[i * num_vertices + j] = edges[k].weight;
		}
	}
	free(edges);
	edges = NULL;
}

void k_alg_generate_paths(const float buffer[], int width, int height, const int vertices[], int num_vertices, const double adjacency[], int8_t output[], double(*weight_callback)(float a, float b), double(*heuristic_callback)(double goal_x, double goal_y, double current_x, double current_y))
{
	assert(("Height buffer is nil", buffer != NULL));
	assert(("Invalid height buffer width", width > 0));
	assert(("Invalid height buffer height", height > 0));
	assert(("Nodes error", vertices != NULL));
	assert(("Nodes not available", num_vertices > 0));
	assert(("Adjacency table not allocated", adjacency != NULL));
	assert(("Bad argument", output != NULL));
	assert(("Cannot calculate edges weights", weight_callback != NULL));
	assert(("No aim for path finding", heuristic_callback != NULL));
	int *came_from = (int*) malloc(width * height * sizeof(int));
	double *cost_so_far = (double*) malloc(width * height * sizeof(double));
	char *enqueue_counter = (char*) malloc(width * height * sizeof(char));

	for (int i = 0; i < num_vertices; ++i) 
	{
		for (int j = 0; j <= i; ++j) 
		{
			if (i != j) 
			{
				if (adjacency[i * num_vertices + j] > 0) 
				{
					int start = vertices[i];
					int goal = vertices[j];
					cold_priority_queue_ptr queue = (cold_priority_queue_ptr) malloc(sizeof(cold_priority_queue));
					memset(queue, 0, sizeof(cold_priority_queue));
					k_alg_priority_queue_init(queue);
					k_alg_priority_queue_push(queue, 1.0, (const void*) start);
					memset(came_from, 0, sizeof(int) * width * height);
					memset(cost_so_far, 0, sizeof(double) * width * height);
					memset(enqueue_counter, 0, sizeof(char) * width * height);
					came_from[start] = -1;
					while (!k_alg_priority_queue_is_empty(queue)) {
						int current = (int) k_alg_priority_queue_pop(queue);
						if (current == goal)
							break;
						enum Direction { DIRECTION_LEFT, DIRECTION_DOWN, DIRECTION_RIGHT, DIRECTION_UP, NUM_DIRECTIONS };
						static int directions[NUM_DIRECTIONS];
						if (current % width == 0)
							directions[DIRECTION_LEFT] = 0;
						else
							directions[DIRECTION_LEFT] = -1;
						if (current % width == (width - 1))
							directions[DIRECTION_RIGHT] = 0;
						else
							directions[DIRECTION_RIGHT] = +1;
						if (current / width == 0)
							directions[DIRECTION_UP] = 0;
						else
							directions[DIRECTION_UP] = -width;
						if (current / width == width - 1)
							directions[DIRECTION_DOWN] = 0;
						else 
							directions[DIRECTION_DOWN] = +width;
						for (int k = DIRECTION_LEFT; k < NUM_DIRECTIONS; ++k) {
							if (directions[k] == 0)
								continue;
							int next = current + directions[k];
							double new_cost = cost_so_far[current] + weight_callback(buffer[current], buffer[next]);
							if (cost_so_far[next] == 0.0f || (new_cost < cost_so_far[next])) {
								cost_so_far[next] = new_cost;
								double heuristic = heuristic_callback(goal % width, goal / width, next % width, next / width);
								double priority = -0.5 * heuristic - new_cost;
								k_alg_priority_queue_push(queue, priority, (const void*)next);
								came_from[next] = current;
							}
						}
					}
					k_alg_priority_queue_shutdown(queue);
					free(queue);
					queue = NULL;
					int current_point = goal;
					int initial_goal = goal;
					int num_iterations = 0;
					while (current_point != -1 && current_point != start && num_iterations++ < 10000) {
						if (output[current_point] == 0)
							output[current_point] = (int8_t) 1;
						current_point = came_from[current_point];
					}
					output[start] = (int8_t)2;
					output[initial_goal] = (int8_t)2;
				}
			}
		}
	}
}

void k_alg_shutdown()
{

}

double k_alg_real_distance(int index_0, int index_1, const float buffer[], int buffer_width, double(*weight_callback)(float a, float b)) {
	int dx, dy, dx2, dy2, x_inc, y_inc, error, index;
	double result = 0.0f;
	int x0 = index_0 % buffer_width;
	int y0 = index_0 / buffer_width;
	int x1 = index_1 % buffer_width;
	int y1 = index_1 / buffer_width;
	int offset = x0 + y0 * buffer_width;
	float previous = buffer[offset];
	dx = x1 - x0;
	dy = y1 - y0;
	if (dx >= 0)
	{
		x_inc = 1;
	}
	else
	{
		x_inc = -1;
		dx = -dx;
	}
	if (dy >= 0)
	{
		y_inc = buffer_width;
	}
	else
	{
		y_inc = -buffer_width;
		dy = -dy;

	}
	dx2 = dx << 1;
	dy2 = dy << 1;
	if (dx > dy)
	{
		error = dy2 - dx;
		for (index = 0; index <= dx; index++)
		{
			result += weight_callback(previous, buffer[offset]);
			previous = buffer[offset];
			if (error >= 0)
			{
				error -= dx2;
				offset += y_inc;
			}
			error += dy2;
			offset += x_inc;
		}
	}
	else
	{
		error = dx2 - dy;
		for (index = 0; index <= dy; index++)
		{
			result += weight_callback(previous, buffer[offset]);
			previous = buffer[offset];
			if (error >= 0)
			{
				error -= dy2;
				offset += x_inc;

			}
			error += dx2;
			offset += y_inc;
		}
	}
	return result;
}

int k_alg_compare(const void* a, const void* b)
{
	const edge_ptr edge_0 = (const edge_ptr)(a);
	const edge_ptr edge_1 = (const edge_ptr)(b);
	if (edge_0->weight < edge_1->weight) 
		return -1;
	if (edge_0->weight > edge_1->weight) 
		return 1;
	return 0;
}

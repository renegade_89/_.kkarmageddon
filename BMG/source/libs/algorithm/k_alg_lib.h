#ifndef COLD_ALGORITHMS_LIB_2B0E7985_3692_4A9C_BAD1_E22186BD957F
#define COLD_ALGORITHMS_LIB_2B0E7985_3692_4A9C_BAD1_E22186BD957F

/**
* @brief Functions to generate graph over some height map (using kruskal) and generate curvy paths between the graph vertices (using a*)
*
*/

#include <stdint.h>

/**
* @brief Perform all needed initialization
*
*/
extern "C" void k_alg_init();

/**
* @brief Generate tree over graph using kruskal
*
* @param buffer Height map over which the graph nodes are located
* @param width Integer value of height buffer width
* @param height Integer value of height buffer height (hate!)
* @param vertices Vertices indices (index defines a point on the map!) 
* @param num_vertices Integer value of number of vertices for the graph
* @param adjacency Here the adjacency matrix will be stored for our tree
* @param weight_callback This is a custom callback which computes and provides 'edge weight' value  
* @see https://en.wikipedia.org/wiki/Kruskal%27s_algorithm
*/
extern "C" void k_alg_generate_graph(const float buffer[], int width, int height, const int vertices[], int num_vertices, double adjacency[], double(*weight_callback)(float a, float b));

/**
* @brief Generate curvy paths between the graph nodes
*
* @param buffer Height map over which the graph nodes are located
* @param width Integer value of height buffer width
* @param height Integer value of height buffer height (hate!)
* @param vertices Vertices indices (index defines a point on the map!)
* @param num_vertices Integer value of number of vertices for the graph
* @param adjacency Here the adjacency matrix is stored for our tree
* @param output The mask of paths will be stored here 
* @param weight_callback This is a custom callback which computes and provides 'edge weight' value (aim for shorter paths)
* @param heuristic_callback This is a cutom callback which computes euristic at given point (aim to goal direction)
* @see https://en.wikipedia.org/wiki/A*_search_algorithm
*/
extern "C" void k_alg_generate_paths(const float buffer[], int width, int height, const int vertices[], int num_vertices, const double adjacency[], int8_t output[], double(*weight_callback)(float a, float b), double(*heuristic_callback)(double goal_x, double goal_y, double current_x, double current_y));

/**
* @brief Perform all needed cleanup
*
*/
extern "C" void k_alg_shutdown();

#endif
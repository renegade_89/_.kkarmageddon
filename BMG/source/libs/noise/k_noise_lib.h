#ifndef COLD_NOISE_LIB_BE87E5E5_2AA8_416D_A08C_7C1D2E43CC19
#define COLD_NOISE_LIB_BE87E5E5_2AA8_416D_A08C_7C1D2E43CC19

/**
* @brief Functions to generate surfaces
*
*/

#define k_noise_DEFAULT_SCALE		1.0f
#define k_noise_DEFAULT_NUM_OCTAVES	2

/**
* @brief Perform all needed initialization
*
*/
extern "C" void k_noise_init();

/**
* @brief Generate perlin noise in given buffer
*
* @param buffer Storage for noise data
* @param width Width of noise data buffer
* @param height Height of noise data buffer
* @param scale Simply scales the noise data
* @param num_octaves More octaves means more details and complexity and more fractal likeness
* @see https://en.wikipedia.org/wiki/Perlin_noise
*/
extern "C" void k_noise_generate_noise(float buffer[], int width, int height, float scale, int num_octaves);

/**
* @brief Generate paraboloid projection
*
* @param buffer Storage for height data
* @param width Width of height data buffer
* @param height Height of height data buffer
* @see https://en.wikipedia.org/wiki/Paraboloid
*/
extern "C" void k_noise_generate_paraboloid(float buffer[], int width, int height);

/**
* @brief Buffer data normalization
*
* @param buffer Storage for data
* @param width Width of data buffer
* @param height Height of data buffer
* @param new_min The lower bound for values
* @param new_max The upper bound for values
*/
extern "C" void k_noise_transform(float buffer[], int width, int height, float new_min, float new_max);

/**
* @brief Perform all needed cleanup
*
*/
extern "C" void k_noise_shutdown();

#endif
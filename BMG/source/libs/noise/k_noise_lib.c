#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <assert.h>

#define k_noise_DEFAULT_PERSISTENCE	0.5f

float k_noise_noise(float x, float y, int octaves, float persistence);

int COEF_A = 0, COEF_B = 0, COEF_C = 0;

void k_noise_init() {
	srand((unsigned int)time(NULL));
}

void k_noise_generate_noise(float buffer[], int width, int height, float scale, int num_octaves) 
{
	assert(("Noise buffer not allocated", buffer != NULL));
	assert(("Buffer dimension is invalid", width > 0));
	assert(("Buffer dimension is invalid", height > 0));
	assert(("Scale should be positive", scale > 0));
	assert(("This numbr of octaves currently not supported", num_octaves > 1 && num_octaves < 5));

	COEF_A = 1488 + rand() % 61876;
	COEF_B = 7 + rand() % 39;
	COEF_C = 3 + rand() % 13;

	float min = 0.0f;
	float max = 0.0f;
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			float n = k_noise_noise(1.0f * i / width * scale, 1.0f * j / height * scale, num_octaves, k_noise_DEFAULT_PERSISTENCE);
			if (min > n) min = n;
			if (max < n) max = n;
		}
	}
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			float n = k_noise_noise(1.0f * i / width * scale, 1.0f * j / height * scale, num_octaves, k_noise_DEFAULT_PERSISTENCE);
			buffer[j * width + i] = (1.0f * (n - min) / (max - min));
		}
	}
}

float k_noise_perlin(float x, float y);

float k_noise_noise(float x, float y, int octaves, float persistence) 
{
	float max = 0.0f, result = 0.0f;
	for (float amplitude = 1.0f; octaves; --octaves, amplitude *= persistence, x *= 2.0f, y *= 2.0f) 
	{
		max += amplitude;
		float temp = k_noise_perlin(x, y);
		result += temp * amplitude;
	}
	return result / max;
}

void k_noise_pseudo_random_gradient(int x, int y, float gradient[]);

float k_noise_dot(float a[], float b[]);

float k_noise_interpolate(float a, float b, float factor);

float k_noise_perlin(float x, float y) {
	int left = (int)floor(x);
	int top = (int)floor(y);
	float local_x = x - left;
	float local_y = y - top;

	float top_left[2], top_right[2], bottom_left[2], bottom_right[2];
	k_noise_pseudo_random_gradient(left, top, top_left);
	k_noise_pseudo_random_gradient(left + 1, top, top_right);
	k_noise_pseudo_random_gradient(left, top + 1, bottom_left);
	k_noise_pseudo_random_gradient(left + 1, top + 1, bottom_right);

	float from_top_left[2] = { local_x, local_y };
	float from_top_right[2] = { local_x - 1, local_y };
	float from_bottom_left[2] = { local_x, local_y - 1 };
	float from_bottom_right[2] = { local_x - 1, local_y - 1 };

	float a = k_noise_dot(from_top_left, top_left);
	float b = k_noise_dot(from_top_right, top_right);
	float c = k_noise_dot(from_bottom_left, bottom_left);
	float d = k_noise_dot(from_bottom_right, bottom_right);

	float i1 = k_noise_interpolate(a, b, local_x);
	float i2 = k_noise_interpolate(c, d, local_x);
	return k_noise_interpolate(i1, i2, local_y);
}

void k_noise_pseudo_random_gradient(int x, int y, float gradient[]) 
{
	srand((x * COEF_A + y*y) * COEF_B + COEF_C + COEF_A ^ y);
	switch (rand() % 4) 
	{
	case 0:  gradient[0] = +1.0f, gradient[1] = +0.0f; break;
	case 1:  gradient[0] = -1.0f, gradient[1] = +0.0f; break;
	case 2:  gradient[0] = +0.0f, gradient[1] = +1.0f; break;
	case 3:  gradient[0] = +0.0f, gradient[1] = -1.0f; break;
	}
}

float k_noise_dot(float a[], float b[]) 
{
	return a[0] * b[0] + a[1] * b[1];
}

float k_noise_interpolate(float a, float b, float factor) 
{
	float fac1 = 3 * powf(1 - factor, 2) - 2 * powf(1 - factor, 3);
	float fac2 = 3 * powf(factor, 2) - 2 * powf(factor, 3);
	return a * fac1 + b * fac2;
}

void k_noise_generate_paraboloid(float buffer[], int width, int height) 
{
	float a = 5.0f;
	float b = 5.0f;
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			float x = (1.0f * i / width - 0.5f);
			float y = (1.0f * j / height - 0.5f);
			buffer[j * width + i] = 1.0f - (a * x * x + b * y * y);
		}
	}
}

void k_noise_transform(float buffer[], int width, int height, float new_min, float new_max) 
{
	float min = 0, max = 0;
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			float temp = buffer[j * width + i];
			if (temp > max)
				max = temp;
			if (temp < min)
				min = temp;
		}
	}
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			float temp = buffer[j * width + i];
			buffer[j * width + i] = new_min + (temp * (new_max - new_min) / (max - min));
		}
	}
}

void k_noise_shutdown()
{

}